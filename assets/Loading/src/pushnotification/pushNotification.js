cc.Class({
    extends: cc.Component,

    properties: {},

    onLoad() {
        if (cc.sys.isNative && 'undefined' !== typeof(sdkbox) && 'undefined' !== typeof(sdkbox.PluginOneSignal)) {
            try {
                sdkbox.PluginOneSignal.init();
                sdkbox.PluginOneSignal.setListener({
                    onSendTag :function (success, key, message) { },
                    onGetTags :function (jsonString) { },
                    onIdsAvailable :function (userId,  pushToken) {
                        if (userId) localStorage.setItem("one_signal_id", userId);
                    },
                    onPostNotification :function (success,  message) { },
                    onNotification :function (isActive,  message, additionalData) { }
                });
                // sdkbox.PluginOneSignal.registerForPushNotifications();
                sdkbox.PluginOneSignal.idsAvailable();
            } catch (error) {
                cc.log(error);
            }
        }
    }
});
