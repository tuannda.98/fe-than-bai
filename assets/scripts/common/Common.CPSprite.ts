import VersionConfig from "./VersionConfig";

const { ccclass, property, requireComponent } = cc._decorator;

@ccclass
@requireComponent(cc.Sprite)
export default class CPSprite extends cc.Component {

    @property(cc.SpriteFrame)
    other: cc.SpriteFrame = null;
    @property(cc.SpriteFrame)
    sin99: cc.SpriteFrame = null;

    onLoad() {
        switch (VersionConfig.CPName) {
            case VersionConfig.CP_NAME_MARBLES99:
            case VersionConfig.CP_NAME_SIN99:
                this.getComponent(cc.Sprite).spriteFrame = this.sin99;
                break;
            default:
                this.getComponent(cc.Sprite).spriteFrame = this.other;
                break;
        }
    }
}
