import Configs from "./Configs";
import VersionConfig from "./VersionConfig";

const { ccclass, property } = cc._decorator;

@ccclass
export default class Http {
    static post(url: string, params: object, onFinished: (err: any, json: any) => void) {
        var xhr = new XMLHttpRequest();
        var _params = "";
        if (params !== null) {
            var count = 0;
            var paramsLength = Object.keys(params).length;
            for (var key in params) {
                if (params.hasOwnProperty(key)) {
                    _params += key + "=" + params[key];
                    if (count < paramsLength - 1) {
                        _params += "&";
                    }
                }
                count++;
            }
        }

        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    var data = null;
                    var e = null;
                    try {
                        data = JSON.parse(xhr.responseText);
                    } catch (ex) {
                        e = ex;
                    }
                    onFinished(e, data);
                } else {
                    onFinished(xhr.status, null);
                }
            }
        };
        xhr.open("POST", url, true);
        xhr.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
        xhr.send(_params);
    }

    static get(url: string, params: object, onFinished: (err: any, json: any) => void) {
        var xhr = new XMLHttpRequest();
        var _params = "";
        params = params || {};
        switch (VersionConfig.CPName) {
            case VersionConfig.CP_NAME_VIP52:
                if (!params.hasOwnProperty("cp")) {
                    params["cp"] = "V";
                }
                params["cl"] = "V";
                break;
            case VersionConfig.CP_NAME_MANVIP:
                if (!params.hasOwnProperty("cp")) {
                    params["cp"] = "MAN";
                }
                params["cl"] = "MAN";
                break;
            case VersionConfig.CP_NAME_XXENG:
                if (!params.hasOwnProperty("cp")) {
                    params["cp"] = "X";
                }
                params["cl"] = "X";
                break;
            case VersionConfig.CP_NAME_R99_2:
                if (!params.hasOwnProperty("cp")) {
                    params["cp"] = "R2";
                }
                params["cl"] = "R2";
                break;
            case VersionConfig.CP_NAME_HT68:
                if (!params.hasOwnProperty("cp")) {
                    params["cp"] = "H";
                }
                params["cl"] = "H";
                break;
            case VersionConfig.CP_NAME_88KING:
                if (!params.hasOwnProperty("cp")) {
                    params["cp"] = "K";
                }
                params["cl"] = "K";
                break;
            case VersionConfig.CP_NAME_MARBLES99:
            case VersionConfig.CP_NAME_SIN99:
                if (!params.hasOwnProperty("cp")) {
                    params["cp"] = "S";
                }
                params["cl"] = "S";
                break;
            case VersionConfig.CP_NAME_TESTGAME1:
                if (!params.hasOwnProperty("cp")) {
                    params["cp"] = "H";
                }
                params["cl"] = "H";
                break;
            default:
                if (!params.hasOwnProperty("cp")) {
                    params["cp"] = "R";
                }
                params["cl"] = "R";
                break;
        }
        if (cc.sys.isNative && cc.sys.os == cc.sys.OS_ANDROID) {
            params["pf"] = "ad";
        } else if (cc.sys.isNative && cc.sys.os == cc.sys.OS_IOS) {
            params["pf"] = "ios";
        } else if (!cc.sys.isNative) {
            params["pf"] = "web";
        } else {
            params["pf"] = "other";
        }
        if (!params["at"]) {
            params["at"] = Configs.Login.AccessToken;
        }
        if (params !== null) {
            // var count = 0;
            // var paramsLength = Object.keys(params).length;
            // for (var key in params) {
            //     if (params.hasOwnProperty(key)) {
            //         _params += key + "=" + params[key];
            //         if (count++ < paramsLength - 1) {
            //             _params += "&";
            //         }
            //     }
            // }
            _params = Http.serialize(params);
        }

        xhr.onreadystatechange = function () {
            if (xhr.readyState === 4) {
                if (xhr.status === 200) {
                    var data = null;
                    var e = null;
                    try {
                        data = JSON.parse(xhr.responseText);
                    } catch (ex) {
                        e = ex;
                    }
                    onFinished(e, data);
                } else {
                    onFinished(xhr.status, null);
                }
            }
        };
        // console.log("*** url",url + "?" + _params);
        xhr.open("GET", url + "?" + _params, true);
        xhr.send();
    }

    static serialize(obj) {
        var str = [];
        for (var p in obj) {
            if (obj.hasOwnProperty(p)) {
                str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
            }
        }
        return str.join("&");
    }
}
