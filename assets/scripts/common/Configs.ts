import SPUtils from "./SPUtils";
import Http from "./Http";
import VersionConfig from "./VersionConfig";

namespace Configs {
	export class Login {
		static UserId: number = 0;
		static Username: string = "";
		static Password: string = "";
		static Nickname: string = "";
		static Avatar: string = "";
		static Coin: number = 0;
		static IsLogin: boolean = false;
		static AccessToken: string = "";
		static SessionKey: string = "";
		static LuckyWheel: number = 0;
		static CreateTime: string = "";
		static Birthday: string = "";
		static IpAddress: string = "";
		static VipPoint: number = 0;
		static VipPointSave: number = 0;
		static MailCount: number = 0;

		static CoinFish: number = 0;
		static UserIdFish: number = 0;
		static UsernameFish: string = "";
		static PasswordFish: string = "";
		static FishConfigs: any = null;
		static BitcoinToken: string = "";

		static isSocialLogin: boolean = false;
		static socialToken: string = "";
		static socialSource: string = "";

		static clear() {
			this.UserId = 0;
			this.Username = "";
			this.Password = "";
			this.Nickname = "";
			this.Avatar = "";
			this.Coin = 0;
			this.IsLogin = false;
			this.AccessToken = "";
			this.SessionKey = "";
			this.CreateTime = "";
			this.Birthday = "";
			this.IpAddress = "";
			this.VipPoint = 0;
			this.VipPointSave = 0;
			this.MailCount = 0;
			this.CoinFish = 0;
			this.UserIdFish = 0;
			this.UsernameFish = "";
			this.PasswordFish = "";
			this.BitcoinToken = "";
			SPUtils.setUserPass("");
			this.isSocialLogin = false;
			this.socialToken = "";
			this.socialSource = "";
		}

		static readonly VipPoints = [80, 800, 4500, 8600, 12000, 50000, 1000000, 2000000];
		static readonly VipPointsName = ["Đá", "Đồng", "Bạc", "Vàng", "BK1", "BK2", "KC1", "KC2", "KC3"];
		static getVipPointName(): string {
			for (let i = this.VipPoints.length - 1; i >= 0; i--) {
				if (Configs.Login.VipPoint > this.VipPoints[i]) {
					return this.VipPointsName[i + 1];
				}
			}
			return this.VipPointsName[0];
		}
		static getVipPointNextLevel(): number {
			for (let i = this.VipPoints.length - 1; i >= 0; i--) {
				if (Configs.Login.VipPoint > this.VipPoints[i]) {
					if (i == this.VipPoints.length - 1) {
						return this.VipPoints[i];
					}
					return this.VipPoints[i + 1];
				}
			}
			return this.VipPoints[0];
		}
		static getVipPointIndex(): number {
			for (let i = this.VipPoints.length - 1; i >= 0; i--) {
				if (Configs.Login.VipPoint > this.VipPoints[i]) {
					return i;
				}
			}
			return 0;
		}
	}

	export class App {
		// static readonly API: string = "https://portal.xxeng.vip/api";
		// static readonly API: string = "https://portal.sieutoc24h.net/api";
		static HOT_UPDATE_URL = "http://r99.vin/web/remote-assets/";
		static SUBPACKAGE_URL = "http://r99.vin/web/remote-assets/";
		static DOMAIN: string = "https://r99.vin/web/";
		static API: string = "https://api.gatrix.net/api";
		static MONEY_TYPE = 1;
		static LINK_DOWNLOAD = "https://r99.vin/download";
		static LINK_EVENT = "https://r99.vin/event";
		static LINK_SUPPORT = "https://www.tidio.com/talk/eniggwwvrw4mcnbgk6ivneugifkygu7o";
		static USE_WSS = true;
		static readonly HOST_MINIGAME = {
			// host: "minigame.xxeng.vip",
			// host: "minigame.sieutoc24h.net",
			host: "6d69.r99.bar",
			port: 443
		};
		static readonly HOST_TAI_XIU_MINI2 = {
			// host: "overunder.xxeng.vip",
			// host: "overunder.sieutoc24h.net",
			host: "overunder.r99.bar",
			port: 443
		};
		static readonly HOST_SLOT = {
			// host: "slot.xxeng.vip",
			// host: "slot.sieutoc24h.net",
			host: "736c.r99.bar",
			port: 443
		};
		static readonly HOST_TLMN = {
			// host: "tlmn.xxeng.vip",
			// host: "tlmn.sieutoc24h.net",
			host: "746c.r99.bar",
			port: 443
		};
		static readonly HOST_SHOOT_FISH = {
			// host: "banca2.xxeng.vip",
			host: "6132.r99.bar",
			port: 443
		};
		static readonly HOST_SAM = {
			host: "sam.xxeng.vip",
			// host: "sam.sieutoc24h.net",
			// host: "746c.r99.bar",
			port: 443
		};
		static readonly HOST_XOCDIA = {
			host: "xocdia.xxeng.vip",
			port: 443
		};
		static readonly HOST_BACAY = {
			host: "bacay.xxeng.vip",
			port: 443
		};
		static readonly HOST_BAICAO = {
			host: "baicao.xxeng.vip",
			port: 443
		};
		static readonly HOST_POKER = {
			host: "poker.xxeng.vip",
			port: 443
		};
		static readonly HOST_MAUBINH = {
			host: "binh.xxeng.vip",
			port: 443
		};

		static readonly HOST_XIDZACH = {
			host: "xizach.xxeng.vip",
			port: 443
		};

		static readonly HOST_LIENG = {
			host: "lieng.xxeng.vip",
			port: 443
		};


		static TELE_GROUP = "";
		static TELE_BOT = "";
		static FANPAGE = "https://facebook.com";
		static CSKH_PHONE = "";

		static readonly SERVER_CONFIG = {
			ratioNapThe: 1,
			ratioTransfer: 0.98,
			ratioTransferDL: 1,
			ratioMuaThe: 1.25,
			ratioRutNH: 1.1,
			listTenNhaMang: ["Viettel", "Vinaphone", "Mobifone"],
			listIdNhaMang: [0, 1, 2, 3],
			listMenhGiaNapThe: [10000, 20000, 50000, 100000, 200000, 500000],
			listMenhGiaMuaThe: [10000, 20000, 50000, 100000, 200000, 500000],
			listIdMenhGiaMuaThe: [0, 1, 2, 3, 4, 5, 6, 7, 8]
		};
		static getServerConfig() {
			Http.get(Configs.App.API, { "c": 130 }, (err, res) => {
				if (err == null) {
					App.SERVER_CONFIG.ratioNapThe = res.ratio_nap_the;
					App.SERVER_CONFIG.ratioTransfer = res.ratio_chuyen;
					App.SERVER_CONFIG.ratioTransferDL = res.ratio_transfer_dl_1;
				}
			});
		}

		static getPlatformName() {
			if (cc.sys.isNative && cc.sys.os == cc.sys.OS_ANDROID) return "android";
			if (cc.sys.isNative && cc.sys.os == cc.sys.OS_IOS) return "ios";
			return "web";
		}

		static getLinkFanpage() {
			return this.FANPAGE;
			switch (VersionConfig.CPName) {
				case VersionConfig.CP_NAME_VIP52:
					return "https://www.facebook.com/101257598255718/posts/101260791588732/?d=n";
				case VersionConfig.CP_NAME_XXENG:
					return "https://www.facebook.com/xxengx2dangcap/";
				case VersionConfig.CP_NAME_MANVIP:
					return "https://www.facebook.com/manvipx2dangcap/";
				case VersionConfig.CP_NAME_88KING:
					return "https://www.facebook.com/88kingclub";
				case VersionConfig.CP_NAME_MARBLES99:
				case VersionConfig.CP_NAME_SIN99:
					return "https://www.facebook.com/sin99club";
					break;
				default:
					return "https://www.facebook.com/R99-Rolls-Royce-G%C3%A1i-%C4%90%E1%BA%B9p-109602154040456";
			}
		}

		static getLinkTelegram() {
			return this.TELE_BOT;
			// switch (VersionConfig.CPName) {
			// 	case VersionConfig.CP_NAME_TESTGAME1:
			// 		return "";
			// 	default:
			// 		return "";
			// }
		}

		static getLinkTelegramGroup() {
			return this.TELE_GROUP;
			// switch (VersionConfig.CPName) {
			// 	case VersionConfig.CP_NAME_VIP52:
			// 		return "r99doithuong";
			// 	case VersionConfig.CP_NAME_XXENG:
			// 		return "https://www.facebook.com/xxengx2dangcap/";
			// 	case VersionConfig.CP_NAME_MANVIP:
			// 		return "https://www.facebook.com/manvipx2dangcap/";
			// 	case VersionConfig.CP_NAME_88KING:
			// 		return "king88_otp_bot";
			// 	case VersionConfig.CP_NAME_MARBLES99:
			// 	case VersionConfig.CP_NAME_SIN99:
			// 		return "KENHSIN99CLUBDANGCAP";
			// 	default:
			// 		return "r99doithuong";
			// }
		}

		static init() {
			switch (VersionConfig.CPName) {
				case VersionConfig.CP_NAME_TESTGAME1:
					this.USE_WSS = true;
					this.HOT_UPDATE_URL = "http://kingonltest.club/web/remote-assets/";
					this.SUBPACKAGE_URL = "https://cdn.mercedesagency.info/king/subpackages/v456/";
					
					this.DOMAIN = "https://kingonltest.club/web/";
					this.API = "https://api.gatrix.net/api";
					this.MONEY_TYPE = 1;
					this.LINK_DOWNLOAD = "https://kingonltest.club/download";
					this.LINK_EVENT = "https://kingonltest.club/event";
					this.LINK_SUPPORT = "https://www.tidio.com/talk/eniggwwvrw4mcnbgk6ivneugifkygu7o";

					this.HOST_MINIGAME.host = "minigame.kingonltest.club";
					this.HOST_TAI_XIU_MINI2.host = "overunder.kingonltest.club";
					this.HOST_SLOT.host = "slot2.kingonltest.club";
					this.HOST_TLMN.host = "tlmn.kingonltest.club";
					this.HOST_SHOOT_FISH.host = "banca2.kingonltest.club";
					this.HOST_SAM.host = "sam.gatrix.net";
					this.HOST_XOCDIA.host = "xocdia.kingonltest.club";
					this.HOST_BACAY.host = "bacay.kingonltest.club";
					this.HOST_BAICAO.host = "baicao.kingonltest.club";
					this.HOST_POKER.host = "poker.kingonltest.club";
					this.HOST_MAUBINH.host = "binh.kingonltest.club";
					this.HOST_XIDZACH.host = "xzdzach.kingonltest.club";
					this.HOST_LIENG.host = "lieng.kingonltest.club";
					break;
				default:
			}
		}

		static readonly REWARD_TYPE = {
			CARD: 0,
			REWARD: 1,
			MOMO: 2,
			BANK: 3,
		}

		static readonly TELCO_NAME = {
			"VTT": "Viettel",
			"VNP": "Vinaphone",
			"VMS": "Mobifone",
		}
	}
	export class GameId {
		static readonly MiniPoker = 1;
		static readonly TaiXiu = 2;
		static readonly BauCua = 3;
		static readonly CaoThap = 4;
		static readonly Slot3x3 = 5;
		static readonly VQMM = 7;
		static readonly Sam = 8;
		static readonly BaCay = 9;
		static readonly MauBinh = 10;
		static readonly TLMN = 11;
		static readonly TaLa = 12;
		static readonly Lieng = 13;
		static readonly XiTo = 14;
		static readonly XocXoc = 15;
		static readonly BaiCao = 16;
		static readonly Poker = 17;
		static readonly Bentley = 19;
		static readonly Avenger = 18;
		static readonly RangeRover = 20;
		static readonly MayBach = 21;
		static readonly RollsRoyce = 22;

		static getGameName(gameId: number): string {
			switch (gameId) {
				case this.MiniPoker:
					return "MiniPoker";
				case this.TaiXiu:
					return "Tài xỉu";
				case this.BauCua:
					return "Bầu cua";
				case this.CaoThap:
					return "Cao thấp";
				case this.Slot3x3:
					// return "Slot3x3";
					return "FootBall";
				case this.VQMM:
					return "VQMM";
				case this.Sam:
					return "Sâm";
				case this.MauBinh:
					return "Mậu binh";
				case this.TLMN:
					return "TLMN";
				case this.TaLa:
					return "Tá lả";
				case this.Lieng:
					return "Liêng";
				case this.XiTo:
					return "Xì tố";
				case this.XocXoc:
					return "Xóc xóc";
				case this.BaiCao:
					return "Bài cào";
				case this.Poker:
					return "Poker";
				case this.Bentley:
					return "Medusa";
				case this.RangeRover:
					return "Kingdom";
				case this.MayBach:
					return "Egypt";
				case this.RollsRoyce:
					return "Golden Monkey";
				case this.Avenger:
					return "Medusa";
			}
			return "Unknow";
		}
	}
}
export default Configs;
Configs.App.init();