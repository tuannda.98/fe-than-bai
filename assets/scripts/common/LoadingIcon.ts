const { ccclass, property } = cc._decorator;

@ccclass
export default class LoadingIcon extends cc.Component {

    @property(cc.Label)
    percent: cc.Label = null;

    @property(cc.Node)
    circle: cc.Node = null;

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        this.circle.runAction(cc.repeatForever(cc.rotateBy(1, 360)));
    }

    setPercent(num) {
        this.percent.string = num + "%";
    }

    show() {
        this.node.x = 0;
        this.node.y = 0;
        this.node.active = true;
        var gameContainer = cc.find("Canvas/Center");
        if (gameContainer) {
            gameContainer.pauseSystemEvents(true);
        }
    }

    hide() {
        if(this.node && this.node.isValid){
            this.node.active = false;
        }
        var gameContainer = cc.find("Canvas/Center");
        if (gameContainer) {
            gameContainer.resumeSystemEvents(true);
        }
    }
}
