const { ccclass, property } = cc._decorator;

@ccclass
export default class Toast extends cc.Component {

    @property(cc.Label)
    txt: cc.Label = null;

    onLoad() {
        this.node.active = false;
        this.node.zIndex = 1000;
    }

    showToast(str: string, time: number = 0.5, timeFade: number = 0.5, d = 80) {
        this.node.stopAllActions();
        this.node.active = true;
        this.node.opacity = 0;
        this.node.x = cc.winSize.width/2;
        this.node.y = cc.winSize.height/2;
        this.node.scaleX = 0;
        this.node.scaleY = 1;
        this.txt.string = str;
        this.node.active = false;
        this.node.active = true;
        this.node.width = this.txt.node.width + 50;
        this.node.runAction(cc.sequence(
            cc.spawn(
                cc.fadeIn(timeFade / 2),
                cc.scaleTo(timeFade, 1, 1).easing(cc.easeBackOut())
            ),
            cc.spawn(
                cc.moveBy(time, 0, d),
                cc.sequence(
                    cc.delayTime(time / 2),
                    cc.fadeOut(time / 2).easing(cc.easeSineIn())
                )
            ),
            cc.callFunc(() => {
                this.node.active = false;
            })
        ));
    }

    hide() {
        this.node.active = false;
        this.node.stopAllActions();
    }

}