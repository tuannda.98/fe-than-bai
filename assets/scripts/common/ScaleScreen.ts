const { ccclass, property } = cc._decorator;

export enum ScaleScreenType {
    FIT_IN = 0,
    FIT_OUT = 1
}

export enum AlignMode {
    ONCE = 0,
    ALWAYS = 1,
    ON_SCREEN_RESIZE = 2
}

@ccclass
export default class ScaleScreen extends cc.Component {
    @property(cc.Boolean)
    fitWidth: boolean = false;

    @property(cc.Boolean)
    fitHeight: boolean = false;

    @property({ type: cc.Enum(ScaleScreenType) })
    scaleType: ScaleScreenType = ScaleScreenType.FIT_IN;

    @property(cc.Integer)
    heightDegree: number = 0;

    @property(cc.Integer)
    widthDegree: number = 0;

    @property({ type: cc.Enum(AlignMode) })
    alignMode: AlignMode = AlignMode.ONCE;

    onLoad() {
        this.doScale();
        if (this.alignMode == AlignMode.ON_SCREEN_RESIZE) {
            // calls.push(this.doScale.bind(this));
        }
    }

    private doScale() {
        if (!this.node) {
            return;
        }
        let scaleFactorX = cc.winSize.width / (this.node.width + this.widthDegree);
        let scaleFactorY = cc.winSize.height / (this.node.height + this.heightDegree);

        if (this.fitWidth && !this.fitHeight) {
            this.node.scale = scaleFactorX;
        }

        if (this.fitHeight && !this.fitWidth) {
            this.node.scale = scaleFactorY;
        }

        if (this.fitWidth && this.fitHeight) {
            if (this.scaleType == ScaleScreenType.FIT_IN) {
                this.node.scale = scaleFactorX < scaleFactorY ? scaleFactorX : scaleFactorY;
            } else if (this.scaleType == ScaleScreenType.FIT_OUT) {
                this.node.scale = scaleFactorX > scaleFactorY ? scaleFactorX : scaleFactorY;
            }
        }

    }

    update() {
        if (this.alignMode == AlignMode.ALWAYS) {
            this.doScale();
        }
    }

}