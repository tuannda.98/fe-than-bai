import VersionConfig from "./VersionConfig";

const { ccclass, property, requireComponent } = cc._decorator;

@ccclass
@requireComponent(cc.Label)
export default class CPSprite extends cc.Component {

    @property
    strR99: string = "";
    @property
    strVip52: string = "";
    @property
    strXXeng: string = "";
    @property
    strManVip: string = "";

    onLoad() {
        switch (VersionConfig.CPName) {
            case VersionConfig.CP_NAME_VIP52:
                this.getComponent(cc.Label).string = this.strVip52;
                break;
            case VersionConfig.CP_NAME_XXENG:
                this.getComponent(cc.Label).string = this.strXXeng;
                break;
            case VersionConfig.CP_NAME_MANVIP:
                this.getComponent(cc.Label).string = this.strManVip;
                break;
            default:
                this.getComponent(cc.Label).string = this.strR99;
                break;
        }
    }
}
