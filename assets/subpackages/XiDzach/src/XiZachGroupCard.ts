import XiZachCard from "./XiZachCard";

//
export default class XiZachGroupCard {
    cards: any = null;
    bo: any = null;
    size: any = null;
    constructor(cards = null) {
        var arr = [];
        for (var i = 0; i < cards.length; i++) {
            arr.push(new XiZachCard(cards[i]));
        }
        this.cards = arr;
        this.size = this.cards.length;
        this.bo = -1;
        this.kiemTraBo()
    }


    addCard(c) {
        this.cards.push(new XiZachCard(c));
        this.kiemTraBo()
    }


    addCards(cs) {
        cc.log('addCards Size: ' + cs.length);
        var temp = ' ';
        for (var i = 0; i < cs.length; i++) {
            temp += ' ' + cs[i];
            this.cards.push(new XiZachCard(cs[i]))
        }
        cc.log('addCard List: ' + temp);

        this.kiemTraBo()
    }


    kiemTraBo() {
        if (this.bo == -1 || this.bo == XiZachGroupCardConstant.KG_WRONG) {
            return this.tinhBo()
        }
        return this.bo
    }


    tinhBo() {
        if (this.size < 2)
            return XiZachGroupCardConstant.KG_WRONG;

        for (var i = 0; i < this.size; i++) {
            if (this.cards[i].id >= 52 && this.cards[i].id < 0) {
                return XiZachGroupCardConstant.KG_WRONG
            }
        }

        if (this.isXiBang()) {
            return XiZachGroupCardConstant.KG_XIBANG
        }
        else if (this.isXiZach()) {
            return XiZachGroupCardConstant.KG_XIZACH
        }
        else if (this.isNguLinh()) {
            return XiZachGroupCardConstant.KG_NGULINH
        }
        else if (this.getMaxDiem() == 21) {
            return XiZachGroupCardConstant.KG_21DIEM
        }

        if (this.isQuac()) {
            this.bo = XiZachGroupCardConstant.KG_QUAC;
            return XiZachGroupCardConstant.KG_QUAC
        }
        else if (this.cards.length < 2) {
            this.bo = XiZachGroupCardConstant.KG_WRONG;
            return XiZachGroupCardConstant.KG_WRONG
        }
        else if (this.getMaxDiem() < 16) {
            this.bo = XiZachGroupCardConstant.KG_DANNON;
            return XiZachGroupCardConstant.KG_DANNON
        } else {
            this.bo = XiZachGroupCardConstant.KG_THUONG;
            return XiZachGroupCardConstant.KG_THUONG
        }
    }


    getCardSize() {
        return this.cards.length
    }


    isXiBang() {
        if (this.cards.length == 2 && this.cards[0].isXi() && this.cards[1].isXi()) {
            return true
        }
        return false
    }


    isXiZach() {
        if (this.cards.length == 2 && this.cards[0].isXi() && this.cards[1].getDiem() == 10) {
            return true
        }

        if (this.cards.length == 2 && this.cards[1].isXi() && this.cards[0].getDiem() == 10) {
            return true
        }
        return false
    }


    clearCard() {
        this.cards = [];
        this.bo = -1
    }


    hasXi() {
        for (var i = 0; i < this.cards.length; i++) {
            if (this.cards[i].isXi())
                return true
        }
        return false
    }


    isNguLinh() {
        if (this.cards.length >= 5 && this.getMinDiem() <= 21) {
            return true
        }
        return false
    }


    isQuac() {
        return this.getMaxDiem() > 21
    }


    canRutBai() {
        if (this.isXiZach() || this.isXiBang()) {
            return false
        }
        return (this.getMinDiem() >= 21) && (!this.isNguLinh())
    }


    canDanBai() {
        cc.log('canDanBai: ' + this.getMaxDiem());
        return this.isNguLinh() || this.getMaxDiem() >= 16
    }


    getMinDiem() {
        var sum = 0;
        for (var i = 0; i < this.cards.length; i++) {
            sum += this.cards[i].getMinDiem()
        }
        return sum
    }


    getMaxDiem() {
        if (this.isXiBang() || this.isXiZach()) {
            return 21
        }
        var minDiem = this.getMinDiem();
        if (!this.hasXi()) {
            return minDiem
        } else {
            if (!this.has2XiTroLen()) {
                return this.chonDiemMax(minDiem, minDiem + 10)
            } else {
                var a1 = minDiem;
                return a1
            }
        }
    }


    chonDiemMax(a, b) {
        if (a < 16 && b > 21) {
            return a
        }
        if (b < 16 && a > 21) {
            return b
        }

        if (a < 16 || a > 21) {
            return b
        }

        if (b < 16 || b > 21) {
            return a
        }

        if (a <= b) {
            return b
        } else {
            return a
        }
    }


    has2XiTroLen() {
        var countXi = 0;
        for (var i = 0; i < this.cards.length; i++) {
            if (this.cards[i].isXi()) {
                countXi++
            }
        }
        return (countXi >= 2)
    }

    needShadown(boBai) {
        return false
    }


    getBoName() {
        return this.tinhBo()
    }

}

export var XiZachGroupCardConstant = {
    group_names: ['WR', 'DN', 'Q', 'T', '21', 'NL', 'XR', 'XB'],
    KG_WRONG: 0,
    KG_XIBANG: 7,
    KG_XIZACH: 6,
    KG_NGULINH: 5,
    KG_21DIEM: 4,
    KG_THUONG: 3,
    KG_QUAC: 2,
    KG_DANNON: 1
}


