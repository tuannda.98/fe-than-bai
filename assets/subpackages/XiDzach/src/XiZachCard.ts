export default class XiZachCard {
    id: any = null;
    so: any = null;
    chat: any = null;

    constructor(id = null) {
        this.id = id;
        this.so = this.getNumber();
        this.chat = this.getSuit()
    }

    setCard(number, suit) {
        this.id = (number - 2) * 4 + suit
    }

    getNumber() {
        return (Math.floor(this.id / 4))
    }

    getSuit() {
        return (this.id % 4)
    }

    getId() {
        return this.id
    }

    getDisplayId(id) {
        var realSo;
        if (id < 0 || id >= 52) {
            realSo = 52
        } else {
            realSo = (id - 8 + 52) % 52
        }

        return realSo
    }

    isXi() {
        return this.so == 0
    }

    getDiem() {
        if (this.isXi()) {
            return 1
        } else {
            if (this.so == 9 || this.so == 10 || this.so == 11 || this.so == 12) {
                return 10
            } else {
                return this.so + 1
            }
        }
    }

    getMinDiem() {
        if (this.isXi()) {
            return 1
        } else {
            if (this.so == 9 || this.so == 10 || this.so == 11 || this.so == 12) {
                return 10
            } else {
                return this.so + 1
            }
        }
    }
}
