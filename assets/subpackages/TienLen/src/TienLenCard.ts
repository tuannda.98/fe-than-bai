export var TienLen = {
  isGreater: function (a, b) {
    return a.id > b.id
  },
  CardGroup: {
    TYPENONE: -1,
    TYPEMOTLA: 1,
    TYPEDOI: 2,
    TYPEBALA: 3,
    TYPESANH: 4,
    TYPETUQUY: 5,
    BADOITHONG: 6,
    BONDOITHONG: 7,
  },
  kQuanbai3: 0,
  kQuanbai4: 1,
  kQuanbai5: 2,
  kQuanbai6: 3,
  kQuanbai7: 4,
  kQuanbai8: 5,
  kQuanbai9: 6,
  kQuanbai10: 7,
  kQuanbaiJ: 8,
  kQuanbaiQ: 9,
  kQuanbaiK: 10,
  kQuanbaiA: 11,
  kQuanbai2: 12,
  kQuanbaiNONE: 13,

  kChatBICH: 0,
  kChatCHUON: 1,
  kChatRO: 2,
  kChatCO: 3,
  kiemtraChatQuan: function (aGroup, bGroup, changeTurnChair, chairLastTurn) {
    if (changeTurnChair != 0) {
      if (bGroup.typeGroup != TienLen.CardGroup.BONDOITHONG || chairLastTurn == 0) {
        return false;
      }
    }

    var len1 = aGroup.cards.length;
    var len2 = bGroup.cards.length;

    if (aGroup.typeGroup != bGroup.typeGroup) {
      // Nhung TH dac biet khac loai nhom bai ma co the chat duoc
      if ((aGroup.typeGroup == TienLen.CardGroup.TYPEMOTLA) && (aGroup.cards[0].so == TienLen.kQuanbai2) && (bGroup.typeGroup == TienLen.CardGroup.TYPETUQUY ||
        bGroup.typeGroup == TienLen.CardGroup.BADOITHONG || bGroup.typeGroup == TienLen.CardGroup.BONDOITHONG)) {
        return true
      } else if ((aGroup.typeGroup == TienLen.CardGroup.TYPEDOI) && (aGroup.cards[0].so == TienLen.kQuanbai2) && (bGroup.typeGroup == TienLen.CardGroup.TYPETUQUY || bGroup.typeGroup == TienLen.CardGroup.BONDOITHONG)) {
        return true
      } else if ((aGroup.typeGroup == TienLen.CardGroup.BADOITHONG) && (bGroup.typeGroup == TienLen.CardGroup.TYPETUQUY || bGroup.typeGroup == TienLen.CardGroup.BONDOITHONG)) {
        return true
      } else if ((aGroup.typeGroup == TienLen.CardGroup.TYPETUQUY) && (bGroup.typeGroup == TienLen.CardGroup.BONDOITHONG)) {
        return true
      }
      return false
    }

    switch (aGroup.typeGroup) {
      case TienLen.CardGroup.TYPEMOTLA:
      case TienLen.CardGroup.TYPEDOI:
      case TienLen.CardGroup.TYPEBALA:
      case TienLen.CardGroup.BADOITHONG:
      case TienLen.CardGroup.BONDOITHONG:
      case TienLen.CardGroup.TYPETUQUY:
        return bGroup.cards[len2 - 1].id > aGroup.cards[len1 - 1].id;
        break;
      case TienLen.CardGroup.TYPESANH: {
        if ((len1 != len2) || (aGroup.cards[len1 - 1].id >= bGroup.cards[len2 - 1].id)) {
          return false
        } else {
          return true
        }

        break
      }
   
      default:
        return false;
        break
    }
  },
  kiemtraDanh: function (cards) {
    var group = new TienLenCardGroup(cards);
    if (group.typeGroup != TienLen.CardGroup.TYPENONE)
      return true;
    return false
  }
}





export class TienLenCard {
  id: any = null;
  chat: any = null;
  so: any = null;
  constructor(id = null) {
    this.id = id;
    this.chat = this.getChatById(this.id);
    this.so = this.getSoById(this.id)
  }

  getChatById(id) {
    return id % 4
  }

  getSoById(id) {
    return Math.floor(id / 4)
  }

  getDisplayId(id) {
    return Math.floor(id / 4)
  }

  isGreater(card2) {
    return this.id > card2.id
  }

  convertToServerCard(idex) {
    return idex
  }
}

export class TienLenCardGroup {
  // ctor with array of Cards
  typeGroup: any = null;
  cards: any = null;
  constructor(cards) {
    this.typeGroup = -1;
    this.cards = cards;
    this.initCards();
  }

  // Sort cards and find typeGroup
  initCards() {
    if (this.cards.length == 0)
      return;

    this.cards.sort(function (card1, card2) {
      return card1.id - card2.id
    }); // sort tang dan`

    var size = this.cards.length;
    this.typeGroup = TienLen.CardGroup.TYPENONE;
    if (size == 1) {
      this.typeGroup = TienLen.CardGroup.TYPEMOTLA;
      return
    } else if (size == 2) {
      if ((this.cards[0].so) == (this.cards[1].so)) {
        this.typeGroup = TienLen.CardGroup.TYPEDOI;
        return
      }
      return
    } else if (size == 3) {
      if ((this.cards[0].so) == (this.cards[1].so) && (this.cards[0].so) == (this.cards[2].so)) {
        this.typeGroup = TienLen.CardGroup.TYPEBALA;
        return
      }
    } else if (size == 4) {
      if ((this.cards[0].so) == (this.cards[1].so) && (this.cards[0].so) == (this.cards[2].so) && (this.cards[0].so) == (this.cards[3].so)) {
        this.typeGroup = TienLen.CardGroup.TYPETUQUY;
        return
      }
    } else if (size == 6) {
      if ((this.cards[0].so) == (this.cards[1].so) && (this.cards[2].so) == (this.cards[3].so) && (this.cards[4].so) == (this.cards[5].so)
        && (this.cards[2].so == this.cards[1].so + 1) && (this.cards[4].so == this.cards[2].so + 1) && (this.cards[4].so != TienLen.kQuanbai2)) {
        this.typeGroup = TienLen.CardGroup.BADOITHONG;
        return
      }
    } else if (size == 8) {
      if ((this.cards[0].so) == (this.cards[1].so) && (this.cards[2].so) == (this.cards[3].so) && (this.cards[4].so) == (this.cards[5].so) && (this.cards[6].so == this.cards[7].so)
        && (this.cards[2].so == this.cards[1].so + 1) && (this.cards[4].so == this.cards[3].so + 1) && (this.cards[6].so == this.cards[5].so + 1) && (this.cards[6].so != TienLen.kQuanbai2)) {
        this.typeGroup = TienLen.CardGroup.BONDOITHONG;
        return
      }
    }

    var sanh = false;
    var so = this.cards[0].so;

    var countSanh = 0;

    for (var i = 1; i < this.cards.length; i++) {
      if ((this.cards[i].so - 1) == so) {
        so = this.cards[i].so;
        countSanh++
      }
    }

    if (countSanh == this.cards.length - 1 && countSanh >= 2 && this.cards[this.cards.length - 1].so != TienLen.kQuanbai2) {
      sanh = true
    }

    if (sanh) {
      this.typeGroup = TienLen.CardGroup.TYPESANH;

    }
  }

  // make sanh theo dung thu tu danh bai
  makeSanhArray() {
    var res = [];
    for (var i = 0; i < this.cards.length; i++) {
      res.push(this.cards[i])
    }
    return res
  }
}
