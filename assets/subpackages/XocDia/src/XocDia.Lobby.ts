import Configs from "../../../scripts/common/Configs";
import BroadcastReceiver from "../../../scripts/common/BroadcastReceiver";
import Utils from "../../../scripts/common/Utils";
import XocDiaNetworkClient from "./XocDia.XocDiaNetworkClient";
import cmd from "./XocDia.Cmd";
import InPacket from "../../../scripts/networks/Network.InPacket";
import App from "../../../scripts/common/App";
import XocDiaController from "./XocDia.XocDiaController";
import CardGameCmd from "../../../scripts/networks/CardGame.Cmd";

const { ccclass, property } = cc._decorator;

@ccclass
export default class Lobby extends cc.Component {

    @property(cc.Label)
    lblNickname: cc.Label = null;
    @property(cc.Label)
    lblCoin: cc.Label = null;

    @property(cc.Node)
    listItems: cc.Node = null;
    @property(cc.Prefab)
    itemTemplate: cc.Prefab = null;

    private inited = false;

    // onLoad () {}

    start() {

    }

    public init() {
        if (this.inited) return;
        this.inited = true;

        this.lblNickname.string = Configs.Login.Nickname;
        BroadcastReceiver.register(BroadcastReceiver.USER_UPDATE_COIN, () => {
            if (!this.node.active) return;
            this.lblCoin.string = Utils.formatNumber(Configs.Login.Coin);
        }, this);
        BroadcastReceiver.send(BroadcastReceiver.USER_UPDATE_COIN);

        XocDiaNetworkClient.getInstance().addListener((data) => {
            if (!this.node.active) return;
            let inpacket = new InPacket(data);
            switch (inpacket.getCmdId()) {
                case cmd.Code.LOGIN:
                    {
                        XocDiaNetworkClient.getInstance().send(new cmd.SendReconnect());
                    }
                    break;
                case cmd.Code.GETLISTROOM:
                    {
                        // let res = new cmd.ReceiveGetListRoom(data);
                        // console.log(res);
                        // let bigBet = 0;
                        // let id = 0;
                        // res.list.sort((a, b) => {
                        //     return a["moneyBet"] - b["moneyBet"];
                        // });
                        // for (let i = 0; i < res.list.length; i++) {
                        //     let itemData = res.list[i];
                        //     let item = cc.instantiate(this.itemTemplate);
                        //     item.parent = this.listItems;
                        //     id++;
                        //     item.getChildByName("ID").getComponent(cc.Label).string = id.toString();
                        //     item.getChildByName("bet").getComponent(cc.Label).string = Utils.formatNumber(itemData["moneyBet"]);
                        //     item.getChildByName("num Players").getComponent(cc.Label).string = itemData["userCount"];// + "/" + itemData["maxUserPerRoom"];
                        //     item.getChildByName("bet Min").getComponent(cc.Label).string = Utils.formatNumber(itemData["requiredMoney"]);
                        //     /*item.getChildByName("lblBet").getComponent(cc.Label).string = Utils.formatNumber(itemData["moneyBet"]);

                        //     item.getChildByName("lblPlayers").getComponent(cc.Label).string = itemData["userCount"] + "/" + itemData["maxUserPerRoom"];
                        //     item.getChildByName("sprPlayers").getComponent(cc.Sprite).fillRange = itemData["userCount"] / itemData["maxUserPerRoom"];
                        //     if (itemData["maxUserPerRoom"] == 500) {
                        //         item.setSiblingIndex(0);
                        //     }*/
                        //     item.active = true;

                        //     item.on("click", () => {
                        //         App.instance.showLoading(true);
                        //         XocDiaNetworkClient.getInstance().send(new cmd.SendJoinRoomById(itemData["id"]));
                        //     });
                        // }
                    }
                    break;
                case cmd.Code.JOIN_ROOM_FAIL:
                    {
                        App.instance.showLoading(false);
                        let res = new cmd.ReceiveJoinRoomFail(data);
                        cc.log(res);
                        let msg = "Lỗi " + res.getError() + ", không xác định.";
                        switch (res.getError()) {
                            case 1:
                                msg = "Lỗi kiểm tra thông tin!";
                                break;
                            case 2:
                                msg = "Không tìm được phòng thích hợp. Vui lòng thử lại sau!";
                                break;
                            case 3:
                                msg = "Bạn không đủ tiền vào phòng chơi này :____!";
                                break;
                            case 4:
                                msg = "Không tìm được phòng thích hợp. Vui lòng thử lại sau!";
                                break;
                            case 5:
                                msg = "Mỗi lần vào phòng phải cách nhau 10 giây!";
                                break;
                            case 6:
                                msg = "Hệ thống bảo trì!";
                                break;
                            case 7:
                                msg = "Không tìm thấy phòng chơi!";
                                break;
                            case 8:
                                msg = "Mật khẩu phòng chơi không đúng!";
                                break;
                            case 9:
                                msg = "Phòng chơi đã đủ người!";
                                break;
                            case 10:
                                msg = "Bạn bị chủ phòng không cho vào bàn!"
                        }
                        App.instance.alertDialog.showMsg(msg);
                    }
                    break;
                case cmd.Code.JOIN_ROOM_SUCCESS:
                    {
                        App.instance.showLoading(false);
                        let res = new cmd.ReceiveJoinRoomSuccess(data);
                        cc.log(res);
                        this.node.active = false;
                        XocDiaController.instance.play.show(res);
                    }
                    break;


                case 3003:
                    {
                        App.instance.showLoading(false);
                        // cc.log("MONEY_BET_CONFIG");
                        let res = new CardGameCmd.ResMoneyBetConfig(data);
                        // cc.log(res);
                        this.listRoom = res.list;
                        this.initRooms(res.list);
                    }
                    break;
                default:
                    console.log("--inpacket.getCmdId(): " + inpacket.getCmdId());
                    break;
            }
        }, this);
    }

    listRoom: any = null;
    initRooms(rooms) {
        let arrBet = [];
        this.listItems.removeAllChildren();
        let id = 0;
        let names = ["San bằng tất cả", "Nhiều tiền thì vào", "Dân chơi", "Bàn cho đại gia", "Tứ quý", "Bốn đôi thông", "Tới trắng", "Chặt heo"];
        for (let i = 0; i < rooms.length; i++) {
            let room = rooms[i];
            id++;
            let isExisted = arrBet.indexOf(room.moneyBet);
            if (isExisted == -1) {
                arrBet.push(room.moneyBet);
            }
        }
        arrBet.sort(function (a, b) {
            return a - b;
        });

        for (let index = 0; index < arrBet.length; index++) {
            let playerCount = 0;
            let maxUser = 0;
            let moneyRequire = 0;
            for (let a = 0; a < rooms.length; a++) {
                let room = rooms[a];
                if (room.moneyBet == arrBet[index]) {
                    playerCount += room.nPersion;
                    maxUser = room.maxUserPerRoom;
                    moneyRequire = room.moneyRequire;
                    // cc.log("room : ", room);
                }
            }
            // cc.log("CardGame_ItemRoom playerCount : ", playerCount);
            XocDiaNetworkClient.getInstance().send(new CardGameCmd.SendJoinRoom(Configs.App.MONEY_TYPE, maxUser, arrBet[index], 0));
            return;
            // var item = cc.instantiate(this.itemTemplate);
            // // item.getChildByName("ID").getComponent(cc.Label).string = (index + 1).toString();
            // // item.getChildByName("bet").getComponent(cc.Label).string = Utils.formatNumber(arrBet[index]);
            // // item.getChildByName("num Players").getComponent(cc.Label).string = playerCount + "/" + maxUser;
            // item.getComponent("ItemRoom").initItem({
            //     id: index + 1,
            //     moneyBet: arrBet[index],
            //     requiredMoney: moneyRequire,
            //     userCount: playerCount,
            //     maxUserPerRoom: maxUser
            // })

            // item.getComponent("ItemRoom").labelNumPlayers.string = playerCount;

            // item.on(cc.Node.EventType.TOUCH_END, (event) => {
            //     XocDiaNetworkClient.getInstance().send(new CardGameCmd.SendJoinRoom(Configs.App.MONEY_TYPE, maxUser, arrBet[index], 0));
            // });
            // item.parent = this.listItems;
        }

        // this.listItems.parent.parent.getComponent(cc.ScrollView).scrollToBottom(0);
        // this.listItems.parent.parent.getComponent(cc.ScrollView).scrollToTop(2);

    }

    public show() {
        this.node.active = true;
        this.actRefesh();
        BroadcastReceiver.send(BroadcastReceiver.USER_UPDATE_COIN);
    }

    public actRefesh() {
        XocDiaNetworkClient.getInstance().send(new CardGameCmd.SendMoneyBetConfig());
    }

    public actBack() {
        XocDiaNetworkClient.getInstance().close();
        App.instance.loadScene("lobby");
    }

    public actCreateTable() {
        App.instance.alertDialog.showMsg("Không thể tạo bàn trong game này.");
    }

    // update (dt) {}
}
