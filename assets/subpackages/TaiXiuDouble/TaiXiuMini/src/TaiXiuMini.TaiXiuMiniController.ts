import cmd from "./TaiXiuMini.Cmd";
import PanelChat from "./TaiXiuMini.PanelChat";
import MiniGame from "../../../../Lobby/src/MiniGame";
import MiniGameNetworkClient from "../../../../scripts/networks/MiniGameNetworkClient";
import InPacket from "../../../../scripts/networks/Network.InPacket";
import Utils from "../../../../scripts/common/Utils";
import Tween from "../../../../scripts/common/Tween";
import Configs from "../../../../scripts/common/Configs";
import BroadcastReceiver from "../../../../scripts/common/BroadcastReceiver";
import App from "../../../../scripts/common/App";
import TaiXiuDoubleController from "../../src/TaiXiuDouble.Controller";
import PopupDetailHistory from "./TaiXiuMini.PopupDetailHistory";
import TaiXiuMiniTanLoc from "./TaiXiuMini.TaiXiuMiniTanLoc";

const { ccclass, property } = cc._decorator;

enum BetDoor {
    None, Tai, Xiu
}

@ccclass
export default class TaiXiuMiniController extends cc.Component {

    static instance: TaiXiuMiniController = null;

    @property(cc.Node)
    gamePlay: cc.Node = null;
    @property([cc.SpriteFrame])
    sprDices: Array<cc.SpriteFrame> = new Array<cc.SpriteFrame>();
    @property(cc.SpriteFrame)
    sprFrameTai: cc.SpriteFrame = null;
    @property(cc.SpriteFrame)
    sprFrameXiu: cc.SpriteFrame = null;
    @property(cc.SpriteFrame)
    sprFrameBtnNan: cc.SpriteFrame = null;
    @property(cc.SpriteFrame)
    sprFrameBtnNan2: cc.SpriteFrame = null;
    @property(cc.Label)
    lblSession: cc.Label = null;
    @property(cc.Label)
    lblRemainTime: cc.Label = null;
    @property(cc.Label)
    lblRemainTime2: cc.Label = null;
    @property(cc.Label)
    lblScore: cc.Label = null;
    @property(cc.Label)
    lblUserTai: cc.Label = null;
    @property(cc.Label)
    lblUserXiu: cc.Label = null;
    @property(cc.Label)
    lblTotalBetTai: cc.Label = null;
    @property(cc.Label)
    lblTotalBetXiu: cc.Label = null;
    @property(cc.Label)
    lblBetTai: cc.Label = null;
    @property(cc.Label)
    lblBetXiu: cc.Label = null;
    @property(cc.Label)
    lblBetedTai: cc.Label = null;
    @property(cc.Label)
    lblBetedXiu: cc.Label = null;
    @property(cc.Sprite)
    dice1: cc.Sprite = null;
    @property(cc.Sprite)
    dice2: cc.Sprite = null;
    @property(cc.Sprite)
    dice3: cc.Sprite = null;
    @property(cc.Node)
    diceAnim: cc.Node = null;
    @property(cc.Node)
    bowl: cc.Node = null;
    @property(sp.Skeleton)
    tai: sp.Skeleton = null;
    @property(sp.Skeleton)
    xiu: sp.Skeleton = null;
    @property(cc.Node)
    btnHistories: cc.Node = null;
    @property(cc.Node)
    nodePanelChat: cc.Node = null;
    @property(cc.Node)
    layoutBet: cc.Node = null;
    @property(cc.Node)
    layoutBet1: cc.Node = null;
    @property(cc.Node)
    layoutBet2: cc.Node = null;
    @property([cc.Button])
    buttonsBet1: Array<cc.Button> = new Array<cc.Button>();
    @property([cc.Button])
    buttonsBet2: Array<cc.Button> = new Array<cc.Button>();
    @property(cc.Label)
    lblToast: cc.Label = null;
    @property(cc.Label)
    lblWinCash: cc.Label = null;
    @property(cc.Node)
    btnNan: cc.Node = null;

    @property(PopupDetailHistory)
    popupDetailHistory: PopupDetailHistory = null;

    @property([cc.Node])
    public popups: cc.Node[] = [];

    @property(cc.Label)
    rutLocConLaiLb: cc.Label = null;

    @property(cc.Label)
    huLocLb: cc.Label = null;

    @property(cc.Node)
    timeRutLocNode: cc.Node = null;

    @property(cc.Node)
    btnRutLoc: cc.Node = null;



    private isBetting = false;
    private remainTime = 0;
    private canBet = true;
    private betedTai = 0;
    private betedXiu = 0;
    private referenceId = 0;
    private betingValue = -1;
    private betingDoor = BetDoor.None;
    private isOpenBowl = false;
    private lastWinCash = 0;
    private lastScore = 0;
    private isNan = false;
    histories = [];
    private isCanChat = true;
    private panelChat: PanelChat = null;
    private readonly maxBetValue = 999999999;
    private listBets = [1000, 5000, 10000, 50000, 100000, 500000, 1000000, 10000000];
    private readonly bowlStartPos = cc.v2(247.222, -1.544);

    onLoad() {
        TaiXiuMiniController.instance = this;
    }

    start() {
        MiniGameNetworkClient.getInstance().addListener((data: Uint8Array) => {
            if (!this.node.active) return;
            let inpacket = new InPacket(data);
            switch (inpacket.getCmdId()) {

                case cmd.Code.GAME_INFO: {
                    let res = new cmd.ReceiveGameInfo(data);
                    // cc.log("GAME_INFO", res)
                    this.stopWin();
                    this.bowl.active = false;
                    if (res.bettingState) {
                        this.isBetting = true;
                        this.dice1.node.active = false;
                        this.dice2.node.active = false;
                        this.dice3.node.active = false;
                        this.lblRemainTime.node.active = true;
                        this.lblRemainTime.string = res.remainTime < 10 ? "0" + res.remainTime : "" + res.remainTime;
                        this.lblRemainTime2.node.parent.active = false;
                        this.lblScore.node.parent.active = false;
                    } else {
                        this.lastScore = res.dice1 + res.dice2 + res.dice3;
                        this.isBetting = false;
                        this.dice1.node.active = true;
                        this.dice1.spriteFrame = this.sprDices[res.dice1];
                        this.dice2.node.active = true;
                        this.dice2.spriteFrame = this.sprDices[res.dice2];
                        this.dice3.node.active = true;
                        this.dice3.spriteFrame = this.sprDices[res.dice3];
                        this.lblRemainTime.node.active = false;
                        this.lblRemainTime2.node.parent.active = true;
                        this.lblRemainTime2.string = "00:" + (res.remainTime < 10 ? "0" + res.remainTime : "" + res.remainTime);
                        this.showResult();
                    }
                    this.diceAnim.active = false;
                    Tween.numberTo(this.lblTotalBetTai, res.potTai, 0.3);
                    Tween.numberTo(this.lblTotalBetXiu, res.potXiu, 0.3);
                    this.betedTai = res.betTai;
                    this.lblBetedTai.string = Utils.formatNumber(this.betedTai);
                    this.betedXiu = res.betXiu;
                    this.lblBetedXiu.string = Utils.formatNumber(this.betedXiu);
                    this.referenceId = res.referenceId;
                    this.lblSession.string = "#" + res.referenceId;
                    this.remainTime = res.remainTime;
                    break;
                }
                case cmd.Code.UPDATE_TIME: {
                    let res = new cmd.ReceiveUpdateTime(data);
                    if (res.bettingState) {
                        this.isBetting = true;
                        this.lblRemainTime.node.active = true;
                        this.lblRemainTime.string = res.remainTime < 10 ? "0" + res.remainTime : "" + res.remainTime;
                        this.lblRemainTime2.node.parent.active = false;
                        this.lblScore.node.parent.active = false;

                        if (this.dice1.node.active) {
                            this.dice1.node.active = false;
                            this.dice2.node.active = false;
                            this.dice3.node.active = false;
                        }
                    } else {
                        this.isBetting = false;
                        this.lblRemainTime.node.active = false;
                        this.lblRemainTime2.node.parent.active = true;
                        this.lblRemainTime2.string = "00:" + (res.remainTime < 10 ? "0" + res.remainTime : "" + res.remainTime);
                        this.layoutBet.active = false;
                        this.lblBetTai.string = "ĐẶT";
                        this.lblBetXiu.string = "ĐẶT";
                        if (res.remainTime < 15 && this.isNan && !this.isOpenBowl) {
                            this.bowl.active = false;
                            this.showResult();
                            this.showWinCash();
                            this.isOpenBowl = true;
                        }
                    }
                    Tween.numberTo(this.lblTotalBetTai, res.potTai, 0.3);
                    Tween.numberTo(this.lblTotalBetXiu, res.potXiu, 0.3);
                    this.lblUserTai.string = "(" + Utils.formatNumber(res.numBetTai) + ")";
                    this.lblUserXiu.string = "(" + Utils.formatNumber(res.numBetXiu) + ")";
                    break;
                }
                case cmd.Code.DICES_RESULT: {
                    let res = new cmd.ReceiveDicesResult(data);
                    this.lastScore = res.dice1 + res.dice2 + res.dice3;
                    this.lblRemainTime.node.active = false;
                    this.dice1.spriteFrame = this.sprDices[res.dice1];
                    this.dice2.spriteFrame = this.sprDices[res.dice2];
                    this.dice3.spriteFrame = this.sprDices[res.dice3];

                    this.scheduleOnce(() => {
                        this.diceAnim.active = true;
                        this.diceAnim.getComponent(sp.Skeleton).setAnimation(0, "animation", false);
                        this.scheduleOnce(() => {
                            this.diceAnim.active = false;
                            this.dice1.node.active = true;
                            this.dice2.node.active = true;
                            this.dice3.node.active = true;

                            if (!this.isNan) {
                                this.showResult();
                            } else {
                                this.bowl.position = this.bowlStartPos;
                                this.bowl.active = true;
                            }
                        }, 1.45);
                    }, 0.1);

                    if (this.histories.length >= 100) {
                        this.histories.slice(0, 1);
                    }
                    this.histories.push({
                        "session": this.referenceId,
                        "dices": [
                            res.dice1,
                            res.dice2,
                            res.dice3
                        ]
                    });
                    break;
                }
                case cmd.Code.RESULT: {
                    let res = new cmd.ReceiveResult(data);
                    // console.log(res);
                    Configs.Login.Coin = res.currentMoney;
                    this.lastWinCash = res.totalMoney;
                    if (!this.bowl.active) {
                        if (res.totalMoney > 0) this.showWinCash();
                    }
                    break;
                }
                case cmd.Code.NEW_GAME: {
                    let res = new cmd.ReceiveNewGame(data);
                    // cc.log("NEW_GAME", res)
                    this.diceAnim.active = false;
                    this.dice1.node.active = false;
                    this.dice2.node.active = false;
                    this.dice3.node.active = false;
                    this.lblTotalBetTai.string = "0";
                    this.lblTotalBetXiu.string = "0";
                    this.lblBetedTai.string = "0";
                    this.lblBetedXiu.string = "0";
                    this.lblUserTai.string = "(0)";
                    this.lblUserXiu.string = "(0)";
                    this.referenceId = res.referenceId;
                    this.lblSession.string = "#" + res.referenceId;
                    this.betingValue = -1;
                    this.betingDoor = BetDoor.None;
                    this.betedTai = 0;
                    this.betedXiu = 0;
                    this.isOpenBowl = false;
                    this.lastWinCash = 0;
                    this.stopWin();
                    break;
                }
                case cmd.Code.HISTORIES: {
                    let res = new cmd.ReceiveHistories(data);
                    var his = res.data.split(",");
                    for (var i = 0; i < his.length; i++) {
                        this.histories.push({
                            "session": this.referenceId - his.length / 3 + parseInt("" + ((i + 1) / 3)) + (this.isBetting ? 0 : 1),
                            "dices": [
                                parseInt(his[i]),
                                parseInt(his[++i]),
                                parseInt(his[++i])
                            ]
                        });
                    }
                    this.updateBtnHistories();
                    break;
                }
                case cmd.Code.LOG_CHAT: {
                    let res = new cmd.ReceiveLogChat(data);
                    // cc.log(res);
                    var msgs = JSON.parse(res.message);
                    for (var i = 0; i < msgs.length; i++) {
                        this.panelChat.addMessage(msgs[i]["u"], msgs[i]["m"]);
                    }
                    this.panelChat.scrollToBottom();
                    break;
                }
                case cmd.Code.SEND_CHAT: {
                    let res = new cmd.ReceiveSendChat(data);
                    // cc.log(res);
                    switch (res.error) {
                        case 0:
                            this.panelChat.addMessage(res.nickname, res.message);
                            break;
                        case 2:
                            App.instance.toast.showToast("Bạn không có quyền Chat!");
                            break;
                        case 3:
                            App.instance.toast.showToast("Tạm thời bạn bị cấm Chat!");
                            break;
                        case 4:
                            App.instance.toast.showToast("Nội dung chat quá dài.");
                            break;
                        default:
                            App.instance.toast.showToast("Bạn không thể chat vào lúc này.");
                            break;
                    }
                    // console.log(res);
                    break;
                }
                case cmd.Code.BET: {
                    let res = new cmd.ReceiveBet(data);
                    switch (res.result) {
                        case 0:
                            switch (this.betingDoor) {
                                case BetDoor.Tai:
                                    this.betedTai += this.betingValue;
                                    this.lblBetedTai.string = Utils.formatNumber(this.betedTai);
                                    break;
                                case BetDoor.Xiu:
                                    this.betedXiu += this.betingValue;
                                    this.lblBetedXiu.string = Utils.formatNumber(this.betedXiu);
                                    break;
                            }
                            Configs.Login.Coin = res.currentMoney;
                            BroadcastReceiver.send(BroadcastReceiver.USER_UPDATE_COIN);

                            this.betingValue = -1;
                            App.instance.toast.showToast("Đặt cược thành công.");
                            break;
                        case 2:
                            this.betingValue = -1;
                            App.instance.toast.showToast("Hết thời gian cược.");
                            break;
                        case 3:
                            this.betingValue = -1;
                            App.instance.toast.showToast("Số dư không đủ vui lòng nạp thêm.");
                            break;
                        case 4:
                            this.betingValue = -1;
                            App.instance.toast.showToast("Số tiền cược không hợp lệ.");
                            break;
                        default:
                            this.betingValue = -1;
                            App.instance.toast.showToast("Đặt cược không thành công.");
                            break;
                    }
                    break;
                }

                case cmd.Code.TX_TAN_LOC:
                    {
                        let res = new cmd.CmdTXTanLoc(data);
                        this.responseTanLoc(res.result, res.currentMoney);
                    }
                    break;
                case cmd.Code.TX_RUT_LOC:
                    {
                        var res = new cmd.CmdTXRutLoc(data);
                        this.responseRutLoc(res.prize, res.currentMoney);
                    }
                    break;

                case cmd.Code.UPDATE_QUY_LOC:
                    {
                        let res = new cmd.CmdTXUpdateQuyLoc(data);
                        this.responseUpdateHuLoc(res.value);
                    }

                    break;

                case cmd.Code.START_NEW_ROUND_RUT_LOC:
                    {
                        let res = new cmd.CmdTXStartRutLoc(data);
                        this.responseStartRutLoc(res.remainTime);
                    }

                    break;

                case cmd.Code.ENABLE_RUT_LOC:
                    this.timeRutLoc = 30;
                    this.btnRutLoc.runAction(cc.repeatForever(cc.sequence(cc.scaleTo(0.15, 1.1, 1.1), cc.scaleTo(0.15, 1, 1))));
                    // this.rutLocConLaiLb.node.parent.active = true;
                    break;

                case cmd.Code.UPDATE_LUOT_RUT_LOC:
                    {
                        let res = new cmd.CmdTXUpdateSoLuotRutLoc(data);
                        this.responseUpdateLuotRutLoc(res.soLuotRut);
                    }
                    break;

                default:
                    // console.log(inpacket.getCmdId());
                    break;
            }
        }, this);
        for (let i = 0; i < this.buttonsBet1.length; i++) {
            let btn = this.buttonsBet1[i];
            let value = this.listBets[i];
            let strValue = value + "";
            if (value >= 1000000) {
                strValue = (value / 1000000) + "M";
            } else if (value >= 1000) {
                strValue = (value / 1000) + "K";
            }
            btn.getComponentInChildren(cc.Label).string = strValue;
            btn.node.on("click", () => {
                if (this.betingDoor === BetDoor.None) return;
                let lblBet = this.betingDoor === BetDoor.Tai ? this.lblBetTai : this.lblBetXiu;
                let number = Utils.stringToInt(lblBet.string) + value;
                if (number > this.maxBetValue) number = this.maxBetValue;
                lblBet.string = Utils.formatNumber(number);
            });
        }
        for (let i = 0; i < this.buttonsBet2.length; i++) {
            let btn = this.buttonsBet2[i];
            let value = btn.getComponentInChildren(cc.Label).string;
            btn.node.on("click", () => {
                if (this.betingDoor === BetDoor.None) return;
                let lblBet = this.betingDoor === BetDoor.Tai ? this.lblBetTai : this.lblBetXiu;
                let number = Utils.stringToInt(lblBet.string + value);
                if (number > this.maxBetValue) number = this.maxBetValue;
                lblBet.string = Utils.formatNumber(number);
            });
        }

        this.bowl.on(cc.Node.EventType.TOUCH_MOVE, (event: cc.Event.EventTouch) => {
            var pos = this.bowl.position;
            pos.x += event.getDeltaX();
            pos.y += event.getDeltaY();
            this.bowl.position = pos;

            let distance = Utils.v2Distance(pos, this.bowlStartPos);
            // console.log(distance);
            if (Math.abs(distance) > 300) {
                this.bowl.active = false;
                this.isOpenBowl = true;
                this.showResult();
                this.showWinCash();
            }
        }, this);
    }

    show() {
        App.instance.buttonMiniGame.showTimeTaiXiu(false);
        this.layoutBet.active = false;
        this.lblToast.node.parent.active = false;
        this.lblWinCash.node.active = false;
        this.layoutBet.active = false;
        this.diceAnim.active = false;
        this.bowl.active = false;
        this.dice1.node.active = false;
        this.dice2.node.active = false;
        this.dice3.node.active = false;
        MiniGameNetworkClient.getInstance().send(new cmd.SendScribe());
        this.showChat();
    }

    showChat() {
        this.panelChat = this.nodePanelChat.getComponent(PanelChat);
        this.panelChat.show(true);
    }

    dismiss() {
        for (let i = 0; i < this.popups.length; i++) {
            this.popups[i].active = false;
        }
        this.panelChat.show(false);
        // MiniGameNetworkClient.getInstance().send(new cmd.SendUnScribe());
    }

    actClose() {
        TaiXiuDoubleController.instance.dismiss();
    }

    actChat() {
        this.panelChat.show(!this.panelChat.node.active);
    }

    actBetTai() {
        if (!this.isBetting) {
            App.instance.toast.showToast("Chưa đến thời gian đặt cược.");
            return;
        }
        if (this.betingValue >= 0) {
            App.instance.toast.showToast("Bạn thao tác quá nhanh.");
            return;
        }
        if (this.betedXiu > 0) {
            App.instance.toast.showToast("Bạn không thể đặt 2 cửa.");
            return;
        }
        this.betingDoor = BetDoor.Tai;
        this.lblBetTai.string = "0";
        this.lblBetXiu.string = "ĐẶT";
        this.layoutBet.active = true;
        this.layoutBet1.active = true;
        this.layoutBet2.active = false;
    }

    actBetXiu() {
        if (!this.isBetting) {
            App.instance.toast.showToast("Chưa đến thời gian đặt cược.");
            return;
        }
        if (this.betingValue >= 0) {
            App.instance.toast.showToast("Bạn thao tác quá nhanh.");
            return;
        }
        if (this.betedTai > 0) {
            App.instance.toast.showToast("Bạn không thể đặt 2 cửa.");
            return;
        }
        this.betingDoor = BetDoor.Xiu;
        this.lblBetXiu.string = "0";
        this.lblBetTai.string = "ĐẶT";
        this.layoutBet.active = true;
        this.layoutBet1.active = true;
        this.layoutBet2.active = false;
    }

    actOtherNumber() {
        this.layoutBet1.active = false;
        this.layoutBet2.active = true;
    }

    actAgree() {
        if (this.betingValue >= 0 || !this.canBet) {
            App.instance.toast.showToast("Bạn thao tác quá nhanh.");
            return;
        }
        if (this.betingDoor === BetDoor.None) return;
        var lblBet = this.betingDoor === BetDoor.Tai ? this.lblBetTai : this.lblBetXiu;
        this.betingValue = Utils.stringToInt(lblBet.string);
        this.betingDoor = this.betingDoor;
        MiniGameNetworkClient.getInstance().send(new cmd.SendBet(this.referenceId, this.betingValue, this.betingDoor == BetDoor.Tai ? 1 : 0, this.remainTime));
        lblBet.string = "0";

        this.canBet = false;
        this.scheduleOnce(function () {
            this.canBet = true;
        }, 1);
    }

    actCancel() {
        this.lblBetXiu.string = "ĐẶT";
        this.lblBetTai.string = "ĐẶT";
        this.betingDoor = BetDoor.None;
        this.layoutBet.active = false;
    }

    actBtnGapDoi() {
        if (this.betingDoor === BetDoor.None) return;
        var lblBet = this.betingDoor === BetDoor.Tai ? this.lblBetTai : this.lblBetXiu;
        var number = Utils.stringToInt(lblBet.string) * 2;
        if (number > this.maxBetValue) number = this.maxBetValue;
        lblBet.string = Utils.formatNumber(number);
    }

    actBtnDelete() {
        if (this.betingDoor === BetDoor.None) return;
        var lblBet = this.betingDoor === BetDoor.Tai ? this.lblBetTai : this.lblBetXiu;
        var number = "" + Utils.stringToInt(lblBet.string);
        number = number.substring(0, number.length - 1);
        number = Utils.formatNumber(Utils.stringToInt(number));
        lblBet.string = number;
    }

    actBtn000() {
        if (this.betingDoor === BetDoor.None) return;
        var lblBet = this.betingDoor === BetDoor.Tai ? this.lblBetTai : this.lblBetXiu;
        var number = Utils.stringToInt(lblBet.string + "000");
        if (number > this.maxBetValue) number = this.maxBetValue;
        lblBet.string = Utils.formatNumber(number);
    }

    actNan() {
        this.isNan = !this.isNan;
        this.btnNan.getComponent(cc.Sprite).spriteFrame = this.isNan ? this.sprFrameBtnNan : this.sprFrameBtnNan2;
        if (this.isNan) {
            App.instance.toast.showToast("Đã bật chế độ Nặn");
        } else {
            App.instance.toast.showToast("Đã tắt chế độ Nặn");
        }
    }

    private showResult() {
        this.lblScore.node.parent.active = true;
        this.lblScore.string = "" + this.lastScore;
        if (this.lastScore >= 11) {
            this.tai.setAnimation(0, "action2", true);
        } else {
            this.xiu.setAnimation(0, "action2", true);
        }
        this.updateBtnHistories();
    }

    private stopWin() {
        // this.tai.clearTrack(0);
        // this.xiu.clearTrack(0);
        this.tai.setAnimation(0, "action1", true);
        this.xiu.setAnimation(0, "action1", true);
    }

    private showToast(message: string) {
        this.lblToast.string = message;
        let parent = this.lblToast.node.parent;
        parent.stopAllActions();
        parent.active = true;
        parent.opacity = 0;
        parent.runAction(cc.sequence(cc.fadeIn(0.1), cc.delayTime(2), cc.fadeOut(0.2), cc.callFunc(() => {
            parent.active = false;
        })));
    }

    private showWinCash() {
        if (this.lastWinCash <= 0) return;
        this.lblWinCash.node.stopAllActions();
        this.lblWinCash.node.active = true;
        this.lblWinCash.node.scale = 0;
        this.lblWinCash.node.position = cc.Vec2.ZERO;
        App.instance.toast.showToast("Chúc mừng bạn đã thắng " + Utils.formatNumber(this.lastWinCash));
        // Tween.numberTo(this.lblWinCash, this.lastWinCash, 0.5, (n) => { return "+" + n });
        // this.lblWinCash.node.runAction(cc.sequence(
        //     cc.scaleTo(0.5, 1),
        //     cc.delayTime(2),
        //     cc.moveBy(1, cc.v2(0, 60)),
        //     cc.callFunc(() => {
        //         this.lblWinCash.node.active = false;
        //     })
        // ));
        BroadcastReceiver.send(BroadcastReceiver.USER_UPDATE_COIN);
    }

    updateBtnHistories() {
        let histories = this.histories.slice();
        if (histories.length > this.btnHistories.childrenCount) {
            histories.splice(0, histories.length - this.btnHistories.childrenCount);
        }
        let idx = histories.length - 1;
        for (var i = this.btnHistories.childrenCount - 1; i >= 0; i--) {
            if (idx >= 0) {
                let _idx = idx;
                var score = histories[idx]["dices"][0] + histories[idx]["dices"][1] + histories[idx]["dices"][2];
                this.btnHistories.children[i].getComponent(cc.Sprite).spriteFrame = score >= 11 ? this.sprFrameTai : this.sprFrameXiu;
                this.btnHistories.children[i].off("click");
                this.btnHistories.children[i].on("click", (e, b) => {
                    this.popupDetailHistory.showDetail(histories[_idx]["session"]);
                    console.log(this.histories[_idx]);
                });
                this.btnHistories.children[i].active = true;
            } else {
                this.btnHistories.children[i].active = false;
            }
            idx--;
        }
    }

    sendChat(message: string) {
        let _this = this;
        if (!_this.isCanChat) {
            App.instance.toast.showToast("Bạn thao tác quá nhanh.");
            return;
        }
        _this.isCanChat = false;
        this.scheduleOnce(function () {
            _this.isCanChat = true;
        }, 1);
        var req = new cmd.SendChat(unescape(encodeURIComponent(message)));
        MiniGameNetworkClient.getInstance().send(req);
    }

    soLanRutLoc: number = 0;
    sendRutLoc() {
        if (this.soLanRutLoc > 0) {
            MiniGameNetworkClient.getInstance().send(new cmd.CmdSendRutLoc());
        } else {
            App.instance.toast.showToast('Bạn đã hết lượt rút lộc');
        }
    }

    responseUpdateLuotRutLoc(soLuot) {
        this.soLanRutLoc = soLuot;
        this.rutLocConLaiLb.string = "Số lần rút còn lại : " + soLuot;
    }

    responseTanLoc(result, currentMoney) {
        Configs.Login.Coin = currentMoney;
        BroadcastReceiver.send(BroadcastReceiver.USER_UPDATE_COIN);
        if (result == 0) {
            App.instance.alertDialog.showMsg('Tán lộc thành công!');
        } else if (result == 1) {
            App.instance.alertDialog.showMsg('Tán lộc không thành công!');
        } else if (result = 2) {
            App.instance.alertDialog.showMsg('Bạn chưa có đủ số dư Lux');
        } else if (result = 3) {
            App.instance.alertDialog.showMsg('Tán lộc phải lớn hơn 1.000 Xu');
        }
    }

    responseRutLoc(prize, currentMoney) {
        Configs.Login.Coin = currentMoney;
        BroadcastReceiver.send(BroadcastReceiver.USER_UPDATE_COIN);
        if (prize == -1) {
            App.instance.alertDialog.showMsg('Rút lộc không thành công! Vui lòng chờ ' + (this.timeRutLoc - 30) + ' giây nữa.');
        } else if (prize == 0) {
            App.instance.alertDialog.showMsg('Chúc bạn may mắn lần sau!');
        } else if (prize > 0) {
            App.instance.alertDialog.showMsg('Bạn rút lộc được ' + Utils.formatNumber(prize) + ' Xu.');
        }
        else if (prize == -3) {
            App.instance.alertDialog.showMsg('Quỹ lộc không đủ.');
        }
        else if (prize == -2) {
            App.instance.alertDialog.showMsg('Bạn đã hết lượt rút lộc.');
        }
    }

    responseUpdateHuLoc(value) {
        this.huLocLb.string = Utils.formatNumber(value);
    }

    timeRutLoc: number = 0;
    responseStartRutLoc(remainTime) {
        this.timeRutLoc = remainTime;
        this.btnRutLoc.stopAllActions();
        this.btnRutLoc.scale = 1;
        this.updateTimeRutLoc();
    }

    updateTimeRutLoc() {
        // if (this.timeRutLoc > 30) {
        //     this.timeRutLocNode.active = true;
        //     this.timeRutLocNode.getChildByName("txt").getComponent(cc.Label).string = this.converToTime(this.timeRutLoc - 30);
        // }
        // else {
        //     this.timeRutLocNode.active = false;
        // }
    }

    converToTime(remainTime: number) {
        var mm = parseInt((remainTime / 60).toString());
        var ss = remainTime % 60;
        return (mm < 10 ? "0" + mm : mm) + ":" + (ss < 10 ? "0" + ss : ss);
    }

    activeHuLoc() {
        if (this.rutLocConLaiLb.node.parent.active) {
            this.rutLocConLaiLb.node.parent.active = false;
        } else {
            this.rutLocConLaiLb.node.parent.active = true;
        }
    }
}