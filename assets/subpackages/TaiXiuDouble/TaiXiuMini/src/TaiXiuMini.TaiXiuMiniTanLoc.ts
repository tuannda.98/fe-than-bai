import App from "../../../../scripts/common/App";
import Configs from "../../../../scripts/common/Configs";
import Dialog from "../../../../scripts/common/Dialog";
import Utils from "../../../../scripts/common/Utils";
import MiniGameNetworkClient from "../../../../scripts/networks/MiniGameNetworkClient";
import cmd from "./TaiXiuMini.Cmd";

const { ccclass, property } = cc._decorator;

@ccclass
export default class TaiXiuMiniTanLoc extends Dialog {

    @property(cc.Label)
    moneyLb: cc.Label = null;

    money: number = 0;

    show() {
        super.show()
        this.money = 0;
        this.moneyLb.string = "0";
        this.node.active = true;
    }

    onClick(event, eventData) {
        this.money += Number(eventData);
        this.moneyLb.string = Utils.formatNumber(this.money);
    }

    tanLocClick() {
        if (Configs.Login.Coin < this.money) {
            App.instance.alertDialog.showMsg('Số tiền không đủ');
            return;
        }
        if (this.money < 1000) {
            App.instance.alertDialog.showMsg('Tán lộc phải lớn hơn 1.000 Xu')
        } else {
            MiniGameNetworkClient.getInstance().send(new cmd.CmdSendTanLoc(this.money));
            this.money = 0;
            this.moneyLb.string = "0";
        }
    }

    clearMoney() {
        this.money = 0;
        this.moneyLb.string = "0";
    }

}
