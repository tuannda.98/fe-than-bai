const { ccclass, property } = cc._decorator;

@ccclass
export default class ButtonPayBet extends cc.Component {
    @property(cc.Button)
    button: cc.Button = null;
    @property(cc.Label)
    lblTotal: cc.Label = null;
    @property(cc.Label)
    lblBeted: cc.Label = null;
    @property(cc.Node)
    overlay: cc.Node = null;
    @property(cc.Label)
    lblFactor: cc.Label = null;
    @property(cc.Node)
    effWin: cc.Node = null;
    @property(sp.Skeleton)
    effIcon: sp.Skeleton = null;
    @property(cc.Node)
    icon: cc.Node = null;

    onDisable() {
        this.effWin.stopAllActions();
        this.effWin.active = false;
        this.effIcon.node.active = false;
        this.icon.active = true;
    }

    setWin(isWin = false) {
        if (isWin) {
            this.effWin.active = true;
            this.effWin.runAction(cc.repeatForever(cc.rotateBy(0.5,-360)));
            this.effIcon.node.active = true;
            this.effIcon.setAnimation(0, "animation", true);
            this.icon.active = false;
        } else {
            this.effWin.stopAllActions();
            this.effWin.active = false;
            this.effIcon.node.active = false;
            this.icon.active = true;
        }
    }
}