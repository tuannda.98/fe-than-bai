
Medusa_Jackpot.png
size: 2048,2048
format: RGBA8888
filter: Linear,Linear
repeat: none
FX/Comp 1/FXSmoke_00000
  rotate: false
  xy: 677, 417
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: -1
FX/Comp 1/FXSmoke_00001
  rotate: false
  xy: 578, 243
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: -1
FX/Comp 1/FXSmoke_00002
  rotate: false
  xy: 578, 91
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: -1
FX/Comp 1/FXSmoke_00003
  rotate: false
  xy: 730, 265
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: -1
FX/Comp 1/FXSmoke_00004
  rotate: false
  xy: 730, 113
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: -1
FX/Comp 1/FXSmoke_00005
  rotate: false
  xy: 1317, 832
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: -1
FX/Comp 1/FXSmoke_00006
  rotate: false
  xy: 1093, 674
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: -1
FX/Comp 1/FXSmoke_00007
  rotate: false
  xy: 1245, 680
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: -1
FX/Comp 1/FXSmoke_00008
  rotate: false
  xy: 1397, 680
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: -1
FX/Comp 1/FXSmoke_00009
  rotate: false
  xy: 829, 466
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: -1
FX/Comp 1/FXSmoke_00010
  rotate: false
  xy: 981, 478
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: -1
FX/Comp 1/FXSmoke_00011
  rotate: false
  xy: 1133, 522
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: -1
FX/Comp 1/FXSmoke_00012
  rotate: false
  xy: 1285, 528
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: -1
FX/Comp 1/FXSmoke_00013
  rotate: false
  xy: 1437, 528
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: -1
FX/Comp 1/FXSmoke_00014
  rotate: false
  xy: 882, 314
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: -1
FX/Comp 1/FXSmoke_00015
  rotate: false
  xy: 882, 162
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: -1
FX/Comp 1/FXSmoke_00016
  rotate: false
  xy: 1034, 326
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: -1
FX/Comp 1/FXSmoke_00017
  rotate: false
  xy: 1034, 174
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: -1
FX/Comp 1/FXSmoke_00018
  rotate: false
  xy: 1186, 370
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: -1
FX/Comp 1/FXSmoke_00019
  rotate: false
  xy: 1186, 218
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: -1
FX/Comp 1/FXSmoke_00020
  rotate: false
  xy: 1338, 376
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: -1
FX/Comp 1/FXSmoke_00021
  rotate: false
  xy: 1338, 224
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: -1
FX/Comp 1/FXSmoke_00022
  rotate: false
  xy: 1490, 376
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: -1
FX/Comp 1/FXSmoke_00023
  rotate: false
  xy: 1490, 224
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: -1
FX/Comp 1/FXSmoke_00024
  rotate: false
  xy: 882, 10
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: -1
FX/Comp 1/FXSmoke_00025
  rotate: false
  xy: 1034, 22
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: -1
FX/Comp 1/FXSmoke_00026
  rotate: false
  xy: 1186, 66
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: -1
FX/Comp 1/FXSmoke_00027
  rotate: false
  xy: 1338, 72
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: -1
FX/Comp 1/FXSmoke_00028
  rotate: false
  xy: 1490, 72
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: -1
FX/Comp 1/FXSmoke_00029
  rotate: false
  xy: 1789, 1364
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: -1
FX/Comp 1/FXSmoke_00030
  rotate: false
  xy: 1789, 1212
  size: 150, 150
  orig: 150, 150
  offset: 0, 0
  index: -1
FX_medu
  rotate: false
  xy: 1651, 1676
  size: 383, 370
  orig: 383, 370
  offset: 0, 0
  index: -1
FaceMeduSa
  rotate: true
  xy: 2, 87
  size: 459, 574
  orig: 459, 574
  offset: 0, 0
  index: -1
Hair medisa
  rotate: false
  xy: 2, 1272
  size: 804, 774
  orig: 804, 774
  offset: 0, 0
  index: -1
Layer 1
  rotate: false
  xy: 270, 42
  size: 88, 43
  orig: 88, 43
  offset: 0, 0
  index: -1
Layer 2
  rotate: false
  xy: 173, 41
  size: 95, 44
  orig: 95, 44
  offset: 0, 0
  index: -1
MedusaJackpot/Layer 1
  rotate: true
  xy: 643, 569
  size: 701, 156
  orig: 701, 156
  offset: 0, 0
  index: -1
MedusaJackpot/Layer 2
  rotate: false
  xy: 1034, 2
  size: 14, 18
  orig: 14, 18
  offset: 0, 0
  index: -1
MedusaJackpot/Shape 11
  rotate: false
  xy: 808, 1154
  size: 577, 129
  orig: 577, 129
  offset: 0, 0
  index: -1
MedusaJackpot/jackpot
  rotate: false
  xy: 1622, 1300
  size: 165, 40
  orig: 165, 40
  offset: 0, 0
  index: -1
MedusaJackpot/jackpot1
  rotate: false
  xy: 2, 41
  size: 169, 44
  orig: 169, 44
  offset: 0, 0
  index: -1
MedusaJackpot/mds copy 2
  rotate: false
  xy: 1622, 1164
  size: 144, 134
  orig: 144, 134
  offset: 0, 0
  index: -1
MedusaJackpot/mds copy 3
  rotate: false
  xy: 1768, 1076
  size: 144, 134
  orig: 144, 134
  offset: 0, 0
  index: -1
snake1
  rotate: true
  xy: 1150, 837
  size: 145, 165
  orig: 145, 165
  offset: 0, 0
  index: -1
snake10
  rotate: true
  xy: 953, 826
  size: 165, 195
  orig: 165, 195
  offset: 0, 0
  index: -1
snake11
  rotate: false
  xy: 1814, 1538
  size: 177, 136
  orig: 177, 136
  offset: 0, 0
  index: -1
snake12
  rotate: false
  xy: 578, 395
  size: 97, 151
  orig: 97, 151
  offset: 0, 0
  index: -1
snake13
  rotate: false
  xy: 1394, 1441
  size: 226, 234
  orig: 226, 234
  offset: 0, 0
  index: -1
snake14
  rotate: false
  xy: 1394, 1290
  size: 226, 149
  orig: 226, 149
  offset: 0, 0
  index: -1
snake15
  rotate: true
  xy: 808, 1677
  size: 369, 841
  orig: 369, 841
  offset: 0, 0
  index: -1
snake16
  rotate: false
  xy: 2, 548
  size: 639, 722
  orig: 639, 722
  offset: 0, 0
  index: -1
snake2
  rotate: false
  xy: 934, 630
  size: 157, 194
  orig: 157, 194
  offset: 0, 0
  index: -1
snake3
  rotate: false
  xy: 1622, 1342
  size: 165, 172
  orig: 165, 172
  offset: 0, 0
  index: -1
snake4
  rotate: false
  xy: 808, 1285
  size: 584, 390
  orig: 584, 390
  offset: 0, 0
  index: -1
snake5
  rotate: false
  xy: 801, 618
  size: 131, 230
  orig: 131, 230
  offset: 0, 0
  index: -1
snake6
  rotate: true
  xy: 1187, 984
  size: 168, 195
  orig: 168, 195
  offset: 0, 0
  index: -1
snake7
  rotate: false
  xy: 953, 993
  size: 232, 159
  orig: 232, 159
  offset: 0, 0
  index: -1
snake8
  rotate: false
  xy: 1622, 1516
  size: 190, 158
  orig: 190, 158
  offset: 0, 0
  index: -1
snake9
  rotate: false
  xy: 801, 850
  size: 150, 302
  orig: 150, 302
  offset: 0, 0
  index: -1
