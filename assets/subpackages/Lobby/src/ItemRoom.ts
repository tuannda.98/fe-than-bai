import BaiCaoController from "../../BaiCao/src/BaiCao.Controller";
import BaCayController from "../../BaCay/src/BaCay.Controller";
import Utils from "../../../scripts/common/Utils";
import MauBinhController from "../../MauBinh/src/MauBinh.Controller";
import PokerController from "../../Poker/src/Poker.Controller";
import Room from "../../Sam/src/Sam.Room";
const { ccclass, property } = cc._decorator;
export namespace GameNameConfig {
	export class NameGame {
		static BaiCao = 1;
		static BaCay = 2;
		static MauBinh = 3;
		static PoKer = 4;
		static Sam = 5;
		static TienLen = 6;
	}
}
@ccclass
export default class ItemRoom extends cc.Component {

	@property(cc.Label)
	labelId: cc.Label = null;
	@property(cc.Label)
	labelBet: cc.Label = null;
	@property(cc.Label)
	labelBetMin: cc.Label = null;
	@property(cc.Label)
	labelNumPlayers: cc.Label = null;
	@property(cc.Sprite)
	progressNumPlayers: cc.Sprite = null;

	private roomInfo = null;
	private game;

	initItem(info, nameGame) {
		this.roomInfo = info;
		this.game = nameGame;
		if (info["id"] != null)
			this.labelId.string = info["id"] == null ? "" : Utils.formatNumber(info["id"]);
		this.labelBet.string = Utils.formatNumber(info["moneyBet"]);
		this.labelBetMin.string = Utils.formatNumber(info["requiredMoney"]);
		if (info["userCount"] == null) return;
		this.labelNumPlayers.string = info["userCount"];// + "/" + info["maxUserPerRoom"];
		this.progressNumPlayers.fillRange = info["userCount"];// / info["maxUserPerRoom"];
	}

	chooseRoom() {
		switch (this.game) {
			case GameNameConfig.NameGame.BaiCao:
				BaiCaoController.instance.joinRoom(this.roomInfo);
				break;
			case GameNameConfig.NameGame.BaCay:
				BaCayController.instance.joinRoom(this.roomInfo);
				break;
			case GameNameConfig.NameGame.MauBinh:
				MauBinhController.instance.joinRoom(this.roomInfo);
				break;
			case GameNameConfig.NameGame.PoKer:
				PokerController.instance.joinRoom(this.roomInfo);
				break;
			case GameNameConfig.NameGame.Sam:
				Room.instance.joinRoom(this.roomInfo);
				break;
		}
	}
}
