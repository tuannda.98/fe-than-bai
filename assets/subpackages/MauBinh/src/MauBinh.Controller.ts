import BroadcastReceiver from "../../../scripts/common/BroadcastReceiver";
import Utils from "../../../scripts/common/Utils";

import App from "../../../scripts/common/App";
import InPacket from "../../../scripts/networks/Network.InPacket";
import cmdNetwork from "../../../scripts/networks/Network.Cmd";
import Configs from "../../../scripts/common/Configs";
import Cmd from "./MauBinh.Cmd";

import MauBinhNetworkClient from "./MauBinh.NetworkClient";
import CardUtils from "./MauBinh.CardUtil"

import DetectPlayerCards from './MauBinh.DetectPlayerCards';
import { GameNameConfig } from "../../Lobby/src/ItemRoom";
import CardGameCmd from "../../../scripts/networks/CardGame.Cmd";

var configPlayer = [
    // {
    //     seatId: 0,
    //     playerId: -1,
    //     playerPos: -1,
    //     isViewer: true
    // }
];

// defaultPlayerPos[0 -> 3][0] = player_pos of me
let defaultPlayerPos = [
    [0, 1, 2, 3],
    [1, 2, 3, 0],
    [2, 3, 0, 1],
    [3, 0, 1, 2]
]

const { ccclass, property } = cc._decorator;

@ccclass
export default class MauBinhController extends cc.Component {

    public static instance: MauBinhController = null;

    // UI Rooms
    @property(cc.Node)
    UI_ChooseRoom: cc.Node = null;
    @property(cc.Label)
    labelNickName: cc.Label = null;
    @property(cc.Label)
    labelCoin: cc.Label = null;
    @property(cc.Node)
    contentListRooms: cc.Node = null;
    @property(cc.Prefab)
    prefabItemRoom: cc.Prefab = null;
    @property(cc.ScrollView)
    scrollListRoom: cc.ScrollView = null;
    @property(cc.EditBox)
    edtFindRoom: cc.EditBox = null;
    @property(cc.Toggle)
    btnHideRoomFull: cc.Toggle = null;

    public isInitedUIRoom = false;

    // UI Playing
    @property(cc.Node)
    UI_Playing: cc.Node = null;
    @property(cc.Node)
    meCards: cc.Node = null;
    @property(cc.Node)
    groupPlayers: cc.Node = null;
    @property(cc.SpriteFrame)
    spriteCards: cc.SpriteFrame[] = [];
    @property(cc.SpriteFrame)
    spriteCardBack: cc.SpriteFrame = null;
    @property(cc.Node)
    cardsDeal: cc.Node = null;
    @property(cc.Button)
    btnLeaveRoom: cc.Button = null;
    @property(cc.Label)
    labelRoomId: cc.Label = null;
    @property(cc.Label)
    labelRoomBet: cc.Label = null;
    @property(cc.Node)
    actionBetting: cc.Node = null;
    @property(cc.Node)
    cardMove: cc.Node = null;
    @property(cc.Node)
    suggestTarget: cc.Node = null;
    @property(cc.Node)
    btnSoChi: cc.Node = null;
    @property(cc.Node)
    btnSwap: cc.Node = null;
    @property(cc.Node)
    btnCombining: cc.Node = null;

    // Notify
    @property(cc.Node)
    notifyTimeStart: cc.Node = null;
    @property(cc.Node)
    notifyTimeEnd: cc.Node = null;
    @property(cc.Node)
    notifyTimeBet: cc.Node = null;
    @property(cc.Node)
    fxSoChiTotal: cc.Node = null;
    @property(cc.SpriteFrame)
    spriteSoChiTotal: cc.SpriteFrame[] = [];

    // UI Chat
    @property(cc.Node)
    UI_Chat: cc.Node = null;
    @property(cc.EditBox)
    edtChatInput: cc.EditBox = null;

    // Popup
    @property(cc.Node)
    popupNodity: cc.Node = null;
    @property(cc.Label)
    labelNotifyContent: cc.Label = null;

    @property(cc.Node)
    popupGuide: cc.Node = null;

    @property(cc.SpriteFrame)
    spriteGroupCard: cc.SpriteFrame[] = [];


    private seatOwner = null;
    private currentRoomBet = null;

    private gameState = null;

    private minutes = null;
    private seconds = null;

    private timeAutoStart = null;
    private timeEnd = null;
    private timeBet = null;
    private intervalWaitting = null;
    private intervalEnd = null;
    private intervalBetting = null;

    private currentCard = null;
    private numCardOpened = 0;

    private timeoutEndGame = null;
    private timeoutChiaBaiDone = null;
    private timeoutBetting = null;

    private isFixAutoKicked = false;

    cardMoveId: -1;
    cardMoveValue: "";

    // LIFE-CYCLE CALLBACKS:

    onLoad() {
        MauBinhController.instance = this;

        this.seatOwner = -1;

        this.initConfigPlayer();
    }

    start() {
        this.showUIRooms();

        App.instance.showErrLoading("Đang kết nối tới server...");
        MauBinhNetworkClient.getInstance().addOnOpen(() => {
            App.instance.showErrLoading("Đang đang đăng nhập...");
            MauBinhNetworkClient.getInstance().send(new cmdNetwork.SendLogin(Configs.Login.Nickname, Configs.Login.AccessToken));
        }, this);
        MauBinhNetworkClient.getInstance().addOnClose(() => {
            App.instance.loadScene("Lobby");
        }, this);
        MauBinhNetworkClient.getInstance().connect();
    }

    onDestroy() {
        clearTimeout(this.timeoutBetting);
        clearTimeout(this.timeoutChiaBaiDone)
    }

    // Request UI Room
    joinRoom(info) {
        App.instance.showLoading(true);
        MauBinhNetworkClient.getInstance().send(new Cmd.SendJoinRoomById(info["id"]));
    }

    refeshListRoom() {
        this.contentListRooms.removeAllChildren(true);
        MauBinhNetworkClient.getInstance().send(new CardGameCmd.SendMoneyBetConfig());
    }

    findRoomId() {
        let text = this.edtFindRoom.string.trim();
        if (text.length > 0) {
            let idFind = parseInt(text);
            for (let index = 0; index < this.contentListRooms.childrenCount; index++) {
                let roomItem = this.contentListRooms.children[index].getComponent("ItemRoom");
                if (roomItem.roomInfo["id"] != idFind) {
                    this.contentListRooms.children[index].active = false;
                }
            }
        } else {
            for (let index = 0; index < this.contentListRooms.childrenCount; index++) {
                this.contentListRooms.children[index].active = true;
            }
        }
    }

    hideRoomFull() {
        if (this.btnHideRoomFull.isChecked) {
            for (let index = 0; index < this.contentListRooms.childrenCount; index++) {
                let roomItem = this.contentListRooms.children[index].getComponent("ItemRoom");
                if (roomItem.roomInfo["userCount"] == roomItem.roomInfo["maxUserPerRoom"]) {
                    this.contentListRooms.children[index].active = false;
                }
            }
        } else {
            for (let index = 0; index < this.contentListRooms.childrenCount; index++) {
                this.contentListRooms.children[index].active = true;
            }
        }
    }

    showUIRooms() {
        this.UI_ChooseRoom.active = true;
        if (this.isInitedUIRoom) {
            BroadcastReceiver.send(BroadcastReceiver.USER_UPDATE_COIN);
        } else {
            this.labelNickName.string = Configs.Login.Nickname;
            BroadcastReceiver.register(BroadcastReceiver.USER_UPDATE_COIN, () => {
                this.labelCoin.string = Utils.formatNumber(Configs.Login.Coin);
            }, this);
            BroadcastReceiver.send(BroadcastReceiver.USER_UPDATE_COIN);
            this.setupListener();
        }
    }

    closeUIRoom() {
        this.UI_ChooseRoom.active = false;
    }

    createRoom() {
        // MauBinhNetworkClient.getInstance().send(new Cmd.SendGetTopServer(1));
    }

    playingNow() {
        let arrRoomOkMoney = [];
        for (let index = 0; index < this.contentListRooms.childrenCount; index++) {
            let roomItem = this.contentListRooms.children[index].getComponent("ItemRoom");
            if (roomItem.roomInfo["requiredMoney"] < Configs.Login.Coin) {
                arrRoomOkMoney.push(index);
            }
        }

        if (arrRoomOkMoney.length > 0) {
            let roomCrowed = arrRoomOkMoney[0];
            for (let index = 0; index < arrRoomOkMoney.length; index++) {
                let roomItem = this.contentListRooms.children[arrRoomOkMoney[index]].getComponent("ItemRoom");
                let roomItemCrowed = this.contentListRooms.children[roomCrowed].getComponent("ItemRoom");
                if (roomItem.roomInfo["userCount"] > roomItemCrowed.roomInfo["userCount"]) {
                    roomCrowed = arrRoomOkMoney[index];
                }
            }
            let roomChoosed = this.contentListRooms.children[roomCrowed].getComponent("ItemRoom");
            // this.joinRoom(roomChoosed.roomInfo);
            MauBinhNetworkClient.getInstance().send(new Cmd.CmdJoinRoom(Configs.App.MONEY_TYPE, roomChoosed.roomInfo.maxUserPerRoom, roomChoosed.roomInfo.moneyBet));
        } else {
            App.instance.alertDialog.showMsg("Không đủ tiền tham gia\nbất kỳ phòng nào !");
        }
    }

    // Chat
    showUIChat() {
        this.UI_Chat.active = true;
        this.UI_Chat.runAction(
            cc.moveTo(0.5, 420, 0)
        );
    }

    closeUIChat() {
        this.UI_Chat.runAction(
            cc.moveTo(0.5, 1000, 0)
        );
    }

    chatEmotion(event, id) {
        MauBinhNetworkClient.getInstance().send(new Cmd.SendChatRoom(1, id));
        this.closeUIChat();
    }

    chatMsg() {
        if (this.edtChatInput.string.trim().length > 0) {
            MauBinhNetworkClient.getInstance().send(new Cmd.SendChatRoom(0, this.edtChatInput.string));
            this.edtChatInput.string = "";
            this.closeUIChat();
        }
    }

    showPopupGuide() {
        this.popupGuide.active = true;
    }

    closePopupGuide() {
        this.popupGuide.active = false;
    }

    backToLobby() {
        MauBinhNetworkClient.getInstance().close();
        App.instance.loadScene("Lobby");
    }

    // Playing
    showUIPlaying() {
        this.UI_Playing.active = true;
    }

    closeUIPlaying() {
        this.isFixAutoKicked = false;
        this.actionLeaveRoom();
    }

    setupMatch(data: Cmd.ReceivedJoinRoomSucceed) {
        this.showUIPlaying();
        this.closeUIChat();

        cc.log("MauBinh setupMatch data : ", data);

        let myChair = data["myChair"];
        let moneyBet = data["moneyBet"];
        let roomId = data["roomId"];
        let gameId = data["gameId"];
        let moneyType = data["moneyType"];
        let rule = data["rule"];
        let playerSize = data["playerSize"];
        let playerStatus = data["playerStatus"];
        let playerInfos = data["playerInfos"];
        let gameState = data["gameState"];
        let gameAction = data["gameAction"];
        let countDownTime = data["countDownTime"];

        this.labelRoomId.string = "MẬU BINH - PHÒNG: " + roomId;
        this.labelRoomBet.string = "MỨC CƯỢC: " + Utils.formatNumber(moneyBet) + "$";

        this.currentRoomBet = moneyBet;

        if (gameState == Cmd.Code.STATE_PLAYING) {
            this.startBettingCountDown(countDownTime);
        }

        configPlayer[0].playerId = Configs.Login.Nickname;
        configPlayer[0].playerPos = myChair;

        var numPlayers = 0;
        var arrPlayerPosExist = [];
        var arrPlayerInfo = [];
        var arrPlayerStatus = [];

        for (let index = 0; index < playerInfos.length; index++) {
            if (playerInfos[index].nickName !== "") {
                numPlayers += 1;
                arrPlayerPosExist.push(index);
                arrPlayerInfo.push(playerInfos[index]);
                arrPlayerStatus.push(playerStatus[index]);
            }
        }

        // setup configPlayer
        for (let a = 0; a < configPlayer.length; a++) {
            configPlayer[a].playerPos = defaultPlayerPos[myChair][a];
        }

        // set State of Seat : Yes | No exist Player
        for (let index = 0; index < configPlayer.length; index++) {
            let findPos = arrPlayerPosExist.indexOf(configPlayer[index].playerPos);

            var seatId = configPlayer[index].seatId;
            this.getPlayerHouse(seatId).resetPlayerInfo(seatId);

            if (findPos > -1) {
                // Exist player -> Set Player Info
                if (arrPlayerStatus[findPos] == Cmd.Code.PLAYER_STATUS_SITTING || arrPlayerStatus[findPos] == Cmd.Code.PLAYER_STATUS_PLAYING) {
                    configPlayer[index].isViewer = false;
                    this.getPlayerHouse(seatId).setIsViewer(false);
                    if (seatId != 0) {
                        if (gameState == Cmd.Code.STATE_PLAYING) {
                            this.getPlayerHouse(seatId).playFxDangXep();
                            this.getPlayerHouse(seatId).showCardReal(true);
                            this.getPlayerHouse(seatId).showCardReady(false);
                        }
                    }
                } else {
                    configPlayer[index].isViewer = true;
                    this.getPlayerHouse(seatId).setIsViewer(true);
                    this.getPlayerHouse(seatId).playFxViewer();
                }

                this.setupSeatPlayer(seatId, arrPlayerInfo[findPos]);
            } else {
                // Not Exist player  -> Active Btn Add player
                this.getPlayerHouse(seatId).showBtnInvite(true);
                configPlayer[index].isViewer = true;
            }
        }
    }


    // Time Start
    startWaittingCountDown(timeWait) {
        this.timeAutoStart = timeWait;
        this.setTimeWaittingCountDown();
        this.notifyTimeStart.active = true;
        this.unschedule(this.intervalWaitting);
        this.schedule(this.intervalWaitting = () => {
            this.timeAutoStart--;
            this.setTimeWaittingCountDown();
            if (this.timeAutoStart < 1) {
                this.unschedule(this.intervalWaitting);
                this.notifyTimeStart.active = false;
            }
        }, 1)
    }

    setTimeWaittingCountDown() {
        this.seconds = Math.floor(this.timeAutoStart % 60);
        this.notifyTimeStart.getComponent(cc.Label).string = " Bắt đầu sau : " + this.seconds + "s ";
    }

    // Time End
    startEndCountDown(timeWait) {
        this.timeEnd = timeWait;
        this.setTimeEndCountDown();
        this.notifyTimeEnd.active = true;
        this.unschedule(this.intervalEnd);
        this.schedule(this.intervalEnd = () => {
            this.timeEnd--;
            this.setTimeEndCountDown();
            if (this.timeEnd < 1) {
                this.unschedule(this.intervalEnd);
                this.notifyTimeEnd.active = false;
            }
        }, 1)
    }

    setTimeEndCountDown() {
        this.seconds = Math.floor(this.timeEnd % 60);
        this.notifyTimeEnd.getComponent(cc.Label).string = " Kết thúc sau : " + this.seconds + "s ";
    }

    // Time Bet
    startBettingCountDown(turnTime) {
        this.timeBet = turnTime;
        this.actionBetting.active = true;
        this.processBetting(1);
        this.unschedule(this.intervalBetting);
        this.schedule(this.intervalBetting = () => {
            this.timeBet--;
            var rate = (this.timeBet / turnTime).toFixed(1);
            this.processBetting(rate);
            if (this.timeBet < 1) {
                this.unschedule(this.intervalBetting);
                this.actionBetting.active = false;
            }
        }, 1);
    }

    processBetting(rate) {
        this.actionBetting.children[0].getComponent(cc.Sprite).fillRange = rate;
    }

    getCardsScore(arrCards) {
        let score = 0;
        for (let a = 0; a < 3; a++) {
            score += CardUtils.getDiemById(arrCards[a]);
        }
        score = score % 10;
        if (score == 0) {
            score = 10;
        }

        return score;
    }

    listRoom: any = null;
    initRooms(rooms) {
        let arrBet = [];
        this.contentListRooms.removeAllChildren();
        let id = 0;
        let names = ["San bằng tất cả", "Nhiều tiền thì vào", "Dân chơi", "Bàn cho đại gia", "Tứ quý", "Bốn đôi thông", "Tới trắng", "Chặt heo"];
        for (let i = 0; i < rooms.length; i++) {
            let room = rooms[i];
            id++;
            let isExisted = arrBet.indexOf(room.moneyBet);
            if (isExisted == -1) {
                arrBet.push(room.moneyBet);
            }
        }
        arrBet.sort(function (a, b) {
            return a - b;
        });

        for (let index = 0; index < arrBet.length; index++) {
            let playerCount = 0;
            let maxUser = 0;
            let moneyRequire = 0;
            for (let a = 0; a < rooms.length; a++) {
                let room = rooms[a];
                if (room.moneyBet == arrBet[index]) {
                    playerCount += room.nPersion;
                    maxUser = room.maxUserPerRoom;
                    moneyRequire = room.moneyRequire;
                }
            }
            var item = cc.instantiate(this.prefabItemRoom);
            item.getComponent("ItemRoom").initItem({
                id: index + 1,
                moneyBet: arrBet[index],
                requiredMoney: moneyRequire,
                userCount: playerCount,
                maxUserPerRoom: maxUser
            })

            item.getComponent("ItemRoom").labelNumPlayers.string = playerCount;

            item.on(cc.Node.EventType.TOUCH_END, (event) => {
                MauBinhNetworkClient.getInstance().send(new CardGameCmd.SendJoinRoom(Configs.App.MONEY_TYPE, maxUser, arrBet[index], 0));
            });
            item.parent = this.contentListRooms;
        }

        this.contentListRooms.parent.parent.getComponent(cc.ScrollView).scrollToBottom(0);
        this.contentListRooms.parent.parent.getComponent(cc.ScrollView).scrollToTop(2);

    }

    // addListener
    setupListener() {
        MauBinhNetworkClient.getInstance().addListener((data) => {
            let inpacket = new InPacket(data);
            switch (inpacket.getCmdId()) {
                case Cmd.Code.LOGIN:
                    App.instance.showLoading(false);
                    this.refeshListRoom();
                    MauBinhNetworkClient.getInstance().send(new Cmd.CmdReconnectRoom());
                    break;
                case Cmd.Code.TOPSERVER:
                    {
                        App.instance.showLoading(false);
                        let res = new Cmd.ReceivedTopServer(data);

                        let rankType = res["rankType"];
                        let topDay_money = res["topDay_money"];
                        let topWeek_money = res["topWeek_money"];
                        let topMonth_money = res["topMonth_money"];
                    }
                    break;
                case Cmd.Code.CMD_PINGPONG:
                    {
                        App.instance.showLoading(false);
                    }
                    break;
                case Cmd.Code.CMD_JOIN_ROOM:
                    {
                        App.instance.showLoading(false);
                    }
                    break;
                case Cmd.Code.CMD_RECONNECT_ROOM:
                    {
                        App.instance.showLoading(false);
                    }
                    break;
                case Cmd.Code.MONEY_BET_CONFIG:
                    {
                        App.instance.showLoading(false);
                        let res = new CardGameCmd.ResMoneyBetConfig(data);
                        this.listRoom = res.list;
                        this.initRooms(res.list);
                    }
                    break;
                case Cmd.Code.JOIN_ROOM_FAIL:
                    {
                        App.instance.showLoading(false);
                        let res = new Cmd.ReceivedJoinRoomFail(data);
                        let msg = "Lỗi " + res.getError() + ", không xác định.";
                        switch (res.getError()) {
                            case 1:
                                msg = "Lỗi kiểm tra thông tin!";
                                break;
                            case 2:
                                msg = "Không tìm được phòng thích hợp. Vui lòng thử lại sau!";
                                break;
                            case 3:
                                msg = "Bạn không đủ tiền vào phòng chơi này!";
                                break;
                            case 4:
                                msg = "Không tìm được phòng thích hợp. Vui lòng thử lại sau!";
                                break;
                            case 5:
                                msg = "Mỗi lần vào phòng phải cách nhau 10 giây!";
                                break;
                            case 6:
                                msg = "Hệ thống bảo trì!";
                                break;
                            case 7:
                                msg = "Không tìm thấy phòng chơi!";
                                break;
                            case 8:
                                msg = "Mật khẩu phòng chơi không đúng!";
                                break;
                            case 9:
                                msg = "Phòng chơi đã đủ người!";
                                break;
                            case 10:
                                msg = "Bạn bị chủ phòng không cho vào bàn!"
                                break;
                        }
                        App.instance.alertDialog.showMsg(msg);
                    }
                    break;
                case Cmd.Code.GET_LIST_ROOM:
                    {
                        let res = new Cmd.ReceivedGetListRoom(data);
                    }
                    break;
                case Cmd.Code.JOIN_GAME_ROOM_BY_ID:
                    {
                        App.instance.showLoading(false);
                    }
                    break;

                // ------------------------ Game ---------------------------     

                case Cmd.Code.BINH_SO_CHI:
                    {
                        App.instance.showLoading(false);
                        let res = new Cmd.ReceivedBinhSoChi(data);

                        let chair = res["chair"];
                        let seatId = this.findPlayerSeatByPos(chair);
                        if (seatId != -1) {
                            if (seatId == 0) {
                                this.btnCombining.active = true;
                                this.btnSoChi.active = false;
                                this.btnSwap.active = false;
                                for (let index = 0; index < 13; index++) {
                                    this.meCards.children[index].getComponent('MauBinh.MeCard').offDrag();
                                }
                            } else {
                                this.getPlayerHouse(seatId).playFxXepXong();
                            }
                        }
                    }
                    break;
                case Cmd.Code.XEP_LAI:
                    {
                        App.instance.showLoading(false);
                        let res = new Cmd.ReceivedXepLai(data);

                        let chair = res["chair"];
                        let seatId = this.findPlayerSeatByPos(chair);
                        if (seatId != -1) {
                            if (seatId == 0) {
                                this.btnCombining.active = false;
                                this.btnSoChi.active = true;
                                this.btnSwap.active = true;
                                for (let index = 0; index < 13; index++) {
                                    this.meCards.children[index].getComponent('MauBinh.MeCard').activeDrag();
                                }
                            } else {
                                this.getPlayerHouse(seatId).playFxDangXep();
                            }
                        }
                    }
                    break;
                case Cmd.Code.KET_THUC:
                    {
                        App.instance.showLoading(false);
                        let res = new Cmd.ReceivedEndGame(data);
                        cc.log("KET_THUC", JSON.stringify(res));
                        this.actionHoldRoom();

                        this.unschedule(this.timeoutBetting);
                        this.actionBetting.active = false;

                        this.btnSoChi.active = false;
                        this.btnSwap.active = false;
                        this.btnCombining.active = false;

                        for (let index = 0; index < 13; index++) {
                            this.meCards.children[index].getComponent('MauBinh.MeCard').offDrag();
                            this.meCards.children[index].getComponent('MauBinh.MeCard').resetState();
                        }

                        let playerResultList = res["playerResultList"];
                        let timeEndGame = res["timeEndGame"];

                        this.unschedule(this.intervalBetting);
                        this.actionBetting.active = false;

                        // show Me cards
                        for (let index = 0; index < playerResultList.length; index++) {
                            let result = playerResultList[index];
                            let seatId = this.findPlayerSeatByPos(result.chairIndex);
                            if (seatId != -1 && seatId == 0) {
                                let totalCards = [
                                    result.chi3[0], result.chi3[1], result.chi3[2],
                                    result.chi2[0], result.chi2[1], result.chi2[2], result.chi2[3], result.chi2[4],
                                    result.chi1[0], result.chi1[1], result.chi1[2], result.chi1[3], result.chi1[4]
                                ];

                                for (let a = 0; a < 13; a++) {
                                    let spriteCardId = CardUtils.getNormalId(totalCards[a]);
                                    this.meCards.children[a].children[1].getComponent(cc.Sprite).spriteFrame = this.spriteCards[spriteCardId];
                                }
                                Configs.Login.Coin = result.currentMoney;
                                BroadcastReceiver.send(BroadcastReceiver.USER_UPDATE_COIN);
                            }
                        }

                        for (let index = 0; index < Cmd.Code.MAX_PLAYER; index++) {
                            this.getPlayerHouse(index).resetResultGame();
                            this.getPlayerHouse(index).prepareFxAction();
                        }

                        this.getPlayerHouse(0).scaleCardReal(0.45);
                        this.soChi(1, playerResultList);
                    }
                    break;
                case Cmd.Code.CHIA_BAI:
                    {
                        App.instance.showLoading(false);
                        let res = new Cmd.ReceivedChiaBai(data);

                        this.btnSoChi.active = false;
                        this.btnSwap.active = false;
                        this.btnCombining.active = false;

                        let cardList = res["cardList"];
                        let mauBinh = res["mauBinh"];
                        let gameId = res["gameId"];
                        let countdown = res["countdown"];

                        clearTimeout(this.timeoutBetting);
                        this.timeoutBetting = setTimeout(() => {
                            this.startBettingCountDown(countdown);
                        }, 3000); // 2000

                        this.currentCard = cardList;

                        let arrChiCuoi = [this.currentCard[0], this.currentCard[1], this.currentCard[2]];
                        let arrChiGiua = [this.currentCard[3], this.currentCard[4], this.currentCard[5], this.currentCard[6], this.currentCard[7]];
                        let arrChiDau = [this.currentCard[8], this.currentCard[9], this.currentCard[10], this.currentCard[11], this.currentCard[12]];
                        this.logCard(this.currentCard);
                        this.logCard(arrChiCuoi);
                        this.logCard(arrChiGiua);
                        this.logCard(arrChiDau);

                        var arrSeatExist = this.getNumPlayers();
                        let numPlayer = arrSeatExist.length;

                        let cardDeal = 4;
                        // Open | Hide cards not use -> Mau binh nhieu la bai qua nen chi chia 4 la tuong trung
                        for (let index = 0; index < Cmd.Code.MAX_PLAYER * cardDeal; index++) { // 4 players * 4 cards
                            this.cardsDeal.children[index].active = index >= numPlayer * cardDeal ? false : true;
                            this.cardsDeal.children[index].position = cc.v2(0, 0);
                        }

                        let timeShip = 0.1; // 0.15
                        // Move Cards used to each player joined
                        for (let a = 0; a < cardDeal; a++) { // players x 4 cards
                            for (let b = 0; b < numPlayer; b++) {
                                let seatId = arrSeatExist[b];
                                if (seatId !== -1) {
                                    let card4Me = this.cardsDeal.children[(a * numPlayer) + b];
                                    let rawPlayerPos = this.groupPlayers.children[seatId].position;
                                    card4Me.runAction(
                                        cc.sequence(
                                            cc.delayTime(((a * numPlayer) + b) * timeShip),
                                            cc.moveTo(0.2, rawPlayerPos)
                                        )
                                    );
                                }
                            }
                        }

                        let delayOver2Under = 0.2;
                        var maxDelay = (((cardDeal - 1) * numPlayer) + (numPlayer - 1)) * timeShip; // ((a * numPlayer) + b) * timeShip
                        let timeUnderLayer = (maxDelay + 0.2 + delayOver2Under) * 1000;
                        clearTimeout(this.timeoutChiaBaiDone);
                        this.timeoutChiaBaiDone = setTimeout(() => {
                            for (let index = 0; index < Cmd.Code.MAX_PLAYER * cardDeal; index++) { // 4 players * 3 cards
                                this.cardsDeal.children[index].active = false;
                            }

                            for (let index = 0; index < numPlayer; index++) {
                                let seatId = arrSeatExist[index];
                                if (seatId !== -1) {
                                    // Drop layer
                                    if (seatId == 0) {
                                        this.getPlayerHouse(seatId).resetCardReady(seatId);
                                        this.getPlayerHouse(seatId).showCardReal(false);
                                        this.getPlayerHouse(seatId).showCardReady(true);
                                        // Open Me cards
                                        this.getPlayerHouse(0).prepareToTransform();
                                        for (let a = 0; a < 13; a++) {
                                            let spriteCardId = CardUtils.getNormalId(cardList[a]);
                                            let cardOpen = this.meCards.children[a];
                                            cardOpen.active = true;
                                            cardOpen.getComponent("MauBinh.MeCard").setupCard({
                                                pos: a,
                                                is_Upper: false,
                                                card: cardList[a]
                                            }, this.spriteCards[spriteCardId]);
                                            this.getPlayerHouse(0).transformToCardReal(a, this.spriteCards[spriteCardId], 0);
                                        }

                                        this.actionAutoBinhSoChi();
                                        this.btnSoChi.active = true;
                                        this.btnSwap.active = true;
                                        let isGood = mauBinh == Cmd.Code.TYPE_BINH_LUNG ? false : true;
                                        let typeName = this.getBinhName(mauBinh);
                                        this.getPlayerHouse(0).resetResultGame();
                                        if (mauBinh != Cmd.Code.TYPE_BINH_THUONG) {
                                            this.getPlayerHouse(0).playFxResultGeneral(0, isGood, typeName, 0);
                                        }
                                        for (let index = 0; index < numPlayer; index++) {
                                            if (arrSeatExist[index] != 0) {
                                                this.getPlayerHouse(arrSeatExist[index]).playFxDangXep();
                                            }
                                        }

                                        let x = new DetectPlayerCards();
                                        x.initCard(this.currentCard);
                                        let result = x.getPlayerCardsInfo(true); // isTinhAce

                                        let arrChiCuoi = [this.currentCard[0], this.currentCard[1], this.currentCard[2]];
                                        let arrChiGiua = [this.currentCard[3], this.currentCard[4], this.currentCard[5], this.currentCard[6], this.currentCard[7]];
                                        let arrChiDau = [this.currentCard[8], this.currentCard[9], this.currentCard[10], this.currentCard[11], this.currentCard[12]];

                                        this.highLightCards(3, result.chiCuoi, arrChiCuoi);
                                        this.highLightCards(2, result.chiGiua, arrChiGiua);
                                        this.highLightCards(1, result.chiDau, arrChiDau);

                                        // fix bugs auto kick
                                        // this.actionLeaveRoom();
                                        this.isFixAutoKicked = true;
                                    } else {
                                        this.getPlayerHouse(seatId).showCardReal(true);
                                        this.getPlayerHouse(seatId).showCardReady(false);
                                    }
                                }
                            }
                        }, timeUnderLayer);
                    }
                    break;
                case Cmd.Code.TU_DONG_BAT_DAU:
                    {
                        App.instance.showLoading(false);
                        let res = new Cmd.ReceivedAutoStart(data);

                        if (res.isAutoStart) {
                            this.startWaittingCountDown(res.timeAutoStart);
                            this.btnSoChi.active = false;
                            this.btnSwap.active = false;
                            this.btnCombining.active = false;
                            this.resetPlayersPlaying();
                            for (let index = 0; index < Cmd.Code.MAX_PLAYER; index++) {
                                this.getPlayerHouse(index).resetResultGame();
                            }
                            this.fxSoChiTotal.stopAllActions();
                            this.fxSoChiTotal.active = false;
                        }
                    }
                    break;
                case Cmd.Code.THONG_TIN_BAN_CHOI:
                    {
                        App.instance.showLoading(false);
                        let res = new Cmd.ReceivedGameInfo(data);

                        // Reconnect
                        this.closeUIRoom();
                        this.showUIPlaying();
                        this.closeUIChat();

                        // {"_pos":142,"_length":142,"_controllerId":1,"_cmdId":3110,"_error":0,"myChair":1,"gameState":1,"gameAction":2,"countdownTime":38,"moneyBet":100,"moneyType":1,"gameId":148502,"roomId":89,"rule":0,"hasInfo":[true,true,true,false],"players":[{"sochi":false,"status":3,"avatar":"3","userId":7630312,"nickName":"FB1626955554","currentMoney":15202},{"cardList":[51,49,47,39,38,36,34,32,17,13,11,10,5],"sochi":false,"status":3,"avatar":"10","userId":6820754,"nickName":"testtest3x","currentMoney":226327},{"sochi":true,"status":3,"avatar":"7","userId":889,"nickName":"Maikhongxong412","currentMoney":2043352}]}

                        let myChair = res["myChair"];
                        let gameState = res["gameState"];
                        // let gameAction = res["gameAction"];
                        let countDownTime = res["countDownTime"];
                        let moneyBet = res["moneyBet"];
                        // let moneyType = res["moneyType"];
                        // let gameId = res["gameId"];
                        let roomId = res["roomId"];
                        // let rule = res["rule"];
                        let hasInfo = res["hasInfo"];
                        let players = res["players"];

                        this.labelRoomId.string = "MẬU BINH - PHÒNG: " + roomId;
                        this.labelRoomBet.string = "MỨC CƯỢC: " + Utils.formatNumber(moneyBet) + "$";

                        this.currentRoomBet = moneyBet;

                        this.currentCard = players[myChair].cardList;

                        configPlayer[0].playerId = Configs.Login.Nickname;
                        configPlayer[0].playerPos = myChair;

                        var numPlayers = 0;
                        var arrPlayerPosExist = [];

                        for (let index = 0; index < hasInfo.length; index++) {
                            if (hasInfo[index]) {
                                numPlayers += 1;
                                arrPlayerPosExist.push(index);
                            }
                        }

                        // setup configPlayer
                        for (let a = 0; a < configPlayer.length; a++) {
                            configPlayer[a].playerPos = defaultPlayerPos[myChair][a];
                        }

                        // set State of Seat : Yes | No exist Player
                        for (let index = 0; index < configPlayer.length; index++) {
                            let findPos = arrPlayerPosExist.indexOf(configPlayer[index].playerPos);

                            var seatId = configPlayer[index].seatId;
                            this.getPlayerHouse(seatId).resetPlayerInfo(seatId);

                            if (findPos > -1) {
                                // Exist player -> Set Player Info
                                this.setupSeatPlayer(seatId, {
                                    nickName: players[findPos].nickName,
                                    avatar: parseInt(players[findPos].avatar),
                                    money: players[findPos].currentMoney
                                });

                                if (players[findPos].status == Cmd.Code.PLAYER_STATUS_VIEWER) {
                                    configPlayer[seatId].isViewer = true;
                                    this.getPlayerHouse(seatId).setIsViewer(true);
                                    this.getPlayerHouse(seatId).playFxViewer();
                                } else {
                                    configPlayer[seatId].isViewer = false;
                                    this.getPlayerHouse(seatId).setIsViewer(false);
                                    if (seatId != 0) {
                                        this.getPlayerHouse(seatId).showCardReady(false);
                                        this.getPlayerHouse(seatId).showCardReal(true);
                                        if (players[findPos].sochi) {
                                            this.getPlayerHouse(seatId).playFxXepXong();
                                        } else {
                                            this.getPlayerHouse(seatId).playFxDangXep();
                                        }
                                    } else {
                                        this.btnSoChi.active = !players[findPos].sochi;
                                        this.btnSwap.active = this.btnSoChi.active;
                                        this.btnCombining.active = players[findPos].sochi;
                                    }
                                }
                            } else {
                                // Not Exist player  -> Active Btn Add player
                                this.getPlayerHouse(seatId).showBtnInvite(true);
                                configPlayer[index].isViewer = true;
                            }
                        }

                        // Open Me cards
                        if (this.currentCard.length > 0) {
                            this.getPlayerHouse(0).resetCardReady(0);
                            this.getPlayerHouse(0).showCardReal(false);
                            this.getPlayerHouse(0).showCardReady(true);
                            // Open Me cards
                            this.getPlayerHouse(0).prepareToTransform();
                            for (let a = 0; a < 13; a++) {
                                let spriteCardId = CardUtils.getNormalId(this.currentCard[a]);
                                let cardOpen = this.meCards.children[a];
                                cardOpen.active = true;
                                cardOpen.getComponent("MauBinh.MeCard").setupCard({
                                    pos: a,
                                    is_Upper: false,
                                    card: this.currentCard[a]
                                }, this.spriteCards[spriteCardId]);
                                this.getPlayerHouse(0).transformToCardReal(a, this.spriteCards[spriteCardId], 0);
                            }

                            this.btnSoChi.active = true;
                            this.btnSwap.active = true;
                            this.checkUpdateCard();
                            this.isFixAutoKicked = true;
                        }

                        if (gameState == Cmd.Code.STATE_PLAYING) {
                            this.startBettingCountDown(countDownTime);
                        }
                    }
                    break;
                case Cmd.Code.DANG_KY_THOAT_PHONG:
                    {
                        App.instance.showLoading(false);
                        let res = new Cmd.ReceivedNotifyRegOutRoom(data);

                        let outChair = res["outChair"];
                        let isOutRoom = res["isOutRoom"];

                        let seatId = this.findPlayerSeatByPos(outChair);
                        if (seatId !== -1) {
                            if (seatId == 0) {
                                if (this.isFixAutoKicked) {
                                    // Auto call
                                    this.isFixAutoKicked = false;
                                    if (isOutRoom) {
                                        // this.actionLeaveRoom();
                                    }
                                } else {
                                    // Client click
                                }
                            }

                            if (isOutRoom) {
                                this.getPlayerHouse(seatId).showNotify("Sắp rời bàn !");
                            } else {
                                this.getPlayerHouse(seatId).showNotify("Khô Máu !");
                            }
                        }
                    }
                    break;
                case Cmd.Code.MOI_DAT_CUOC:
                    {
                        App.instance.showLoading(false);
                        let res = new Cmd.ReceivedMoiDatCuoc(data);
                        this.startBettingCountDown(res.timeDatCuoc);

                        if (this.seatOwner == 0) { // Me la Chuong -> K dc bet va k dc vao ga
                            this.btnSoChi.active = false;
                            this.btnSwap.active = false;
                            this.btnCombining.active = false;

                        } else {
                            this.btnSoChi.active = true;
                            this.btnSwap.active = true;
                            this.btnCombining.active = true;

                        }

                        this.numCardOpened = 0;
                    }
                    break;
                case Cmd.Code.CHEAT_CARDS:
                    {
                        App.instance.showLoading(false);
                    }
                    break;
                case Cmd.Code.DANG_KY_CHOI_TIEP:
                    {
                        App.instance.showLoading(false);
                    }
                    break;
                case Cmd.Code.UPDATE_OWNER_ROOM:
                    {
                        App.instance.showLoading(false);
                    }
                    break;
                case Cmd.Code.JOIN_ROOM_SUCCESS:
                    {
                        App.instance.showLoading(false);
                        let res = new Cmd.ReceivedJoinRoomSucceed(data);
                        this.closeUIRoom();
                        this.setupMatch(res);
                    }
                    break;
                case Cmd.Code.LEAVE_GAME:
                    {
                        App.instance.showLoading(false);
                        let res = new Cmd.ReceivedUserLeaveRoom(data);

                        let chair = res["chair"];
                        let seatId = this.findPlayerSeatByPos(chair);
                        if (seatId !== -1) {
                            // Need clear configPlayer
                            for (let index = 0; index < configPlayer.length; index++) {
                                if (configPlayer[index].seatId == seatId) {
                                    configPlayer[index].playerId = -1;
                                    configPlayer[index].isViewer = true;
                                }
                            }

                            // Change UI
                            this.getPlayerHouse(seatId).resetPlayerInfo(seatId);
                            this.getPlayerHouse(seatId).showBtnInvite(true);

                            let arrSeatExistLast = this.getNumPlayers();
                            if (arrSeatExistLast.length == 1) {
                                this.resetPlayersPlaying();
                            }

                            if (seatId == 0) {
                                // Me leave
                                // Change UI
                                this.UI_Playing.active = false;
                                this.UI_ChooseRoom.active = true;
                                this.refeshListRoom();
                            }
                        }
                    }
                    break;
                case Cmd.Code.NOTIFY_KICK_FROM_ROOM:
                    {
                        App.instance.showLoading(false);
                        let res = new Cmd.ReceivedKickOff(data);
                        cc.log("NOTIFY_KICK_FROM_ROOM", JSON.stringify(res));
                        this.onKickFromRoom(res.reason);
                    }
                    break;
                case Cmd.Code.NEW_USER_JOIN:
                    {
                        App.instance.showLoading(false);
                        let res = new Cmd.ReceivedUserJoinRoom(data);
                        let info = res["info"];
                        let myChair = res["myChair"];
                        let uStatus = res["uStatus"];

                        // set State of Seat : Yes | No exist Player
                        for (let index = 0; index < configPlayer.length; index++) {
                            if (configPlayer[index].playerPos == myChair) {
                                // Exist player -> Set Player Info
                                var seatId = configPlayer[index].seatId;
                                this.getPlayerHouse(seatId).resetPlayerInfo(seatId);
                                var customPlayerInfo = {
                                    "avatar": info["avatar"],
                                    "nickName": info["nickName"],
                                    "money": info["money"],
                                }

                                this.setupSeatPlayer(seatId, customPlayerInfo);

                                if (uStatus == Cmd.Code.PLAYER_STATUS_VIEWER) {
                                    configPlayer[seatId].isViewer = true;
                                    this.getPlayerHouse(seatId).setIsViewer(true);
                                    this.getPlayerHouse(seatId).playFxViewer();
                                } else {
                                    configPlayer[seatId].isViewer = false;
                                    this.getPlayerHouse(seatId).setIsViewer(false);
                                }
                            }
                        }
                    }
                    break;
                case Cmd.Code.NOTIFY_USER_GET_JACKPOT:
                    {
                        App.instance.showLoading(false);
                    }
                    break;
                case Cmd.Code.UPDATE_MATCH:
                    {
                        App.instance.showLoading(false);
                        let res = new Cmd.ReceivedUpdateMatch(data);

                        let myChair = res["myChair"];
                        let hasInfo = res["hasInfo"];
                        let infos = res["infos"];

                        // theo Pos khong phai SeatId
                        for (let index = 0; index < hasInfo.length; index++) {
                            let pos = configPlayer[index]["playerPos"];
                            if (hasInfo[pos]) {
                                // setGold se inactive isViewer nen dat no len dau de ben duoi config lai
                                this.getPlayerHouse(index).setGold(infos[pos]["money"]);
                                configPlayer[index]["playerId"] = infos[pos]["nickName"];
                                if (infos[pos]["status"] == Cmd.Code.PLAYER_STATUS_SITTING || infos[pos]["status"] == Cmd.Code.PLAYER_STATUS_PLAYING) {
                                    configPlayer[index]["isViewer"] = false;
                                    this.getPlayerHouse(index).setIsViewer(false);
                                } else {
                                    configPlayer[index]["isViewer"] = true;
                                    this.getPlayerHouse(index).setIsViewer(true);
                                    this.getPlayerHouse(index).playFxViewer();
                                }
                                this.setupSeatPlayer(index, infos[pos]);
                            } else {
                                configPlayer[index]["playerId"] = -1;
                                configPlayer[index]["isViewer"] = true;
                            }
                        }
                    }
                    break;
                case Cmd.Code.CHAT_ROOM:
                    {
                        App.instance.showLoading(false);
                        let res = new Cmd.ReceivedChatRoom(data);

                        let chair = res["chair"];
                        let isIcon = res["isIcon"];
                        let content = res["content"];
                        if (isIcon) {
                            // Chat Icon
                            let seatId = this.findPlayerSeatByPos(chair);
                            if (seatId != -1) {
                                this.getPlayerHouse(seatId).showChatEmotion(content);
                            }
                        } else {
                            // Chat Msg
                            let seatId = this.findPlayerSeatByPos(chair);
                            if (seatId != -1) {
                                this.getPlayerHouse(seatId).showChatMsg(content);
                            }
                        }
                    }
                    break;
                default:

                    break;
            }
        }, this);
    }

    // request
    actionLeaveRoom() {
        MauBinhNetworkClient.getInstance().send(new Cmd.CmdSendRequestLeaveGame());
    }

    actionHoldRoom() {
        MauBinhNetworkClient.getInstance().send(new Cmd.CmdSendHoldRoom());
    }

    actionBaoBinh() {
        this.btnSoChi.active = false;
        this.btnSwap.active = false;
        this.btnCombining.active = true;
        MauBinhNetworkClient.getInstance().send(new Cmd.SendBaoBinh());
    }

    actionBinhSoChi() {
        let arrChiCuoi = [this.currentCard[0], this.currentCard[1], this.currentCard[2]];
        let arrChiGiua = [this.currentCard[3], this.currentCard[4], this.currentCard[5], this.currentCard[6], this.currentCard[7]];
        let arrChiDau = [this.currentCard[8], this.currentCard[9], this.currentCard[10], this.currentCard[11], this.currentCard[12]];

        this.logCard(this.currentCard);
        this.logCard(arrChiCuoi);
        this.logCard(arrChiGiua);
        this.logCard(arrChiDau);

        MauBinhNetworkClient.getInstance().send(new Cmd.SendBinhSoChi(arrChiDau, arrChiGiua, arrChiCuoi));
    }

    actionBinhSwap() {
        for (let i = 3; i <= 7; i++) {
            this.swapObjec(i, i + 5)
        }
        this.checkUpdateCard();
    }

    swapObjec(index1, index2) {
        let vl1 = this.currentCard[index1]
        this.currentCard[index1] = this.currentCard[index2];
        this.currentCard[index2] = vl1;
    }

    actionAutoBinhSoChi() {
        return; // Open will error -> Me auto leave room 
        let arrChiCuoi = [this.currentCard[0], this.currentCard[1], this.currentCard[2]];
        let arrChiGiua = [this.currentCard[3], this.currentCard[4], this.currentCard[5], this.currentCard[6], this.currentCard[7]];
        let arrChiDau = [this.currentCard[8], this.currentCard[9], this.currentCard[10], this.currentCard[11], this.currentCard[12]];

        MauBinhNetworkClient.getInstance().send(new Cmd.SendAutoBinhSoChi(arrChiDau, arrChiGiua, arrChiCuoi));
    }

    actionXepLai() {
        this.btnSoChi.active = true;
        this.btnSwap.active = true;
        this.btnCombining.active = false;
        MauBinhNetworkClient.getInstance().send(new Cmd.SendXepLai());
    }

    cardSelect(card_info, card_pos, card_Id) {
        this.cardMove.active = true;
        this.cardMove.position = cc.v2(card_pos.x, card_pos.y - 95);
        let spriteCardId = CardUtils.getNormalId(card_info.card);
        this.cardMove.children[0].getComponent(cc.Sprite).spriteFrame = this.spriteCards[spriteCardId];

        this.cardMoveId = card_Id;
        this.cardMoveValue = card_info.card;

        this.getPlayerHouse(0).resetResultGame();
        for (let a = 0; a < 13; a++) {
            this.meCards.children[a].getComponent("MauBinh.MeCard").setCardShadow(false);
            this.getPlayerHouse(0).shadowCard(a, false);
        }
    }

    showMoveTarget(targetName) {
        for (let index = 0; index < 13; index++) {
            let cardTarget = this.meCards.children[index].getComponent("MauBinh.MeCard");
            if (index == targetName) {
                cardTarget.setCardShadow(false);
                cardTarget.setCardFocus(true);
            } else {
                cardTarget.setCardShadow(true);
                cardTarget.setCardFocus(false);
            }
        }
    }

    completeMoveCard(targetName) {
        let cardFrom = this.currentCard[this.cardMoveId];
        let cardTo = this.currentCard[targetName];
        this.currentCard[this.cardMoveId] = cardTo;
        this.currentCard[targetName] = cardFrom;

        this.checkUpdateCard();
    }

    checkUpdateCard() {
        for (let index = 0; index < 13; index++) {
            let card_id = this.currentCard[index];      // {"card":4,"face":2}
            let spriteCardId = CardUtils.getNormalId(card_id);
            let src = this.spriteCards[spriteCardId];
            let card_Open = this.meCards.children[index];
            card_Open.active = true;
            card_Open.getComponent("MauBinh.MeCard").updateCard(card_id, src);
        }

        this.cardMoveValue = "";
        this.cardMoveId = -1;

        let x = new DetectPlayerCards();
        x.initCard(this.currentCard);
        let result = x.getPlayerCardsInfo(true); // isTinhAce

        let isGood = result.cardType == Cmd.Code.TYPE_BINH_LUNG ? false : true;
        let typeName = this.getBinhName(result.cardType);
        this.getPlayerHouse(0).resetResultGame();
        if (result.cardType != Cmd.Code.TYPE_BINH_THUONG) {
            this.getPlayerHouse(0).playFxResultGeneral(0, isGood, typeName, 0);
        }

        let arrChiCuoi = [this.currentCard[0], this.currentCard[1], this.currentCard[2]];
        let arrChiGiua = [this.currentCard[3], this.currentCard[4], this.currentCard[5], this.currentCard[6], this.currentCard[7]];
        let arrChiDau = [this.currentCard[8], this.currentCard[9], this.currentCard[10], this.currentCard[11], this.currentCard[12]];

        this.logCard(this.currentCard);
        this.logCard(arrChiCuoi);
        this.logCard(arrChiGiua);
        this.logCard(arrChiDau);

        this.highLightCards(3, result.chiCuoi, arrChiCuoi);
        this.highLightCards(2, result.chiGiua, arrChiGiua);
        this.highLightCards(1, result.chiDau, arrChiDau);

        this.actionAutoBinhSoChi();
    }
    highLightCards(chiId, groupKind, cardList) {
        let start = -1;
        let end = -1;
        if (chiId == 3) {
            start = 0;
            end = 3;
        } else if (chiId == 2) {
            start = 3;
            end = 8;
        } else {
            start = 8;
            end = 13;
        }

        for (var a = start; a < end; a++) this.getPlayerHouse(0).shadowCard(a, true);

        switch (groupKind) {
            case Cmd.Code.GROUP_THUNG_PHA_SANH:
            case Cmd.Code.GROUP_CU_LU:
            case Cmd.Code.GROUP_THUNG:
            case Cmd.Code.GROUP_SANH:
                for (var a = start; a < end; a++) this.getPlayerHouse(0).shadowCard(a, false);
                break;
            case Cmd.Code.GROUP_TU_QUY:
            case Cmd.Code.GROUP_SAM_CO:
            case Cmd.Code.GROUP_MOT_DOI:
            case Cmd.Code.GROUP_MAU_THAU:
                for (let a = 0; a < cardList.length - 1; a++) {
                    for (let b = a + 1; b < cardList.length; b++) {
                        if (CardUtils.getNumber(cardList[a]) == CardUtils.getNumber(cardList[b])) {
                            this.getPlayerHouse(0).shadowCard(a + start, false);
                            this.getPlayerHouse(0).shadowCard(b + start, false);
                        }
                    }
                }
                break;
            case Cmd.Code.GROUP_THU:
                for (let a = 0; a < cardList.length - 1; a++) {
                    for (let b = a + 1; b < cardList.length; b++) {
                        if (CardUtils.getNumber(cardList[a]) == CardUtils.getNumber(cardList[b])) {
                            this.getPlayerHouse(0).shadowCard(a + start, false);
                            this.getPlayerHouse(0).shadowCard(b + start, false);
                        }
                    }
                }
                break;
            default:
                break;
        }
    }

    // State
    initConfigPlayer() {
        configPlayer = [];
        for (let index = 0; index < Cmd.Code.MAX_PLAYER; index++) {
            configPlayer.push({
                seatId: index,
                playerId: -1,
                playerPos: -1,
                isViewer: true
            });
        }
    }

    resetPlayersPlaying() {
        for (let index = 0; index < Cmd.Code.MAX_PLAYER; index++) {
            this.getPlayerHouse(index).resetMatchHistory(index);
        }
    }

    // handle Game Players
    setupSeatPlayer(seatId, playerInfo) {
        configPlayer[seatId].playerId = playerInfo.nickName;
        this.getPlayerHouse(seatId).setAvatar(playerInfo.avatar);
        this.getPlayerHouse(seatId).setName(playerInfo.nickName);
        this.getPlayerHouse(seatId).setGold(playerInfo.money);
    }

    findPlayerSeatByUid(uid) {
        let seat = -1;
        for (let index = 0; index < configPlayer.length; index++) {
            if (configPlayer[index].playerId === uid) {
                seat = configPlayer[index].seatId;
            }
        }
        return seat;
    }

    findPlayerPosBySeat(seat) {
        return configPlayer[seat].playerPos;
    }

    findPlayerSeatByPos(pos) {
        if (pos == -1) {
            return -1;
        }

        let seat = -1;
        for (let index = 0; index < configPlayer.length; index++) {
            if (configPlayer[index].playerPos === pos) {
                seat = configPlayer[index].seatId;
            }
        }
        return seat;
    }

    getPlayerHouse(seatId) {
        return this.groupPlayers.children[seatId].getComponent("MauBinh.Player");
    }

    getNumPlayers() {
        var playerPosEntry = [];
        for (let index = 0; index < configPlayer.length; index++) {
            if (configPlayer[index].playerId !== -1 && !configPlayer[index].isViewer) {
                playerPosEntry.push(configPlayer[index].seatId);
            }
        }
        return playerPosEntry;
    }

    getBinhName(maubinhType) {
        let name = "";
        switch (maubinhType) {
            case Cmd.Code.TYPE_SANH_RONG:
                name = "Sảnh Rồng";
                break;
            case Cmd.Code.TYPE_MUOI_BA_CAY_DONG_MAU:
                name = "Mười Ba Cây Đồng Màu";
                break;
            case Cmd.Code.TYPE_MUOI_HAI_CAY_DONG_MAU:
                name = "Mười Hai Cây Đồng Màu";
                break;
            case Cmd.Code.TYPE_BA_CAI_THUNG:
                name = "Ba Cái Thùng";
                break;
            case Cmd.Code.TYPE_BA_CAI_SANH:
                name = "Ba Cái Sảnh";
                break;
            case Cmd.Code.TYPE_LUC_PHE_BON:
                name = "Lục Phế Bôn";
                break;
            case Cmd.Code.TYPE_BINH_THUONG:
                name = "Binh Thường";
                break;
            case Cmd.Code.TYPE_BINH_LUNG:
                name = "Binh Lủng";
                break;
            default:
                break;
        }
        return name;
    }

    needSoChi(playerResultList) {
        let a = 0;
        for (var b = 0; b < playerResultList.length; b++) {
            if (playerResultList[b].maubinhType == Cmd.Code.TYPE_BINH_THUONG) {
                a++;
            }
        }
        return 2 <= a;
    }

    needShowMoneyWhenSoChi(playerResultList) {
        for (var a = 0; a < playerResultList.length; a++) {
            var b = playerResultList[a];
            if (0 == b.chairIndex && b.maubinhType != Cmd.Code.TYPE_BINH_THUONG) return !1
        }
        return !0
    }

    needBatSap(playerResultList) {
        for (var a = 0; a < playerResultList.length; a++)
            if (0 != playerResultList[a].moneySap) return !0;
        return !1
    }

    soChi(chiId, playerResultList) {
        // hide result chi 1 - 3 : not hide general
        for (let index = 0; index < Cmd.Code.MAX_PLAYER; index++) {
            this.getPlayerHouse(index).resetResultChi(1);
            this.getPlayerHouse(index).resetResultChi(2);
            this.getPlayerHouse(index).resetResultChi(3);
        }

        let isNeedSoChi = this.needSoChi(playerResultList);
        let isNeedShowMoneyWhenSoChi = this.needShowMoneyWhenSoChi(playerResultList);

        for (let index = 0; index < playerResultList.length; index++) {
            let result = playerResultList[index];
            let chair = result['chairIndex'];

            let totalCards = [
                result.chi3[0], result.chi3[1], result.chi3[2],
                result.chi2[0], result.chi2[1], result.chi2[2], result.chi2[3], result.chi2[4],
                result.chi1[0], result.chi1[1], result.chi1[2], result.chi1[3], result.chi1[4]
            ];
            let x = new DetectPlayerCards();
            x.initCard(totalCards);
            let playerCardInfo = x.getPlayerCardsInfo(true); // isTinhAce

            let seatId = this.findPlayerSeatByPos(chair);
            if (seatId != -1) {
                if (result.maubinhType == Cmd.Code.TYPE_BINH_THUONG && isNeedSoChi) {
                    // So Chi -> Show card tung chi 1
                    let spriteId = -1;
                    let goldChi = result.moneyInChi[chiId - 1];
                    if (chiId == 1) {
                        spriteId = playerCardInfo.chiDau;
                    } else if (chiId == 2) {
                        spriteId = playerCardInfo.chiGiua;
                    } else {
                        spriteId = playerCardInfo.chiCuoi;
                    }
                    this.getPlayerHouse(seatId).playFxCompareChi(chiId, this.spriteGroupCard[spriteId]);
                    if (isNeedShowMoneyWhenSoChi) {
                        this.getPlayerHouse(seatId).playFxGoldSoChi(goldChi);
                    }

                    // Show cards chi
                    if (chiId == 3) {
                        for (var a = 0; a < 3; a++) {
                            let spriteCardId = CardUtils.getNormalId(result.chi3[a]);
                            this.getPlayerHouse(seatId).prepareCardReal(a);
                            this.getPlayerHouse(seatId).transformToCardReal(a, this.spriteCards[spriteCardId], seatId);
                        }

                        setTimeout(() => {
                            if (this.node == null) return;
                            let totalGoldChi = result.moneyInChi[0] + result.moneyInChi[1] + result.moneyInChi[2];
                            if (totalGoldChi >= 0) {
                                this.getPlayerHouse(seatId).playFxWinSoChi(totalGoldChi);
                            } else {
                                this.getPlayerHouse(seatId).playFxLoseSoChi(totalGoldChi);
                            }
                        }, 1000);
                    } else if (chiId == 2) {
                        for (var a = 0; a < 5; a++) {
                            let spriteCardId = CardUtils.getNormalId(result.chi2[a]);
                            this.getPlayerHouse(seatId).prepareCardReal(a + 3);
                            this.getPlayerHouse(seatId).transformToCardReal(a + 3, this.spriteCards[spriteCardId], seatId);
                        }
                    } else {
                        for (var a = 0; a < 5; a++) {
                            let spriteCardId = CardUtils.getNormalId(result.chi1[a]);
                            this.getPlayerHouse(seatId).prepareCardReal(a + 8);
                            this.getPlayerHouse(seatId).transformToCardReal(a + 8, this.spriteCards[spriteCardId], seatId);
                        }
                    }
                } else {
                    // Khong can So chi -> Show tat card ra
                    if (chiId == 1) {
                        //  // show All cards
                        if (result.maubinhType == Cmd.Code.TYPE_BINH_THUONG) {
                            // show Binh Type
                            if (playerCardInfo.chiDau < 2) {
                                this.getPlayerHouse(seatId).playFxCompareChi(chiId, this.spriteGroupCard[playerCardInfo.chiDau]);
                            }

                            if (playerCardInfo.chiGiua < 2) {
                                this.getPlayerHouse(seatId).playFxCompareChi(chiId, this.spriteGroupCard[playerCardInfo.chiGiua]);
                            }

                            if (playerCardInfo.chiGiua == Cmd.Code.GROUP_SAM_CO) {
                                this.getPlayerHouse(seatId).playFxCompareChi(chiId, this.spriteGroupCard[playerCardInfo.chiCuoi]);
                            }
                        } else {
                            let isGood = result.maubinhType == Cmd.Code.TYPE_BINH_LUNG ? false : true;
                            let typeName = this.getBinhName(result.maubinhType);
                            this.getPlayerHouse(seatId).resetResultGame();
                            this.getPlayerHouse(seatId).playFxResultGeneral(seatId, isGood, typeName, 1);
                        }

                        let totalCards = [
                            result.chi3[0], result.chi3[1], result.chi3[2],
                            result.chi2[0], result.chi2[1], result.chi2[2], result.chi2[3], result.chi2[4],
                            result.chi1[0], result.chi1[1], result.chi1[2], result.chi1[3], result.chi1[4]
                        ];

                        for (let a = 0; a < 13; a++) {
                            let spriteCardId = CardUtils.getNormalId(totalCards[a]);
                            if (seatId == 0) {
                                this.meCards.children[a].children[1].getComponent(cc.Sprite).spriteFrame = this.spriteCards[spriteCardId];
                            } else {
                                this.getPlayerHouse(seatId).prepareToTransform();
                                this.getPlayerHouse(seatId).transformToCardReal(a, this.spriteCards[spriteCardId], seatId);
                            }
                        }

                    }
                }
            }
            if (seatId == 0) {
                for (let a = 0; a < 13; a++) {
                    this.getPlayerHouse(0).shadowCard(a, false);
                    this.meCards.children[a].getComponent("MauBinh.MeCard").setCardShadow(false);
                }
            }
        }

        if (isNeedSoChi) {
            this.node.stopAllActions();
            this.node.runAction(
                cc.sequence(
                    cc.delayTime(2.5), //3s
                    cc.callFunc(() => {
                        if (chiId < 3) {
                            this.soChi(chiId + 1, playerResultList);
                        } else {
                            this.showGoldResult(playerResultList, 3000);
                            this.batSap(playerResultList);
                        }
                    })
                )
            )
        } else {
            // show Gold
            this.showGoldResult(playerResultList, 2000);
            this.batSap(playerResultList);
        }
    }

    showGoldResult(playerResultList, delayTime) {
        setTimeout(() => {
            if (this.node == null) return;
            for (let index = 0; index < playerResultList.length; index++) {
                let result = playerResultList[index];
                let chair = result['chairIndex'];
                let seatId = this.findPlayerSeatByPos(chair);

                if (seatId != -1) {
                    this.getPlayerHouse(seatId).resetResultGame();
                    if (result.moneyCommon >= 0) {
                        // Win
                        this.getPlayerHouse(seatId).fxWin({
                            moneyChange: result.moneyCommon,
                            money: result.currentMoney == 0 ? -1 : result.currentMoney
                        });
                    } else {
                        // Lose
                        this.getPlayerHouse(seatId).fxLose({
                            moneyChange: result.moneyCommon,
                            money: result.currentMoney == 0 ? -1 : result.currentMoney
                        });
                    }
                }
            }
        }, delayTime);
    }

    batSap(playerResultList) {
        if (this.needBatSap(playerResultList)) {
            let countWin = 0;
            let countLose = 0;
            for (let index = 0; index < playerResultList.length; index++) {
                let seatId = this.findPlayerSeatByPos(playerResultList[index].chairIndex);
                if (seatId != -1 && seatId != 0) {
                    if (playerResultList[index].moneySap > 0) {
                        countWin += 1;
                    } else {
                        if (playerResultList[index].moneySap < 0) {
                            countLose += 1;
                        }
                    }
                }
            }
            this.fxSoChiTotal.active = false;

            for (let index = 0; index < playerResultList.length; index++) {
                let seatId = this.findPlayerSeatByPos(playerResultList[index].chairIndex);
                if (seatId == 0) {
                    if (countWin > 0) {
                        this.fxSoChiTotal.active = true;
                        if (countWin == 3) {
                            // bi_sap_lang
                            this.fxSoChiTotal.getComponent(cc.Sprite).spriteFrame = this.spriteSoChiTotal[1];
                        } else {
                            // sap_3_chi
                            this.fxSoChiTotal.getComponent(cc.Sprite).spriteFrame = this.spriteSoChiTotal[2];
                        }
                        this.fxSoChiTotal.runAction(
                            cc.sequence(
                                cc.scaleTo(0.25, 1.1, 1.1),
                                cc.scaleTo(0.25, 1, 1),
                                cc.scaleTo(0.25, 1.1, 1.1),
                                cc.scaleTo(0.25, 1, 1)
                            )
                        );
                        setTimeout(() => {
                            if (this.node == null) return;
                            this.fxSoChiTotal.stopAllActions();
                            this.fxSoChiTotal.active = false;
                        }, 2000);
                    }

                    if (countLose > 0) {
                        if (countLose == 3) {
                            // bat_sap_lang
                            this.fxSoChiTotal.active = true;
                            this.fxSoChiTotal.getComponent(cc.Sprite).spriteFrame = this.spriteSoChiTotal[0];
                            this.fxSoChiTotal.runAction(
                                cc.sequence(
                                    cc.scaleTo(0.25, 1.1, 1.1),
                                    cc.scaleTo(0.25, 1, 1),
                                    cc.scaleTo(0.25, 1.1, 1.1),
                                    cc.scaleTo(0.25, 1, 1)
                                )
                            );
                            setTimeout(() => {
                                if (this.node == null) return;
                                this.fxSoChiTotal.stopAllActions();
                                this.fxSoChiTotal.active = false;
                            }, 2000);
                        } else {
                            this.fxSoChiTotal.active = false;
                        }
                    }
                } else {
                    if (playerResultList[index].moneySap < 0) {
                        // sap_3_chi
                        this.getPlayerHouse(seatId).playFxSoChiTotal(this.spriteSoChiTotal[2]);
                    }
                }
            }
        } else {
            this.soAt();
        }
    }

    soAt() {
        if (true) {
            // so At
        } else {
            // Tinh tien chung
        }
    }

    logCard(arrCard) {
        let result = "";
        for (let index = 0; index < arrCard.length; index++) {
            let num = Math.floor(arrCard[index] / 4) + 2;
            let face = arrCard[index] % 4;

            switch (num) {
                case 14:
                    result += "A";
                    break;
                case 13:
                    result += "K";
                    break;
                case 12:
                    result += "Q";
                    break;
                case 11:
                    result += "J";
                    break;
                default:
                    result += num;
                    break;
            }

            switch (face) {
                case 0:
                    result += "♠ "; //B
                    break;
                case 1:
                    result += "♣ "; //T
                    break;
                case 2:
                    result += "♦ "; //R
                    break;
                case 3:
                    result += "♥ "; //C
                    break;
                default:
                    break;
            }
        }
    }

    onKickFromRoom(reason: number) {
        let msg = "Bạn bị kick khỏi bàn chơi!";
        switch (reason) {
            case Cmd.Code.ERROR_MONEY:
                msg = "Tiền trong bàn không đủ để tiếp tục!";
                break;

            case Cmd.Code.ERROR_BAO_TRI:
                msg = "Hệ thống bảo trì!";
                break;

            default:
                break;
        }

        let seatId = 0;
        for (let index = 0; index < configPlayer.length; index++) {
            if (configPlayer[index].seatId == seatId) {
                configPlayer[index].playerId = -1;
                configPlayer[index].isViewer = true;
            }
        }

        // Change UI
        this.getPlayerHouse(seatId).resetPlayerInfo();
        this.getPlayerHouse(seatId).showBtnInvite(true);

        let arrSeatExistLast = this.getNumPlayers();
        if (arrSeatExistLast.length == 1) {
            this.resetPlayersPlaying();
        }

        if (seatId == 0) {
            // Me leave
            // Change UI
            this.UI_Playing.active = false;
            this.UI_ChooseRoom.active = true;
            this.refeshListRoom();
        }
        this.scheduleOnce(() => {
            App.instance.alertDialog.showMsg(msg);
        }, 0.5);
    }
}
