import SamPlayer from "./Sam.Player"
import SamNetworkClient from "../../../scripts/networks/SamNetworkClient";
import SamCmd from "./Sam.Cmd";
import Room from "./Sam.Room";
import InGame from "../../TienLen/src/TienLen.InGame"
import Card from "../../TienLen/src/TienLen.Card";
import App from "../../../scripts/common/App";
import SamConstant from "./Sam.Constant";

const { ccclass, property } = cc._decorator;

@ccclass
export default class SamInGame extends InGame {

    public static instance: SamInGame = null;

    @property({
        type: SamPlayer,
        override: true
    })
    players: SamPlayer[] = [];
    maxPlayer = SamConstant.Config.MAX_PLAYER;

    onLoad() {
        SamInGame.instance = this;
        this.initRes();
    }

    actLeaveRoom() {
        SamNetworkClient.getInstance().send(new SamCmd.SendRequestLeaveGame());
    }

    userLeaveRoom(data) {
        var chair = this.convertChair(data.chair);
        this.players[chair].setLeaveRoom();
        if (chair == 0) {
            this.show(false);
            Room.instance.show(true);
            Room.instance.refreshRoom();
        }
    }

    chiaBai(data) {
        super.chiaBai(data);
        this.setActiveButtons(["bt_sam", "bt_huy_sam"], [true, true]);
        this.players.forEach(p => {
            if (p.active && p.status == SamConstant.PlayerStatus.PLAY)
                p.setTimeRemain(data.timeBaoSam);
        });
    }

    changeTurn(turn) {
        var chair = this.convertChair(turn.chair);
        this.chairLastTurn = this.changeTurnChair;
        this.changeTurnChair = chair;
        for (let index = 0; index < this.maxPlayer; index++) {
            this.players[index].setTimeRemain(0);
        }
        if (turn.time) {
            this.players[chair].setTimeRemain(turn.time);
        } else {
            this.players[chair].setTimeRemain(14);
        }
        if (chair == 0) {
            this.setActiveButtons(["bt_submit_turn", "bt_pass_turn"], [true, true]);
            this.checkTurn = true;
        }
        if (turn.newRound) {
            this.cleanCardsOnBoard();
            this.currTurnCards = [];
            this.checkTurn = false;
            for (let i = 0; i < this.players.length; i++) {
                if (this.players[i].active) {
                    this.players[i].setStatus();
                }
            }
        }
    }

    sendSubmitTurn(cardSelected) {
        SamNetworkClient.getInstance().send(new SamCmd.SendDanhBai(!1, cardSelected));
    }

    sendPassTurn() {
        SamNetworkClient.getInstance().send(new SamCmd.SendBoLuot(!0));
    }

    actBaoSam() {
        SamNetworkClient.getInstance().send(new SamCmd.SendBaoSam());
    }

    actHuyBaoSam() {
        SamNetworkClient.getInstance().send(new SamCmd.SendHuyBaoSam());
    }

    onBaoSam(data) {
        var chair = this.convertChair(data.chair);
        var p = this.players[chair];
        p.setTimeRemain(0);
        p.setStatus("BÁO SÂM");
        if (data.chair == this.myChair)
            this.setActiveButtons(["bt_sam", "bt_huy_sam"], [false, false]);
    }

    onHuyBaoSam(data) {
        var chair = this.convertChair(data.chair);
        var p = this.players[chair];
        p.setTimeRemain(0);
        p.setStatus("HUỶ SÂM");
        if (data.chair == this.myChair)
            this.setActiveButtons(["bt_sam", "bt_huy_sam"], [false, false]);
    }

    onQuyetDinhSam(data) {
        this.setActiveButtons(["bt_sam", "bt_huy_sam"], [false, false]);
        if (data.isSam) {
            var chair = this.convertChair(data.chair);
            var p = this.players[chair];
            if (p.active)
                App.instance.toast.showToast(p.info.nickName + " được quyền báo sâm");
        }
    }

    notifyUserRegOutRoom(res) {
        let outChair = res["outChair"];
        let isOutRoom = res["isOutRoom"];
        let chair = this.convertChair(outChair);
        if (chair == 0) {
            if (isOutRoom) {
                this.players[chair].setNotify("Sắp rời bàn !");
            } else {
                this.players[chair].setNotify("Khô Máu !");
            }
        }
    }

    playerChatChong(res) {
        let winChair = res["winChair"];
        let lostChair = res["lostChair"];
        let winMoney = res["winMoney"];
        let lostMoney = res["lostMoney"];
        let winCurrentMoney = res["winCurrentMoney"];
        let lostCurrentMoney = res["lostCurrentMoney"];

        setTimeout(() => {
            let seatIdWin = this.convertChair(winChair);
            let seatIdLost = this.convertChair(lostChair);
            this.players[seatIdWin].setCoinChange(winMoney);
            this.players[seatIdLost].setCoinChange(lostMoney);
            this.players[seatIdWin].setCoin(winCurrentMoney);
            this.players[seatIdLost].setCoin(lostCurrentMoney);
            setTimeout(() => {
                this.players[seatIdWin].setStatus("");
                this.players[seatIdLost].setStatus("");
            }, 2000);
        }, 1000);
    }
    chatEmotion(event, id) {
        SamNetworkClient.getInstance().send(new SamCmd.SendChatRoom(1, id));
        this.closeUIChat();
    }

    chatMsg() {
        if (this.edtChatInput.string.trim().length > 0) {
            SamNetworkClient.getInstance().send(new SamCmd.SendChatRoom(0, this.edtChatInput.string));
            this.edtChatInput.string = "";
            this.closeUIChat();
        }
    }

    actSubmitTurn() {
        var cardSelected = [];
        this.cardLine.children.forEach(card => {
            var _card = card.getComponent(Card);
            if (_card.isSelected)
                cardSelected.push(_card.getCardIndex());
        });
        this.sendSubmitTurn(cardSelected);

        this.checkTurn = false;
    }

    public show(isShow: boolean, roomInfo = null) {
        if (isShow) {
            this.node.active = true;
            this.cleanCardLine();
            this.cleanCardsOnBoard();
            this.cleanCardsOnHand();
            for (let index = 0; index < this.maxPlayer; index++) {
                this.players[index].setStatus();
                this.players[index].setLeaveRoom();
            }
            this.setRoomInfo(roomInfo);
        } else {
            this.node.active = false;
        }
    }

    convertChair(a) {
        return (a - this.myChair + this.maxPlayer) % this.maxPlayer;
    }

    onKickFromRoom(reason: number) {
        let msg = "Bạn bị kick khỏi bàn chơi!";
        switch (reason) {
            case SamCmd.Code.ERROR_MONEY:
                msg = "Tiền trong bàn không đủ để tiếp tục!";
                break;

            case SamCmd.Code.ERROR_BAO_TRI:
                msg = "Hệ thống bảo trì!";
                break;

            default:
                break;
        }

        this.players[0].setLeaveRoom();
        this.show(false);
        Room.instance.show(true);
        Room.instance.refreshRoom();
        this.scheduleOnce(() => {
            App.instance.alertDialog.showMsg(msg);
        }, 0.5);
    }
}
