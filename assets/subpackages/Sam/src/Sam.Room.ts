import InPacket from "../../../scripts/networks/Network.InPacket";
import SamNetworkClient from "../../../scripts/networks/SamNetworkClient";
import CardGameCmd from "../../../scripts/networks/CardGame.Cmd";
import SamCmd from "./Sam.Cmd";
import Tween from "../../../scripts/common/Tween";
import Configs from "../../../scripts/common/Configs";
import Utils from "../../../scripts/common/Utils";
import App from "../../../scripts/common/App";
import BroadcastReceiver from "../../../scripts/common/BroadcastReceiver";
import InGame from "./Sam.InGame";
import { GameNameConfig } from "../../Lobby/src/ItemRoom";
import cmd from "./Sam.Cmd";
import { SamConstant } from "./Sam.Constant";
import TienLenConstant from "../../TienLen/src/TienLen.Constant";

const { ccclass, property } = cc._decorator;

@ccclass
export default class Room extends cc.Component {

	public static IS_SOLO = false;
	public static instance: Room = null;

	@property(cc.Node)
	roomContent: cc.Node = null;
	@property(cc.ScrollView)
	scrollListRoom: cc.ScrollView = null;
	@property(cc.Prefab)
	roomItem: cc.Node = null;
	@property(cc.Node)
	ingameNode: cc.Node = null;
	@property(cc.Label)
	lbCoin: cc.Label = null;
	@property(cc.Label)
	lbName: cc.Label = null;
	@property(cc.Sprite)
	avatar: cc.Sprite = null;
	@property(cc.Label)
	lblTitle: cc.Label = null;
	@property(cc.Toggle)
	btnHideRoomFull: cc.Toggle = null;

	private ingame: InGame = null;
	private listRoom = [];

	onLoad() {
		Room.instance = this;

		this.ingame = this.ingameNode.getComponent(InGame);
		this.ingameNode.active = false;

		this.lbCoin.string = Utils.formatNumber(Configs.Login.Coin);
		this.lbName.string = Configs.Login.Nickname;
		this.avatar.spriteFrame = App.instance.getAvatarSpriteFrame(Configs.Login.Avatar);
		BroadcastReceiver.register(BroadcastReceiver.USER_UPDATE_COIN, () => {
			Tween.numberTo(this.lbCoin, Configs.Login.Coin, 0.3);
		}, this);
		BroadcastReceiver.register(BroadcastReceiver.USER_INFO_UPDATED, () => {
			this.lbName.string = Configs.Login.Nickname;
			this.avatar.spriteFrame = App.instance.getAvatarSpriteFrame(Configs.Login.Avatar);
			Tween.numberTo(this.lbCoin, Configs.Login.Coin, 0.3);
		}, this);

		SamNetworkClient.getInstance().addOnClose(() => {
			this.actBack();
		}, this);

		this.lblTitle.string = Room.IS_SOLO ? "SÂM Solo" : "SÂM";
	}

	start() {
		SamNetworkClient.getInstance().addListener((data) => {
			let inpacket = new InPacket(data);
			let cmdId = inpacket.getCmdId();
			cc.log("Sam cmd: ", cmdId);

			switch (cmdId) {
				case CardGameCmd.Code.LOGIN:
					SamNetworkClient.getInstance().send(new SamCmd.SendReconnectRoom());
					break;
				case CardGameCmd.Code.MONEY_BET_CONFIG: {
					let res = new CardGameCmd.ResMoneyBetConfig(data);
					cc.log(res);
					this.listRoom = res.list;
					this.initRooms(res.list);
					break;
				}
				case CardGameCmd.Code.JOIN_ROOM_FAIL: {
					let res = new CardGameCmd.ReceivedJoinRoomFail(data);
					var e = "";
					switch (res.error) {
						case 1:
							e = "L\u1ed7i ki\u1ec3m tra th\u00f4ng tin!";
							break;
						case 2:
							e = "Kh\u00f4ng t\u00ecm \u0111\u01b0\u1ee3c ph\u00f2ng th\u00edch h\u1ee3p. Vui l\u00f2ng th\u1eed l\u1ea1i sau!";
							break;
						case 3:
							e = "B\u1ea1n kh\u00f4ng \u0111\u1ee7 ti\u1ec1n v\u00e0o ph\u00f2ng ch\u01a1i n\u00e0y!";
							break;
						case 4:
							e = "Kh\u00f4ng t\u00ecm \u0111\u01b0\u1ee3c ph\u00f2ng th\u00edch h\u1ee3p. Vui l\u00f2ng th\u1eed l\u1ea1i sau!";
							break;
						case 5:
							e = "M\u1ed7i l\u1ea7n v\u00e0o ph\u00f2ng ph\u1ea3i c\u00e1ch nhau 10 gi\u00e2y!";
							break;
						case 6:
							e = "H\u1ec7 th\u1ed1ng b\u1ea3o tr\u00ec!";
							break;
						case 7:
							e = "Kh\u00f4ng t\u00ecm th\u1ea5y ph\u00f2ng ch\u01a1i!";
							break;
						case 8:
							e = "M\u1eadt kh\u1ea9u ph\u00f2ng ch\u01a1i kh\u00f4ng \u0111\u00fang!";
							break;
						case 9:
							e = "Ph\u00f2ng ch\u01a1i \u0111\u00e3 \u0111\u1ee7 ng\u01b0\u1eddi!";
							break;
						case 10:
							e = "B\u1ea1n b\u1ecb ch\u1ee7 ph\u00f2ng kh\u00f4ng cho v\u00e0o b\u00e0n!"
					}
					App.instance.alertDialog.showMsg(e);
					break;
				}
				case SamCmd.Code.JOIN_ROOM_SUCCESS: {
					let res = new SamCmd.ReceivedJoinRoomSuccess(data);
					cc.log(res);
					this.show(false);
					this.ingame.show(true, res);
					break;
				}
				case SamCmd.Code.UPDATE_GAME_INFO: {
					let res = new SamCmd.ReceivedUpdateGameInfo(data);
					cc.log(res);
					this.show(false);
					this.ingame.updateGameInfo(res);
					break;
				}
				case SamCmd.Code.AUTO_START: {
					let res = new SamCmd.ReceivedAutoStart(data);
					cc.log(res);
					this.ingame.autoStart(res);
					break;
				}
				case SamCmd.Code.USER_JOIN_ROOM: {
					let res = new SamCmd.ReceiveUserJoinRoom(data);
					cc.log(res);
					this.ingame.onUserJoinRoom(res);
					break;
				}
				case SamCmd.Code.FIRST_TURN: {
					let res = new SamCmd.ReceivedFirstTurnDecision(data);
					cc.log(res);
					this.ingame.firstTurn(res);
					break;
				}
				case SamCmd.Code.CHIA_BAI: {
					let res = new SamCmd.ReceivedChiaBai(data);
					cc.log(res);
					this.ingame.chiaBai(res)
					break;
				}
				case SamCmd.Code.CHANGE_TURN: {
					let res = new SamCmd.ReceivedChangeTurn(data);
					// cc.log(res);
					this.ingame.changeTurn(res);
					break;
				}
				case SamCmd.Code.DANH_BAI: {
					let res = new SamCmd.ReceivedDanhBai(data);
					// cc.log(res);
					this.ingame.submitTurn(res);
					break;
				}
				case SamCmd.Code.BO_LUOT: {
					let res = new SamCmd.ReceivedBoluot(data);
					// cc.log(res);
					this.ingame.passTurn(res);
					break;
				}
				case SamCmd.Code.END_GAME: {
					let res = new SamCmd.ReceivedEndGame(data);
					cc.log(res);
					this.ingame.endGame(res);
					break;
				}
				case SamCmd.Code.UPDATE_MATCH: {
					let res = new SamCmd.ReceivedUpdateMatch(data);
					cc.log(res);
					this.ingame.updateMatch(res);
					break;
				}
				case SamCmd.Code.USER_LEAVE_ROOM: {
					let res = new SamCmd.UserLeaveRoom(data);
					cc.log(res);
					this.ingame.userLeaveRoom(res);
					break;
				}
				// case SamCmd.Code.RECONNECT_GAME_ROOM: {
				//     let res = new SamCmd.UserLeaveRoom(data);
				//     cc.log(res);
				//     this.ingame.userLeaveRoom(res);
				//     break;
				// }
				case SamCmd.Code.BAO_SAM: {
					let res = new SamCmd.ReceiveBaoSam(data);
					cc.log(res);
					this.ingame.onBaoSam(res);
					break;
				}
				case SamCmd.Code.HUY_BAO_SAM: {
					let res = new SamCmd.ReceiveHuyBaoSam(data);
					// cc.log(res);
					this.ingame.onHuyBaoSam(res);
					break;
				}
				case SamCmd.Code.QUYET_DINH_SAM: {
					let res = new SamCmd.ReceivedQuyetDinhSam(data);
					// cc.log(res);
					this.ingame.onQuyetDinhSam(res);
					break;
				}
				case SamCmd.Code.CHAT_ROOM:
					{
						App.instance.showLoading(false);
						let res = new SamCmd.ReceivedChatRoom(data);
						cc.log("SAM CHAT_ROOM res : ", JSON.stringify(res));
						this.ingame.setPlayerChat(res);
					}
					break;
				case SamCmd.Code.CHAT_CHONG:
					{
						App.instance.showLoading(false);
						let res = new SamCmd.ReceivedChatChong(data);
						cc.log("SAM CHAT_CHONG res : ", JSON.stringify(res));
						this.ingame.playerChatChong(res);
					}
					break;
				case SamCmd.Code.NOTIFY_KICK_OFF:
					{
						App.instance.showLoading(false);
						let res = new SamCmd.ReceivedKickOff(data);
						this.ingame.onKickFromRoom(res.reason);
					}
					break;
			}
		}, this);

		//get list room
		this.refreshRoom();
	}

	initRooms(rooms) {
		let arrBet = [];
		this.roomContent.removeAllChildren();
		let id = 0;
		let names = ["San bằng tất cả", "Nhiều tiền thì vào", "Dân chơi", "Bàn cho đại gia", "Tứ quý", "Bốn đôi thông", "Tới trắng", "Chặt heo"];
		for (let i = 0; i < rooms.length; i++) {
			let room = rooms[i];
			if ((Room.IS_SOLO && room.maxUserPerRoom == 2) || (!Room.IS_SOLO && room.maxUserPerRoom != 2)) {
				id++;
				let isExisted = arrBet.indexOf(room.moneyBet);
				if (isExisted == -1) {
					arrBet.push(room.moneyBet);
				}
			}
		}
		arrBet.sort(function (a, b) {
			return a - b;
		});

		for (let index = 0; index < arrBet.length; index++) {
			let playerCount = 0;
			let maxUser = 0;
			let moneyRequire = 0;
			for (let a = 0; a < rooms.length; a++) {
				let room = rooms[a];
				if ((Room.IS_SOLO && room.maxUserPerRoom == 2) || (!Room.IS_SOLO && room.maxUserPerRoom != 2)) {
					if (room.moneyBet == arrBet[index]) {
						playerCount += room.nPersion;
						maxUser = room.maxUserPerRoom;
						moneyRequire = room.moneyRequire;
						// cc.log("room : ", room);
					}
				}
			}
			// cc.log("CardGame_ItemRoom playerCount : ", playerCount);
			var item = cc.instantiate(this.roomItem);
			// item.getChildByName("ID").getComponent(cc.Label).string = (index + 1).toString();
			// item.getChildByName("bet").getComponent(cc.Label).string = Utils.formatNumber(arrBet[index]);
			// item.getChildByName("num Players").getComponent(cc.Label).string = playerCount + "/" + maxUser;
			item.getComponent("ItemRoom").initItem({
				id: index + 1,
				moneyBet: arrBet[index],
				requiredMoney: moneyRequire,
				userCount: playerCount,
				maxUserPerRoom: maxUser
			})

			item.on(cc.Node.EventType.TOUCH_END, (event) => {
				SamNetworkClient.getInstance().send(new CardGameCmd.SendJoinRoom(Configs.App.MONEY_TYPE, Room.IS_SOLO ? 2 : 5, arrBet[index], 0));
			});
			item.parent = this.roomContent;
		}

		this.roomContent.parent.parent.getComponent(cc.ScrollView).scrollToBottom(0);
		this.roomContent.parent.parent.getComponent(cc.ScrollView).scrollToTop(2);
	}
	joinRoom(info) {
		cc.log("Poker joinRoom roomInfo : ", info);
		App.instance.showLoading(true);
		SamNetworkClient.getInstance().send(new cmd.SendJoinRoomById(info["id"]));
	}

	actBack() {
		SamNetworkClient.getInstance().close();
		App.instance.loadScene("Lobby");
	}

	public show(isShow: boolean) {
		this.node.active = isShow;
		BroadcastReceiver.send(BroadcastReceiver.USER_UPDATE_COIN);
	}

	refreshRoom() {
		SamNetworkClient.getInstance().send(new CardGameCmd.SendMoneyBetConfig());
	}

	public actQuickPlay() {
		if (this.listRoom == null) {
			App.instance.alertDialog.showMsg("Không tìm thấy bàn nào phù hợp với bạn.");
			return;
		}
		//find all room bet < coin
		let listRoom = [];
		for (let i = 0; i < this.listRoom.length; i++) {
			if (this.listRoom[i].moneyRequire <= Configs.Login.Coin) {
				let room = this.listRoom[i];
				if ((Room.IS_SOLO && room.maxUserPerRoom == 2) || (!Room.IS_SOLO && room.maxUserPerRoom != 2)) {
					listRoom.push(room);
				}
			}
		}
		if (listRoom.length <= 0) {
			App.instance.alertDialog.showMsg("Không tìm thấy bàn nào phù hợp với bạn.");
			return;
		}
		let randomIdx = Utils.randomRangeInt(0, listRoom.length);
		let room = listRoom[randomIdx];
		SamNetworkClient.getInstance().send(new CardGameCmd.SendJoinRoom(Configs.App.MONEY_TYPE, room.maxUserPerRoom, room.moneyBet, 0));
	}
	hideRoomFull() {
		if (this.btnHideRoomFull.isChecked) {
			for (let index = 0; index < this.roomContent.childrenCount; index++) {
				let roomItem = this.roomContent.children[index].getComponent("ItemRoom");
				if (roomItem.roomInfo["userCount"] == roomItem.roomInfo["maxUserPerRoom"]) {
					this.roomContent.children[index].active = false;
				}
			}
		} else {
			for (let index = 0; index < this.roomContent.childrenCount; index++) {
				this.roomContent.children[index].active = true;
			}
		}
	}
}