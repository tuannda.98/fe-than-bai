

const { ccclass, property } = cc._decorator;

@ccclass("")
export default class PopupMailBox extends cc.Component {

    @property(cc.Node)
    lstContent: cc.Node[] = [];

    @property(cc.ScrollView)
    eventTitleScroll: cc.ScrollView = null;

    @property(cc.ScrollView)
    contentScroll: cc.ScrollView = null;

    eventTitleClick(event, eventData) {
        this.lstContent.forEach(item => {
            item.active = false;
        })
        this.lstContent[Number(eventData)].active = true;
        this.contentScroll.content = this.lstContent[Number(eventData)];
        this.contentScroll.scrollToTop(0.2);
        event.target.parent.children.forEach(item => {
            item.getChildByName("selected").active = false;
        })
        event.target.getChildByName("selected").active = true;
    }
}