import App from "../../scripts/common/App";
import Dialog from "../../scripts/common/Dialog";
import TaiXiuDoubleController from "../../subpackages/TaiXiuDouble/src/TaiXiuDouble.Controller";
import PopupMission from "./Lobby.PopupMission";

const { ccclass, property } = cc._decorator;

@ccclass
export default class Banner extends Dialog {

    public static instance: Banner = null;
    private static initing: boolean = false;
    public static createAndShow(parent: cc.Node) {
        if (!this.initing) {
            if (this.instance == null || this.instance.node == null) {
                this.initing = true;
                App.instance.loadPrefab("Lobby/Banner", (err, prefab) => {
                    this.initing = false;
                    if (err != null) {
                        App.instance.alertDialog.showMsg(err);
                        return;
                    }
                    let go = cc.instantiate(prefab);
                    go.parent = parent;
                    this.instance = go.getComponent(Banner);
                    this.instance.node.zIndex = 1000;
                    this.instance.show();
                });
            } else {
                this.instance.node.zIndex = 1000;
                this.instance.show();
            }
        }
    }

    showMission() {
        App.instance.lobby.actMission(null, "x3");
        this.dismiss();
        TaiXiuDoubleController.instance.dismiss();
    }


}
