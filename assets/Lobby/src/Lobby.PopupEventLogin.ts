import Dialog from "../../scripts/common/Dialog";
import Configs from "../../scripts/common/Configs";
import App from "../../scripts/common/App";

const { ccclass, property } = cc._decorator;

@ccclass
export default class PopupEventLogin extends Dialog {
    public actOpen() {
        App.instance.openTelegram(Configs.App.getLinkTelegram());
    }
}
