import Dialog from "../../scripts/common/Dialog";

const {ccclass, property} = cc._decorator;

@ccclass
export default class PopupGuide extends Dialog {

    fillOpacity = 255;
}
