import App from "../../scripts/common/App";
import Http from "../../scripts/common/Http";
import Configs from "../../scripts/common/Configs";
import MiniGameNetworkClient from "../../scripts/networks/MiniGameNetworkClient";
import BroadcastReceiver from "../../scripts/common/BroadcastReceiver";
import SPUtils from "../../scripts/common/SPUtils";
import Tween from "../../scripts/common/Tween";
import SlotNetworkClient from "../../scripts/networks/SlotNetworkClient";
import TienLenNetworkClient from "../../scripts/networks/TienLenNetworkClient";
import SamNetworkClient from "../../scripts/networks/SamNetworkClient";
import PopupGiftCode from "./Lobby.PopupGiftCode";
import cmd from "./Lobby.Cmd";
import InPacket from "../../scripts/networks/Network.InPacket";
import TabsListGame from "./Lobby.TabsListGame";
import PopupUpdateNickname from "./PopupUpdateNickname";
import PopupLuckyWheel from "./Lobby.PopupLuckyWheel";
import PopupTransaction from "./Lobby.PopupTransaction";
import PopupSecurity from "./Lobby.PopupSecurity";
import Utils from "../../scripts/common/Utils";
import ButtonListJackpot from "./Lobby.ButtonListJackpot";
import PopupBoomTan from "./Lobby.PopupBoomTan";
import VersionConfig from "../../scripts/common/VersionConfig";
import ShootFishNetworkClient from "../../scripts/networks/ShootFishNetworkClient";
import Dialog from "../../scripts/common/Dialog";
import AudioManager from "../../scripts/common/Common.AudioManager";
import PopupCashOut from "./Lobby.PopupCashOut";
import PopupMailBox from "./Lobby.PopupMailBox";
import PopupMission from "./Lobby.PopupMission";
import Room from "../../subpackages/TienLen/src/TienLen.Room";
import SamRoom from "../../subpackages/Sam/src/Sam.Room";
import Banner from "./Lobby.Banner";
import Res from "../../subpackages/TienLen/src/TienLen.Res";

const { ccclass, property } = cc._decorator;

@ccclass("Lobby.LobbyController.PanelMenu")
export class PanelMenu {
    @property(cc.Node)
    node: cc.Node = null;
    @property(cc.Toggle)
    toggleMusic: cc.Toggle = null;
    @property(cc.Toggle)
    toggleSound: cc.Toggle = null;

    private animate = false;

    start() {
        this.toggleMusic.node.on("toggle", () => {
            SPUtils.setMusicVolumn(this.toggleMusic.isChecked ? 1 : 0);
            BroadcastReceiver.send(BroadcastReceiver.ON_AUDIO_CHANGED);
        });
        this.toggleSound.node.on("toggle", () => {
            SPUtils.setSoundVolumn(this.toggleSound.isChecked ? 1 : 0);
            BroadcastReceiver.send(BroadcastReceiver.ON_AUDIO_CHANGED);
        });
        this.toggleMusic.isChecked = SPUtils.getMusicVolumn() > 0;
        this.toggleSound.isChecked = SPUtils.getSoundVolumn() > 0;
        this.node.active = false;
    }

    show() {
        if (this.animate) return;
        this.animate = true;
        this.node.stopAllActions();
        this.node.active = true;
        this.node.scaleY = 0;
        this.node.runAction(cc.sequence(
            cc.scaleTo(0.2, 1).easing(cc.easeBackOut()),
            cc.callFunc(() => {
                this.animate = false;
            })
        ));
    }

    dismiss() {
        if (this.animate) return;
        this.animate = true;
        this.node.stopAllActions();
        this.node.runAction(cc.sequence(
            cc.scaleTo(0.2, 1, 0).easing(cc.easeBackIn()),
            cc.callFunc(() => {
                this.node.active = false;
                this.animate = false;
            })
        ));
    }

    toggle() {
        if (this.node.active) {
            this.dismiss();
        } else {
            this.show();
        }
    }
}

namespace Lobby {
    @ccclass
    export class LobbyController extends cc.Component {

        @property(cc.Node)
        panelNotLogin: cc.Node = null;
        @property(cc.Node)
        panelLogined: cc.Node = null;
        @property(PanelMenu)
        panelMenu: PanelMenu = null;
        @property(cc.EditBox)
        edbUsername: cc.EditBox = null;
        @property(cc.EditBox)
        edbPassword: cc.EditBox = null;
        @property(cc.Sprite)
        sprAvatar: cc.Sprite = null;
        @property(cc.Label)
        lblNickname: cc.Label = null;
        @property(cc.Label)
        lblVipPoint: cc.Label = null;
        @property(cc.Slider)
        sliderVipPoint: cc.Slider = null;
        @property(cc.Label)
        lblVipPointName: cc.Label = null;
        @property(cc.Sprite)
        spriteProgressVipPoint: cc.Sprite = null;
        @property(cc.Label)
        lblCoin: cc.Label = null;
        @property(cc.Label)
        lblCoinFish: cc.Label = null;
        @property(cc.RichText)
        txtNotifyMarquee: cc.RichText = null;
        @property(ButtonListJackpot)
        buttonListJackpot: ButtonListJackpot = null;
        @property(cc.Node)
        panelSupport: cc.Node = null;
        @property(cc.Label)
        lblMailCount: cc.Label = null;

        @property(TabsListGame)
        tabsListGame: TabsListGame = null;

        @property(PopupGiftCode)
        popupGiftCode: PopupGiftCode = null;
        @property(PopupUpdateNickname)
        popupUpdateNickname: PopupUpdateNickname = null;
        @property(PopupLuckyWheel)
        popupLuckyWheel: PopupLuckyWheel = null;
        @property(PopupTransaction)
        popupTransaction: PopupTransaction = null;
        @property(PopupSecurity)
        popupSecurity: PopupSecurity = null;
        @property(PopupBoomTan)
        popupBoomTan: PopupBoomTan = null;
        @property(Dialog)
        popupEventLogin: Dialog = null;

        @property({ type: cc.AudioClip })
        clipBgm: cc.AudioClip = null;

        @property(cc.Node)
        popups: cc.Node = null;

        @property(cc.Label)
        version: cc.Label = null;

        @property(cc.Node)
        fbBtn: cc.Node = null;

        private static notifyMarquee = "";

        private intervalMailCount = null;

        start() {
            if ((<any>window).currentVersion) {
                this.version.string = (<any>window).currentVersion;
            }
            console.log("CPName: " + VersionConfig.CPName);
            console.log("VersionName: " + VersionConfig.VersionName);

            this.lblCoin.node.parent.active = true;
            this.lblCoinFish.node.parent.active = false;

            this.panelMenu.start();

            BroadcastReceiver.register(BroadcastReceiver.UPDATE_NICKNAME_SUCCESS, (data) => {
                this.edbUsername.string = data["username"];
                this.edbPassword.string = data["password"];
                this.actLogin();
            }, this);

            BroadcastReceiver.register(BroadcastReceiver.USER_UPDATE_COIN, () => {
                Tween.numberTo(this.lblCoin, Configs.Login.Coin, 0.3);
                Tween.numberTo(this.lblCoinFish, Configs.Login.CoinFish, 0.3);
            }, this);

            BroadcastReceiver.register(BroadcastReceiver.USER_INFO_UPDATED, () => {
                this.lblNickname.string = Configs.Login.Nickname;
                this.sprAvatar.spriteFrame = App.instance.getAvatarSpriteFrame(Configs.Login.Avatar);
                this.lblVipPoint.string = "VP: " + Utils.formatNumber(Configs.Login.VipPoint) + "/" + Utils.formatNumber(Configs.Login.getVipPointNextLevel());
                this.sliderVipPoint.progress = Math.min(Configs.Login.VipPoint / Configs.Login.getVipPointNextLevel(), 1);
                this.spriteProgressVipPoint.fillRange = this.sliderVipPoint.progress;
                this.lblVipPointName.string = Configs.Login.getVipPointName();
                Tween.numberTo(this.lblCoin, Configs.Login.Coin, 0.3);
                Tween.numberTo(this.lblCoinFish, Configs.Login.CoinFish, 0.3);
            }, this);

            BroadcastReceiver.register(BroadcastReceiver.USER_LOGOUT, (data) => {
                Configs.Login.clear();
                this.panelNotLogin.active = true;
                this.panelLogined.active = false;
                this.edbUsername.string = SPUtils.getUserName();
                this.edbPassword.string = SPUtils.getUserPass();
                MiniGameNetworkClient.getInstance().close();
                SlotNetworkClient.getInstance().close();
                TienLenNetworkClient.getInstance().close();
                ShootFishNetworkClient.getInstance().close();
                App.instance.buttonMiniGame.hidden();
            }, this);

            BroadcastReceiver.register(BroadcastReceiver.UPDATE_MAIL_COUNT, () => {
                this.lblMailCount.node.parent.active = Configs.Login.MailCount > 0;
                this.lblMailCount.string = Configs.Login.MailCount.toString();
            }, this);

            this.edbUsername.string = SPUtils.getUserName();
            this.edbPassword.string = SPUtils.getUserPass();

            let moveAndCheck = () => {
                this.txtNotifyMarquee.node.runAction(cc.sequence(
                    cc.moveBy(0.5, cc.v2(-60, 0)),
                    cc.callFunc(() => {
                        if (this.txtNotifyMarquee.node.position.x < -this.txtNotifyMarquee.node.width - 50) {
                            this.txtNotifyMarquee.string = LobbyController.notifyMarquee;
                            // this.notifyMarquee = "";
                            let pos = this.txtNotifyMarquee.node.position;
                            pos.x = this.txtNotifyMarquee.node.parent.width + 50;
                            this.txtNotifyMarquee.node.position = pos;
                        }
                        moveAndCheck();
                    })
                ));
            };
            let pos = this.txtNotifyMarquee.node.position;
            pos.x = this.txtNotifyMarquee.node.parent.width + 50;
            this.txtNotifyMarquee.node.position = pos;
            this.txtNotifyMarquee.string = LobbyController.notifyMarquee;
            moveAndCheck();

            if (!Configs.Login.IsLogin) {
                if (this.edbUsername.string.length > 0 && this.edbPassword.string.length > 0) {
                    this.actLogin();
                }
                this.panelNotLogin.active = true;
                this.panelLogined.active = false;
                App.instance.buttonMiniGame.hidden();

                //fake jackpot
                // this.tabsListGame.updateItemJackpots("nudiepvien", Utils.randomRangeInt(5000, 7000) * 100, false, Utils.randomRangeInt(5000, 7000) * 1000, false, Utils.randomRangeInt(5000, 7000) * 10000, false);
                // this.tabsListGame.updateItemJackpots("khobau", Utils.randomRangeInt(5000, 7000) * 100, false, Utils.randomRangeInt(5000, 7000) * 1000, false, Utils.randomRangeInt(5000, 7000) * 10000, false);
                // this.tabsListGame.updateItemJackpots("sieuanhhung", Utils.randomRangeInt(5000, 7000) * 100, false, Utils.randomRangeInt(5000, 7000) * 1000, false, Utils.randomRangeInt(5000, 7000) * 10000, false);
                // this.tabsListGame.updateItemJackpots("vuongquocvin", Utils.randomRangeInt(5000, 7000) * 100, false, Utils.randomRangeInt(5000, 7000) * 1000, false, Utils.randomRangeInt(5000, 7000) * 10000, false);
                // this.tabsListGame.updateItemJackpots("shootfish", Utils.randomRangeInt(5000, 7000) * 100, false, Utils.randomRangeInt(5000, 7000) * 1000, false, Utils.randomRangeInt(5000, 7000) * 10000, false);
            } else {
                this.panelNotLogin.active = false;
                this.panelLogined.active = true;
                BroadcastReceiver.send(BroadcastReceiver.USER_INFO_UPDATED);
                SlotNetworkClient.getInstance().sendCheck(new cmd.ReqSubcribeHallSlot());
                MiniGameNetworkClient.getInstance().sendCheck(new cmd.ReqGetMoneyUse());
                this.getMailNotRead();
            }

            // Configs.App.getServerConfig(); //cfg exchange

            MiniGameNetworkClient.getInstance().addListener((data) => {
                let inPacket = new InPacket(data);
                console.log(inPacket.getCmdId());
                switch (inPacket.getCmdId()) {
                    case 21000:
                        let res = new cmd.ResUpdateUserMoney(data);
                        Configs.Login.Coin = res.totalMoney;
                        BroadcastReceiver.send(BroadcastReceiver.USER_UPDATE_COIN);
                        if (res.showPopup) {
                            App.instance.alertDialog.showMsg("Có thư mới :\n" + res.desc);
                        }
                        break;
                    case cmd.Code.NOTIFY_MARQUEE: {
                        let res = new cmd.ResNotifyMarquee(data);
                        let resJson = JSON.parse(res.message);
                        LobbyController.notifyMarquee = "";
                        for (let i = 0; i < resJson["entries"].length; i++) {
                            let e = resJson["entries"][i];
                            LobbyController.notifyMarquee += "<color=#90ff00>(" + Configs.GameId.getGameName(e["g"]) + ")</color>";
                            LobbyController.notifyMarquee += "<color=#ff0054> " + e["n"] + "</color> thắng ";
                            LobbyController.notifyMarquee += "<color=#ffeb30>" + Utils.formatNumber(e["m"]) + "</color>";
                            if (i < resJson["entries"].length - 1) {
                                LobbyController.notifyMarquee += "        ";
                            }
                        }
                        break;
                    }
                    case cmd.Code.UPDATE_JACKPOTS: {
                        let res = new cmd.ResUpdateJackpots(data);
                        // this.buttonListJackpot.setData(res);
                        break;
                    }
                    case cmd.Code.GET_MONEY_USE: {
                        let res = new cmd.ResGetMoneyUse(data);
                        // console.log(res);
                        Configs.Login.Coin = res.moneyUse;
                        BroadcastReceiver.send(BroadcastReceiver.USER_UPDATE_COIN);
                        break;
                    }
                }
            }, this);
            // MiniGameNetworkClient.getInstance().send(new cmd.ReqSubcribeJackpots());
            this.buttonListJackpot.updateJackpot(0.3);

            SlotNetworkClient.getInstance().addListener((data) => {
                let inPacket = new InPacket(data);
                switch (inPacket.getCmdId()) {
                    case cmd.Code.UPDATE_JACKPOT_SLOTS: {
                        //{"ndv":{"100":{"p":673620,"x2":0},"1000":{"p":6191000,"x2":0},"10000":{"p":73540000,"x2":0}},"kb":{"100":{"p":503160,"x2":0},"1000":{"p":5044400,"x2":0},"10000":{"p":51398000,"x2":0}},"vqv":{"100":{"p":509480,"x2":0},"1000":{"p":5013000,"x2":0},"10000":{"p":50852000,"x2":0}},"sah":{"100":{"p":502890,"x2":0},"1000":{"p":6932010,"x2":0},"10000":{"p":55193700,"x2":0}}}
                        let res = new cmd.ResUpdateJackpotSlots(data);
                        this.buttonListJackpot.setData(res);
                        let resJson = JSON.parse(res.pots);
                        // cc.log(resJson);
                        // let ndv = resJson["ndv"];
                        // this.tabsListGame.updateItemJackpots("nudiepvien", ndv["100"]["p"], ndv["100"]["x2"] == 1, ndv["1000"]["p"], ndv["1000"]["x2"] == 1, ndv["10000"]["p"], ndv["10000"]["x2"] == 1);

                        // let kb = resJson["kb"];
                        // this.tabsListGame.updateItemJackpots("khobau", kb["100"]["p"], kb["100"]["x2"] == 1, kb["1000"]["p"], kb["1000"]["x2"] == 1, kb["10000"]["p"], kb["10000"]["x2"] == 1);

                        // let sah = resJson["sah"];
                        // this.tabsListGame.updateItemJackpots("sieuanhhung", sah["100"]["p"], sah["100"]["x2"] == 1, sah["1000"]["p"], sah["1000"]["x2"] == 1, sah["10000"]["p"], sah["10000"]["x2"] == 1);

                        // let vqv = resJson["vqv"];
                        // this.tabsListGame.updateItemJackpots("vuongquocvin", vqv["100"]["p"], vqv["100"]["x2"] == 1, vqv["1000"]["p"], vqv["1000"]["x2"] == 1, vqv["10000"]["p"], vqv["10000"]["x2"] == 1);

                        // let fb = resJson["football"];
                        // this.tabsListGame.updateItemJackpots("football", fb["100"]["p"], fb["100"]["x2"] == 1, fb["1000"]["p"], fb["1000"]["x2"] == 1, fb["10000"]["p"], fb["10000"]["x2"] == 1);

                        // let ct = resJson["caothap"];
                        // this.tabsListGame.updateItemJackpots("caothap", ct["1000"]["p"], ct["1000"]["x2"] == 1, ct["10000"]["p"], ct["10000"]["x2"] == 1, ct["50000"]["p"], ct["50000"]["x2"] == 1);

                        break;
                    }
                }
            }, this);

            ShootFishNetworkClient.getInstance().addListener((route, data) => {
                switch (route) {
                    case "OnUpdateJackpot":
                        this.tabsListGame.updateItemJackpots("shootfish", data["14"], false, data["24"], false, data["34"], false);
                        break;
                }
            }, this);

            switch (VersionConfig.CPName) {
                default:
                    AudioManager.getInstance().playBackgroundMusic(this.clipBgm);
                    break;
            }

            Http.get(Configs.App.API, { c: 6011 }, (err, res) => {
                App.instance.showLoading(false);
                if (!err) {
                    Configs.App.FANPAGE = res.fanpage;
                    Configs.App.TELE_BOT = res.telegram;
                    Configs.App.TELE_GROUP = res.teleGroup;
                    Configs.App.CSKH_PHONE = res.cskh;
                }
            });

            this.getMailNotRead();
            this.unschedule(this.intervalMailCount);
            this.schedule(this.intervalMailCount = () => {
                this.getMailNotRead();
            }, 60);
            Res.getInstance();

            if ('undefined' === typeof (sdkbox) || 'undefined' === typeof (sdkbox.PluginFacebook)) {
                this.fbBtn.active = false;
            } else {
                this.fbBtn.active = true;
                this.initFacebookSdk()
            }

            if('undefined' !== typeof (FBInstant)){
                this.initFacebookInstance();
            }
        }

        onDestroy() {
            SlotNetworkClient.getInstance().send(new cmd.ReqUnSubcribeHallSlot());
        }

        initFacebookInstance(){
            App.instance.showLoading(true);
            var playerID = FBInstant.player.getID();
            let sign;
            FBInstant.player.getSignedPlayerInfoAsync().then(function (result) {
                sign = result.getSignature();
            });
            Http.get(Configs.App.API, { c: 17, pi: playerID, sign: sign, "app_id": window.providerCode,s: "fb" }, (err, res) => {
                App.instance.showLoading(false);
                if (err != null) {
                    App.instance.alertDialog.showMsg("Đăng nhập không thành công, vui lòng kiểm tra lại kết nối.");
                    return;
                }
                switch (parseInt(res["errorCode"])) {
                    case 0:
                        // console.log("Đăng nhập thành công.");
                        Configs.Login.AccessToken = res["accessToken"];
                        Configs.Login.SessionKey = res["sessionKey"];
                        Configs.Login.Username = res["username"];
                        Configs.Login.Password = "";
                        Configs.Login.IsLogin = true;
                        var userInfo = JSON.parse(base64.decode(Configs.Login.SessionKey));
                        // console.log(userInfo);
                        Configs.Login.Nickname = userInfo["nickname"];
                        Configs.Login.Avatar = userInfo["avatar"];
                        Configs.Login.Coin = userInfo["vinTotal"];
                        Configs.Login.LuckyWheel = userInfo["luckyRotate"];
                        Configs.Login.IpAddress = userInfo["ipAddress"];
                        Configs.Login.CreateTime = userInfo["createTime"];
                        Configs.Login.Birthday = userInfo["birthday"];
                        Configs.Login.Birthday = userInfo["birthday"];
                        Configs.Login.VipPoint = userInfo["vippoint"];
                        Configs.Login.VipPointSave = userInfo["vippointSave"];
                        Configs.Login.isSocialLogin = true;
                        Configs.Login.socialToken = sign;
                        Configs.Login.socialSource = "fb";


                        // MiniGameNetworkClient.getInstance().checkConnect();
                        MiniGameNetworkClient.getInstance().sendCheck(new cmd.ReqSubcribeJackpots());
                        SlotNetworkClient.getInstance().sendCheck(new cmd.ReqSubcribeHallSlot());
                        // ShootFishNetworkClient.getInstance().checkConnect(() => {
                        //     BroadcastReceiver.send(BroadcastReceiver.USER_UPDATE_COIN);
                        // });

                        SPUtils.setUserName(Configs.Login.Username);
                        SPUtils.setUserPass(Configs.Login.Password);

                        App.instance.buttonMiniGame.show();
                        this.getMailNotRead();

                        BroadcastReceiver.send(BroadcastReceiver.USER_INFO_UPDATED);
                        this.actGameTaiXiu(null);
                        // PopupMailBox.createAndShow(this.popups, true);
                        this.actBanner();
                        this.panelNotLogin.active = false;
                        this.panelLogined.active = true;
                        break;
                    case 1007:
                        App.instance.alertDialog.showMsg("Thông tin đăng nhập không hợp lệ.");
                        break;
                    case 2001:
                        this.popupUpdateNickname.show2("", "");
                        break;
                    default:
                        App.instance.alertDialog.showMsg("Đăng nhập không thành công vui lòng thử lại sau. " + res["errorCode"]);
                        break;
                }
            });
            
        }

        initFacebookSdk() {
            sdkbox.PluginFacebook.init();
            var _this = this;
            sdkbox.PluginFacebook.setListener({
                onLogin: function (isLogin, msg) {
                    if (isLogin) {
                        cc.log("login successful ");
                        var token = sdkbox.PluginFacebook.getAccessToken();
                        // App.instance.alertDialog.showMsg(token);
                        _this.onLoginFacebookGG(token);
                    }
                    else {
                        cc.log("login failed");
                        App.instance.alertDialog.showMsg("Đăng nhập không thành công, vui lòng thử lại sai");
                    }
                },
                onAPI: function (tag, data) {
                    cc.log("============");
                    cc.log("tag=%s", tag);
                    cc.log("data=%s", data);
                    if (tag == "me") {
                        var obj = JSON.parse(data);
                        cc.log(obj.name + " || " + obj.email);

                        console.log("picture:" + obj.picture.data.url);

                    } else if (tag == "/me/friendlists") {
                        var obj = JSON.parse(data);
                        var friends = obj.data;
                        for (var i = 0; i < friends.length; i++) {
                            cc.log("id %s", friends[i].id);
                        }
                    } else if (tag == "__fetch_picture_tag__") {
                        var obj = JSON.parse(data);
                        var url = obj.data.url;
                        cc.log("get friend's profile picture=%s", url);
                        // self.iconSprite.updateWithUrl(url);
                    }
                },
                onPermission: function (isLogin, msg) {
                    cc.log(msg);
                    if (isLogin) {
                        cc.log("request permission successful");
                    }
                    else {
                        cc.log("request permission failed");
                    }
                },
            })
        }

        actLoginFB() {
            if (cc.sys.isNative && (cc.sys.os == cc.sys.OS_IOS || cc.sys.os == cc.sys.OS_ANDROID)) {
                if (!sdkbox.PluginFacebook.isLoggedIn()) {
                    sdkbox.PluginFacebook.login();
                }
                else {
                    this.onLoginFacebookGG(sdkbox.PluginFacebook.getAccessToken());
                }
            }
            //  else {
            //     FB.getLoginStatus(function (response) {
            //         if (response.status === 'connected') {
            //             var uid = response.authResponse.userID;
            //             var accessToken = response.authResponse.accessToken;
            //             cc.log("accessToken : ", accessToken);
            //             _this.loginWithAccesToken(accessToken);
            //         }
            //         else {
            //             FB.login(function (response) {
            //                 if (response.authResponse) {
            //                     FB.api('/me', 'get', { fields: 'name,email,gender,verified,link' }, function (response) {
            //                         cc.log("fbID : " + response.id);
            //                         var accessToken = FB.getAuthResponse()['accessToken'];
            //                         cc.log("accessToken : ", accessToken);
            //                         _this.loginWithAccesToken(accessToken);
            //                     });
            //                 }
            //                 else {
            //                     cc.log('User cancelled login or did not fully authorize.');
            //                 }
            //             });
            //         }
            //     });
            // }
        }

        onLoginFacebookGG(token: string, source: string = "fb"): void {
            App.instance.showLoading(true);
            // App.instance.alertDialog.show3(token, null);
            Http.get(Configs.App.API, { c: 3, at: token, s: source, "app_id": window.providerCode }, (err, res) => {
                App.instance.showLoading(false);
                if (err != null) {
                    App.instance.alertDialog.showMsg("Đăng nhập không thành công, vui lòng kiểm tra lại kết nối.");
                    return;
                }
                // console.log(res);
                switch (parseInt(res["errorCode"])) {
                    case 0:
                        // console.log("Đăng nhập thành công.");
                        Configs.Login.AccessToken = res["accessToken"];
                        Configs.Login.SessionKey = res["sessionKey"];
                        Configs.Login.Username = res["username"];
                        Configs.Login.Password = "";
                        Configs.Login.IsLogin = true;
                        var userInfo = JSON.parse(base64.decode(Configs.Login.SessionKey));
                        // console.log(userInfo);
                        Configs.Login.Nickname = userInfo["nickname"];
                        Configs.Login.Avatar = userInfo["avatar"];
                        Configs.Login.Coin = userInfo["vinTotal"];
                        Configs.Login.LuckyWheel = userInfo["luckyRotate"];
                        Configs.Login.IpAddress = userInfo["ipAddress"];
                        Configs.Login.CreateTime = userInfo["createTime"];
                        Configs.Login.Birthday = userInfo["birthday"];
                        Configs.Login.Birthday = userInfo["birthday"];
                        Configs.Login.VipPoint = userInfo["vippoint"];
                        Configs.Login.VipPointSave = userInfo["vippointSave"];
                        Configs.Login.isSocialLogin = true;
                        Configs.Login.socialToken = token;
                        Configs.Login.socialSource = source;


                        // MiniGameNetworkClient.getInstance().checkConnect();
                        MiniGameNetworkClient.getInstance().sendCheck(new cmd.ReqSubcribeJackpots());
                        SlotNetworkClient.getInstance().sendCheck(new cmd.ReqSubcribeHallSlot());
                        // ShootFishNetworkClient.getInstance().checkConnect(() => {
                        //     BroadcastReceiver.send(BroadcastReceiver.USER_UPDATE_COIN);
                        // });

                        this.panelNotLogin.active = false;
                        this.panelLogined.active = true;

                        SPUtils.setUserName(Configs.Login.Username);
                        SPUtils.setUserPass(Configs.Login.Password);

                        App.instance.buttonMiniGame.show();
                        this.getMailNotRead();

                        BroadcastReceiver.send(BroadcastReceiver.USER_INFO_UPDATED);
                        this.actGameTaiXiu(null);
                        // PopupMailBox.createAndShow(this.popups, true);
                        this.actBanner();

                        break;
                    case 1007:
                        App.instance.alertDialog.showMsg("Thông tin đăng nhập không hợp lệ.");
                        break;
                    case 2001:
                        this.popupUpdateNickname.show2("", "");
                        break;
                    default:
                        App.instance.alertDialog.showMsg("Đăng nhập không thành công vui lòng thử lại sau. " + res["errorCode"]);
                        break;
                }
            });
        }

        actLogin(): void {
            // console.log("actLogin");
            let username = this.edbUsername.string.trim();
            let password = this.edbPassword.string;

            if (username.length == 0) {
                App.instance.alertDialog.showMsg("Tên đăng nhập không được để trống.");
                return;
            }

            if (password.length == 0) {
                App.instance.alertDialog.showMsg("Mật khẩu không được để trống.");
                return;
            }

            App.instance.showLoading(true);
            Http.get(Configs.App.API, { c: 3, un: username, pw: md5(password), "app_id": window.providerCode }, (err, res) => {
                App.instance.showLoading(false);
                if (err != null) {
                    App.instance.alertDialog.showMsg("Đăng nhập không thành công, vui lòng kiểm tra lại kết nối.");
                    return;
                }
                // console.log(res);
                switch (parseInt(res["errorCode"])) {
                    case 0:
                        // console.log("Đăng nhập thành công.");
                        Configs.Login.AccessToken = res["accessToken"];
                        Configs.Login.SessionKey = res["sessionKey"];
                        Configs.Login.Username = username;
                        Configs.Login.Password = password;
                        Configs.Login.IsLogin = true;
                        var userInfo = JSON.parse(base64.decode(Configs.Login.SessionKey));
                        // console.log(userInfo);
                        Configs.Login.Nickname = userInfo["nickname"];
                        Configs.Login.Avatar = userInfo["avatar"];
                        Configs.Login.Coin = userInfo["vinTotal"];
                        Configs.Login.LuckyWheel = userInfo["luckyRotate"];
                        Configs.Login.IpAddress = userInfo["ipAddress"];
                        Configs.Login.CreateTime = userInfo["createTime"];
                        Configs.Login.Birthday = userInfo["birthday"];
                        Configs.Login.Birthday = userInfo["birthday"];
                        Configs.Login.VipPoint = userInfo["vippoint"];
                        Configs.Login.VipPointSave = userInfo["vippointSave"];
                        Configs.Login.isSocialLogin = false;
                        Configs.Login.socialToken = "";
                        Configs.Login.socialSource = "";

                        // MiniGameNetworkClient.getInstance().checkConnect();
                        MiniGameNetworkClient.getInstance().sendCheck(new cmd.ReqSubcribeJackpots());
                        SlotNetworkClient.getInstance().sendCheck(new cmd.ReqSubcribeHallSlot());
                        // ShootFishNetworkClient.getInstance().checkConnect(() => {
                        //     BroadcastReceiver.send(BroadcastReceiver.USER_UPDATE_COIN);
                        // });

                        this.panelNotLogin.active = false;
                        this.panelLogined.active = true;

                        SPUtils.setUserName(Configs.Login.Username);
                        SPUtils.setUserPass(Configs.Login.Password);

                        App.instance.buttonMiniGame.show();
                        this.getMailNotRead();

                        BroadcastReceiver.send(BroadcastReceiver.USER_INFO_UPDATED);
                        this.actGameTaiXiu(null);
                        // PopupMailBox.createAndShow(this.popups, true);
                        this.actBanner();


                        break;
                    case 1007:
                        App.instance.alertDialog.showMsg("Thông tin đăng nhập không hợp lệ.");
                        break;
                    case 2001:
                        this.popupUpdateNickname.show2(username, password);
                        break;
                    default:
                        App.instance.alertDialog.showMsg("Đăng nhập không thành công vui lòng thử lại sau.");
                        break;
                }
            });
        }

        actPlayNow(): void {
            var id = "";
            try {
                if (cc.sys.os == cc.sys.OS_ANDROID && cc.sys.isNative) {
                    id = jsb.reflection.callStaticMethod("org/cocos2dx/javascript/AppActivity", "getDeviceId", "()Ljava/lang/String;");
                }
                else if (cc.sys.os == cc.sys.OS_IOS && cc.sys.isNative) {
                    id = jsb.reflection.callStaticMethod("AppController", "getDeviceId");
                } else {
                    return;
                    // id="123123123123";
                }
            } catch (error) {
                cc.log(error);
            }
            var idTemp = id;
            while (id.length < 20) {
                id += idTemp;
            }
            this.onLoginFacebookGG(id, "dev");
        }

        actMenu() {
            this.panelMenu.toggle();
        }

        actDaiLy() {
            if (!Configs.Login.IsLogin) {
                App.instance.alertDialog.showMsg("Bạn chưa đăng nhập.");
                return;
            }
        }

        actGiftCode() {
            if (!Configs.Login.IsLogin) {
                App.instance.alertDialog.showMsg("Bạn chưa đăng nhập.");
                return;
            }
            this.popupGiftCode.show();
        }

        actVQMM() {
            if (!Configs.Login.IsLogin) {
                App.instance.alertDialog.showMsg("Bạn chưa đăng nhập.");
                return;
            }
            this.popupLuckyWheel.show();
        }

        actEvent(event) {
            if (!Configs.Login.IsLogin) {
                App.instance.alertDialog.showMsg("Bạn chưa đăng nhập.");
                return;
            }
            // cc.sys.openURL(Configs.App.LINK_EVENT);
            this.actGoToShootFish(event);
        }

        actDownload() {
            cc.sys.openURL(Configs.App.LINK_DOWNLOAD);
        }

        actFanpage() {
            cc.sys.openURL(Configs.App.getLinkFanpage());
        }

        actTelegram() {
            App.instance.openTelegram(Configs.App.getLinkTelegramGroup());
        }

        actHotLine() {
            App.instance.alertDialog.showMsg("Hotline " + Configs.App.CSKH_PHONE);
        }

        actAppOTP() {
            App.instance.openTelegram();
        }

        actSupportOnline() {
            cc.sys.openURL(Configs.App.LINK_SUPPORT);
        }

        actSupport() {
            this.panelSupport.active = !this.panelSupport.active;
            // if (this.panelSupport.active) {
            //     this.panelSupport.stopAllActions();
            //     this.panelSupport.scale = 1;
            //     this.panelSupport.runAction(cc.sequence(
            //         cc.scaleTo(0.2, 0).easing(cc.easeBackIn()),
            //         cc.callFunc(() => {
            //             this.panelSupport.active = false;
            //         })
            //     ));
            // } else {
            //     this.panelSupport.stopAllActions();
            //     this.panelSupport.active = true;
            //     this.panelSupport.scale = 0;
            //     this.panelSupport.runAction(cc.sequence(
            //         cc.scaleTo(0.2, 1).easing(cc.easeBackOut()),
            //         cc.callFunc(() => {
            //         })
            //     ));
            // }
            return;
            cc.sys.openURL(Configs.App.LINK_SUPPORT);
        }

        actBack() {
            App.instance.confirmDialog.show3("Bạn có muốn đăng xuất khỏi tài khoản?", "ĐĂNG XUẤT", (isConfirm) => {
                if (isConfirm) {
                    BroadcastReceiver.send(BroadcastReceiver.USER_LOGOUT);
                }
            });
        }

        public actSwitchCoin() {
            if (this.lblCoin.node.parent.active) {
                this.lblCoin.node.parent.active = false;
                this.lblCoinFish.node.parent.active = true;
            } else {
                this.lblCoin.node.parent.active = true;
                this.lblCoinFish.node.parent.active = false;
            }
        }

        actGameTaiXiu(event) {
            if (!Configs.Login.IsLogin) {
                App.instance.alertDialog.showMsg("Bạn chưa đăng nhập.");
                return;
            }
            App.instance.openGameTaiXiuMini(event ? event.target : null);
        }

        actGameBauCua(event) {
            if (!Configs.Login.IsLogin) {
                App.instance.alertDialog.showMsg("Bạn chưa đăng nhập.");
                return;
            }
            App.instance.openGameBauCua(event ? event.target : null);
        }

        actGameOanTuXi(event) {
            if (!Configs.Login.IsLogin) {
                App.instance.alertDialog.showMsg("Bạn chưa đăng nhập.");
                return;
            }
            App.instance.openGameOanTuTi(event ? event.target : null);
        }

        actGameCaoThap(event) {
            if (!Configs.Login.IsLogin) {
                App.instance.alertDialog.showMsg("Bạn chưa đăng nhập.");
                return;
            }
            App.instance.openGameCaoThap(event ? event.target : null);
        }

        actGameSlot3x3(event) {
            if (!Configs.Login.IsLogin) {
                App.instance.alertDialog.showMsg("Bạn chưa đăng nhập.");
                return;
            }
            App.instance.openGameSlot3x3(event ? event.target : null);
        }

        actGameMiniPoker(event) {
            if (!Configs.Login.IsLogin) {
                App.instance.alertDialog.showMsg("Bạn chưa đăng nhập.");
                return;
            }
            App.instance.openGameMiniPoker(event ? event.target : null);
        }

        actGameTaLa() {
            if (!Configs.Login.IsLogin) {
                App.instance.alertDialog.showMsg("Bạn chưa đăng nhập.");
                return;
            }
            App.instance.alertDialog.showMsg("Sắp ra mắt.");
        }

        actGoToSlot1(event) {
            if (!Configs.Login.IsLogin) {
                App.instance.alertDialog.showMsg("Bạn chưa đăng nhập.");
                return;
            }
            App.instance.showErrLoading("Đang kết nối tới server...");
            SlotNetworkClient.getInstance().checkConnect(() => {
                App.instance.showLoading(false);
                App.instance.loadSceneInSubpackage("Slot1", "Slot1", event ? event.target : null);
            });
        }

        actGoToSlot2(event) {
            if (!Configs.Login.IsLogin) {
                App.instance.alertDialog.showMsg("Bạn chưa đăng nhập.");
                return;
            }
            App.instance.showErrLoading("Đang kết nối tới server...");
            SlotNetworkClient.getInstance().checkConnect(() => {
                App.instance.showLoading(false);
                App.instance.loadSceneInSubpackage("Slot2", "Slot2", event ? event.target : null);
            });
        }

        actGoToSlot3(event) {
            if (!Configs.Login.IsLogin) {
                App.instance.alertDialog.showMsg("Bạn chưa đăng nhập.");
                return;
            }
            App.instance.showErrLoading("Đang kết nối tới server...");
            SlotNetworkClient.getInstance().checkConnect(() => {
                App.instance.showLoading(false);
                App.instance.loadSceneInSubpackage("Slot3", "Slot3", event ? event.target : null);
            });
        }

        actGoToSlot4(event) {
            if (!Configs.Login.IsLogin) {
                App.instance.alertDialog.showMsg("Bạn chưa đăng nhập.");
                return;
            }
            App.instance.showErrLoading("Đang kết nối tới server...");
            SlotNetworkClient.getInstance().checkConnect(() => {
                App.instance.showLoading(false);
                App.instance.loadSceneInSubpackage("Slot4", "Slot4", event ? event.target : null);
            });
        }

        actGoToSlot5(event) {
            if (!Configs.Login.IsLogin) {
                App.instance.alertDialog.showMsg("Bạn chưa đăng nhập.");
                return;
            }
            // if (VersionConfig.CPName == VersionConfig.CP_NAME_88KING || VersionConfig.CPName == VersionConfig.CP_NAME_MARBLES99) {
            //     App.instance.alertDialog.showMsg("Sắp ra mắt.");
            //     return;
            // }
            App.instance.showErrLoading("Đang kết nối tới server...");
            SlotNetworkClient.getInstance().checkConnect(() => {
                App.instance.showLoading(false);
                App.instance.loadSceneInSubpackage("Slot5", "Slot5", event ? event.target : null);
            });
        }


        actGoToSlot6(event) {
            if (!Configs.Login.IsLogin) {
                App.instance.alertDialog.showMsg("Bạn chưa đăng nhập.");
                return;
            }
            // if (VersionConfig.CPName == VersionConfig.CP_NAME_88KING || VersionConfig.CPName == VersionConfig.CP_NAME_MARBLES99) {
            //     App.instance.alertDialog.showMsg("Sắp ra mắt.");
            //     return;
            // }
            App.instance.showErrLoading("Đang kết nối tới server...");
            SlotNetworkClient.getInstance().checkConnect(() => {
                App.instance.showLoading(false);
                App.instance.loadSceneInSubpackage("Slot6", "Slot6", event ? event.target : null);
            });
        }

        actGoToSlot7(event) {
            if (!Configs.Login.IsLogin) {
                App.instance.alertDialog.showMsg("Bạn chưa đăng nhập.");
                return;
            }
            // if (VersionConfig.CPName == VersionConfig.CP_NAME_88KING || VersionConfig.CPName == VersionConfig.CP_NAME_MARBLES99) {
            //     App.instance.alertDialog.showMsg("Sắp ra mắt.");
            //     return;
            // }
            App.instance.showErrLoading("Đang kết nối tới server...");
            SlotNetworkClient.getInstance().checkConnect(() => {
                App.instance.showLoading(false);
                App.instance.loadSceneInSubpackage("Slot7", "Slot7", event ? event.target : null);
            });
        }

        actGoToSlot8(event) {
            if (!Configs.Login.IsLogin) {
                App.instance.alertDialog.showMsg("Bạn chưa đăng nhập.");
                return;
            }
            // if (VersionConfig.CPName == VersionConfig.CP_NAME_88KING || VersionConfig.CPName == VersionConfig.CP_NAME_MARBLES99) {
            //     App.instance.alertDialog.showMsg("Sắp ra mắt.");
            //     return;
            // }
            App.instance.showErrLoading("Đang kết nối tới server...");
            SlotNetworkClient.getInstance().checkConnect(() => {
                App.instance.showLoading(false);
                App.instance.loadSceneInSubpackage("Slot8", "Slot8", event ? event.target : null);
            });
        }

        actDev() {
            if (!Configs.Login.IsLogin) {
                App.instance.alertDialog.showMsg("Bạn chưa đăng nhập.");
                return;
            }
            App.instance.alertDialog.showMsg("Sắp ra mắt.");
            return;
        }

        actGoToShootFish(event) {
            if (!Configs.Login.IsLogin) {
                App.instance.alertDialog.showMsg("Bạn chưa đăng nhập.");
                return;
            }
            App.instance.loadSceneInSubpackage("ShootFish", "ShootFish", event ? event.target : null);
        }

        actGotoLoto(event) {
            if (!Configs.Login.IsLogin) {
                App.instance.alertDialog.showMsg("Bạn chưa đăng nhập.");
                return;
            }
            // if (VersionConfig.CPName == VersionConfig.CP_NAME_88KING || VersionConfig.CPName == VersionConfig.CP_NAME_MARBLES99) {
            //     App.instance.alertDialog.showMsg("Sắp ra mắt.");
            //     return;
            // }
            App.instance.loadSceneInSubpackage("Loto", "Loto", event ? event.target : null);
        }

        actGoToXocDia(event) {
            if (!Configs.Login.IsLogin) {
                App.instance.alertDialog.showMsg("Bạn chưa đăng nhập.");
                return;
            }
            // App.instance.alertDialog.showMsg("Sắp ra mắt.");
            // return;
            App.instance.loadSceneInSubpackage("XocDia", "XocDia", event ? event.target : null);
        }

        actGoToMauBinh(event) {
            if (!Configs.Login.IsLogin) {
                App.instance.alertDialog.showMsg("Bạn chưa đăng nhập.");
                return;
            }
            // App.instance.alertDialog.showMsg("Sắp ra mắt.");
            App.instance.loadSceneInSubpackage("MauBinh", "MauBinh", event ? event.target : null);
        }

        actAddCoin() {
            if (!Configs.Login.IsLogin) {
                App.instance.alertDialog.showMsg("Bạn chưa đăng nhập.");
                return;
            }
            App.instance.popupShop.show();
        }

        accExchange() {
            if (!Configs.Login.IsLogin) {
                App.instance.alertDialog.showMsg("Bạn chưa đăng nhập.");
                return;
            }
            PopupCashOut.createAndShow(this.popups);
        }

        actGoToTLMN(event) {
            if (!Configs.Login.IsLogin) {
                App.instance.alertDialog.showMsg("Bạn chưa đăng nhập.");
                return;
            }
            // App.instance.alertDialog.showMsg("Sắp ra mắt.");
            // return;

            App.instance.showErrLoading("Đang kết nối tới server...");
            TienLenNetworkClient.getInstance().checkConnect(() => {
                App.instance.showLoading(false);
                // App.instance.loadScene("TienLen");
                Room.IS_SOLO = false;
                App.instance.loadSceneInSubpackage("TienLen", "TienLen", event ? event.target : null);
            });
        }

        actGameTLMNSolo(event) {
            if (!Configs.Login.IsLogin) {
                App.instance.alertDialog.showMsg("Bạn chưa đăng nhập.");
                return;
            }
            // App.instance.alertDialog.showMsg("Sắp ra mắt.");
            // return;

            App.instance.showErrLoading("Đang kết nối tới server...");
            TienLenNetworkClient.getInstance().checkConnect(() => {
                App.instance.showLoading(false);
                Room.IS_SOLO = true;
                App.instance.loadSceneInSubpackage("TienLen", "TienLen", event ? event.target : null);
            });
        }

        actGoToSam(event) {
            if (!Configs.Login.IsLogin) {
                App.instance.alertDialog.showMsg("Bạn chưa đăng nhập.");
                return;
            }

            App.instance.showErrLoading("Đang kết nối tới server...");
            SamNetworkClient.getInstance().checkConnect(() => {
                App.instance.showLoading(false);
                SamRoom.IS_SOLO = false;
                App.instance.loadSceneInSubpackage("Sam", "Sam", event ? event.target : null);
            });
        }

        actGoToSamSolo(event) {
            if (!Configs.Login.IsLogin) {
                App.instance.alertDialog.showMsg("Bạn chưa đăng nhập.");
                return;
            }

            App.instance.showErrLoading("Đang kết nối tới server...");
            SamNetworkClient.getInstance().checkConnect(() => {
                App.instance.showLoading(false);
                SamRoom.IS_SOLO = true;
                App.instance.loadSceneInSubpackage("Sam", "Sam", event ? event.target : null);
            });
        }

        actGoToBaCay(event) {
            if (!Configs.Login.IsLogin) {
                App.instance.alertDialog.showMsg("Bạn chưa đăng nhập.");
                return;
            }
            App.instance.loadSceneInSubpackage("BaCay", "BaCay", event ? event.target : null);
        }

        actGoToBaiCao(event) {
            if (!Configs.Login.IsLogin) {
                App.instance.alertDialog.showMsg("Bạn chưa đăng nhập.");
                return;
            }
            // App.instance.alertDialog.showMsg("Sắp ra mắt.");
            // return;
            App.instance.loadSceneInSubpackage("BaiCao", "BaiCao", event ? event.target : null);
        }

        actGoToLieng(event) {
            if (!Configs.Login.IsLogin) {
                App.instance.alertDialog.showMsg("Bạn chưa đăng nhập.");
                return;
            }
            // App.instance.alertDialog.showMsg("Sắp ra mắt.");
            // return;
            App.instance.loadSceneInSubpackage("Lieng", "Lieng", event ? event.target : null);
        }

        actGoToXiDach(event) {
            if (!Configs.Login.IsLogin) {
                App.instance.alertDialog.showMsg("Bạn chưa đăng nhập.");
                return;
            }
            App.instance.loadSceneInSubpackage("XiDzach", "XiDzach", event ? event.target : null);
            // App.instance.alertDialog.showMsg("Sắp ra mắt.");
        }

        actGoToPoker(event) {
            if (!Configs.Login.IsLogin) {
                App.instance.alertDialog.showMsg("Bạn chưa đăng nhập.");
                return;
            }
            // App.instance.alertDialog.showMsg("Sắp ra mắt.");
            // return;
            App.instance.loadSceneInSubpackage("Poker", "Poker", event ? event.target : null);
        }

        actMailBox() {
            if (!Configs.Login.IsLogin) {
                App.instance.alertDialog.showMsg("Bạn chưa đăng nhập.");
                return;
            }
            PopupMailBox.createAndShow(this.popups);
        }

        actMission(event, selectedTab?) {
            if (!Configs.Login.IsLogin) {
                App.instance.alertDialog.showMsg("Bạn chưa đăng nhập.");
                return;
            }
            PopupMission.createAndShow(this.popups, selectedTab);
        }

        actBanner() {
            if (!Configs.Login.IsLogin) {
                App.instance.alertDialog.showMsg("Bạn chưa đăng nhập.");
                return;
            }
            Banner.createAndShow(cc.find("Canvas").parent);
        }

        actEventMission() {
            if (!Configs.Login.IsLogin) {
                App.instance.alertDialog.showMsg("Bạn chưa đăng nhập.");
                return;
            }
            PopupMailBox.createAndShow(this.popups, true);
        }

        private getMailNotRead() {
            Http.get(Configs.App.API, { "c": 405, "nn": Configs.Login.Nickname, "p": 1 }, (err, res) => {
                if (err != null) return;
                if (res["success"]) {
                    Configs.Login.MailCount = res["mailNotRead"];
                    BroadcastReceiver.send(BroadcastReceiver.UPDATE_MAIL_COUNT);
                    for (var i = 0; i < res.transactions.length; i++) {
                        if (res.transactions[i].type && res.transactions[i].type != "Normal" && !res.transactions[i].status) {
                            App.instance.alertDialog.showMsg("Có thư mới :\n" + res.transactions[i].content);
                            Http.get(Configs.App.API, { "c": 404, "mid": res.transactions[i].mail_id }, (err, res) => {
                                if (err != null) return;
                                Configs.Login.MailCount -= 1;
                                BroadcastReceiver.send(BroadcastReceiver.UPDATE_MAIL_COUNT);
                            });
                            break;
                        }
                    }
                }
            });
        }
    }
}
export default Lobby.LobbyController;