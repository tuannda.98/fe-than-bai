import MiniGameNetworkClient from "../../scripts/networks/MiniGameNetworkClient";
import InPacket from "../../scripts/networks/Network.InPacket";
import App from "../../scripts/common/App";
import cmd from "./Lobby.Cmd";

import * as cmdTX from "../../subpackages/TaiXiuDouble/TaiXiuMini/src/TaiXiuMini.Cmd"

const { ccclass, property } = cc._decorator;

@ccclass
export default class ButtonMiniGame extends cc.Component {

    @property(cc.Label)
    labelTime: cc.Label = null;

    @property(cc.Node)
    button: cc.Node = null;

    @property(cc.Node)
    panel: cc.Node = null;

    @property(cc.Node)
    container: cc.Node = null;

    @property(cc.Node)
    taiNode: cc.Node = null;

    @property(cc.Node)
    xiuNode: cc.Node = null;

    private buttonClicked = true;
    private buttonMoved = cc.Vec2.ZERO;

    onLoad() {
        this.panel.active = false;
        this.button.active = false;
        this.labelTime.string = "00";

        this.button.on(cc.Node.EventType.TOUCH_START, (event: cc.Event.EventTouch) => {
            this.buttonClicked = true;
            this.buttonMoved = cc.Vec2.ZERO;
        }, this);

        this.button.on(cc.Node.EventType.TOUCH_MOVE, (event: cc.Event.EventTouch) => {
            this.buttonMoved = this.buttonMoved.add(event.getDelta());
            if (this.buttonClicked) {
                if (Math.abs(this.buttonMoved.x) > 30 || Math.abs(this.buttonMoved.y) > 30) {
                    let pos = this.button.position;
                    pos.x += this.buttonMoved.x;
                    pos.y += this.buttonMoved.y;
                    this.button.position = pos;
                    this.buttonClicked = false;
                }
            } else {
                let pos = this.button.position;
                pos.x += event.getDeltaX();
                pos.y += event.getDeltaY();
                this.button.position = pos;
            }
        }, this);

        this.button.on(cc.Node.EventType.TOUCH_END, (event: cc.Event.EventTouch) => {
            if (this.buttonClicked) {
                this.actButton();
            }
        }, this);

        MiniGameNetworkClient.getInstance().addListener((data: Uint8Array) => {
            let inpacket = new InPacket(data);
            let res = null;
            switch (inpacket.getCmdId()) {
                case cmdTX.cmd.Code.UPDATE_TIME:
                    res = new cmdTX.cmd.ReceiveUpdateTime(data);
                    this.labelTime.string = res.remainTime > 9 ? res.remainTime.toString() : "0" + res.remainTime;
                    if (res.bettingState) {
                        this.xiuNode.stopAllActions();
                        this.taiNode.active = false;
                        this.taiNode.stopAllActions();
                        this.xiuNode.active = false;
                    }
                    break;

                case cmdTX.cmd.Code.DICES_RESULT:
                    res = new cmdTX.cmd.ReceiveDicesResult(data);
                    var lastScore = res.dice1 + res.dice2 + res.dice3;
                    if (lastScore < 11) {
                        this.xiuNode.active = true;
                        this.xiuNode.stopAllActions();
                        this.xiuNode.runAction(cc.repeatForever(cc.sequence(cc.scaleTo(0.2, 1.1), cc.scaleTo(0.2, 0.9))));
                        this.taiNode.active = false;
                    } else {
                        this.taiNode.active = true;
                        this.taiNode.stopAllActions();
                        this.taiNode.runAction(cc.repeatForever(cc.sequence(cc.scaleTo(0.2, 1.1), cc.scaleTo(0.2, 0.9))));
                        this.xiuNode.active = false;
                    }
                    break;
            }
        }, this);
        MiniGameNetworkClient.getInstance().send(new cmdTX.cmd.SendScribe());
    }

    show() {
        this.panel.active = false;
        this.button.active = true;
        this.labelTime.string = "00";
    }

    hidden() {
        this.panel.active = false;
        this.button.active = false;
    }

    showTimeTaiXiu(isShow: boolean) {
        // console.log(isShow);
        // this.labelTime.node.parent.active = isShow;
    }

    actButton() {
        this.panel.active = true;
        this.panel.scaleY = 0;
        this.panel.runAction(
            cc.scaleTo(0.2, 1).easing(cc.easeBackOut())
        );
        this.button.active = false;
    }

    actHidden() {
        this.panel.runAction(cc.sequence(
            cc.scaleTo(0.2, 0),
            cc.callFunc(() => { this.panel.active = false; })
        ));
        this.button.active = true;
    }

    actTaiXiu() {
        App.instance.openGameTaiXiuMini(null);
        this.actHidden();
    }

    actMiniPoker() {
        App.instance.openGameMiniPoker(null);
        this.actHidden();
    }

    actSlot3x3() {
        App.instance.openGameSlot3x3(null);
        this.actHidden();
    }

    actCaoThap() {
        App.instance.openGameCaoThap(null);
        this.actHidden();
    }

    actBauCua() {
        App.instance.openGameBauCua(null);
        this.actHidden();
    }

    actChimDien() {
        App.instance.alertDialog.showMsg("Game sắp ra mắt.");
        this.actHidden();
    }

    actOanTuTi() {
        App.instance.openGameOanTuTi(null);
        this.actHidden();
    }
}