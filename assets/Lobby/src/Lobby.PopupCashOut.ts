import Dialog from "../../scripts/common/Dialog";
import App from "../../scripts/common/App";
import Http from "../../scripts/common/Http";
import Configs from "../../scripts/common/Configs";
import PopupShop from "./Lobby.PopupShop";
import Dropdown from "../../scripts/customui/CustomUI.Dropdown";
import InPacket from "../../scripts/networks/Network.InPacket";
import Utils from "../../scripts/common/Utils";
import MiniGameNetworkClient from "../../scripts/networks/MiniGameNetworkClient";
import cmd from "./Lobby.Cmd";
import BroadcastReceiver from "../../scripts/common/BroadcastReceiver";
import PopupOTP from "./Lobby.PopupOTP";
import LobbyController from "./Lobby.LobbyController";
import PopupCardInfo from "./Lobby.PopupCardInfo";

const { ccclass, property } = cc._decorator;

@ccclass("PopupCashout.TabDaiLy")
class TabDaiLy {
	@property(cc.Node)
	node: cc.Node = null;
	@property(cc.Node)
	itemTemplate: cc.Node = null;

	start() {

	}

	reset() {
		for (let i = 0; i < this.itemTemplate.parent.childrenCount; i++) {
			this.itemTemplate.parent.children[i].active = false;
		}
		this.loadData();
	}

	private getItem(): cc.Node {
		let item = null;
		for (let i = 0; i < this.itemTemplate.parent.childrenCount; i++) {
			let node = this.itemTemplate.parent.children[i];
			if (node != this.itemTemplate && !node.active) {
				item = node;
				break;
			}
		}
		if (item == null) {
			item = cc.instantiate(this.itemTemplate);
			item.parent = this.itemTemplate.parent;
		}
		item.active = true;
		return item;
	}

	private loadData() {
		App.instance.showLoading(true);
		Http.get(Configs.App.API, { "c": 401 }, (err, res) => {
			App.instance.showLoading(false);
			if (err != null) return;
			if (res["success"]) {
				for (let i = 0; i < res["transactions"].length; i++) {
					let itemData = res["transactions"][i];
					let nickname = itemData["nickName"];
					let item = this.getItem();
					item.getChildByName("bg").opacity = i % 2 == 0 ? 10 : 0;
					item.getChildByName("No.").getComponent(cc.Label).string = (i + 1).toString();
					item.getChildByName("Fullname").getComponent(cc.Label).string = itemData["fullName"];
					item.getChildByName("Nickname").getComponent(cc.Label).string = nickname;
					item.getChildByName("Phone").getComponent(cc.Label).string = itemData["mobile"];
					item.getChildByName("Phone").color = cc.Color.WHITE;
					item.getChildByName("Phone").off("click");
					if (itemData["mobile"] && itemData["mobile"].trim().length > 0 && itemData["mobile"].trim()[0] != "0") {
						item.getChildByName("Phone").color = cc.Color.CYAN;
						item.getChildByName("Phone").on("click", () => {
							App.instance.openTelegram(itemData["mobile"]);
						});
					}
					item.getChildByName("Address").getComponent(cc.Label).string = itemData["address"];
					item.getChildByName("BtnFacebook").off("click");
					item.getChildByName("BtnFacebook").on("click", () => {
						cc.sys.openURL(itemData["facebook"]);
					});
					item.getChildByName("BtnTransfer").off("click");
					item.getChildByName("BtnTransfer").on("click", () => {
						// PopupShop.createAndShowTransfer(App.instance.popups, nickname);
					});
				}
			}
		});
	}
}

@ccclass("PopupCashout.TabBank")
class TabBank {
	@property(cc.Node)
	node: cc.Node = null;
	@property(Dropdown)
	dropdownBank: Dropdown = null;
	@property(cc.EditBox)
	edbAccountName: cc.EditBox = null;
	@property(cc.EditBox)
	edbAccountNumber: cc.EditBox = null;
	@property(Dropdown)
	dropdownCoin: Dropdown = null;
	@property(cc.EditBox)
	edbOTP: cc.EditBox = null;
	@property(cc.Button)
	btnGetOTP: cc.Button = null;
	@property(cc.Button)
	btnSubmit: cc.Button = null;

	listBanks = ["Agribank", "BIDV", "Vietcombank", "Vietinbank", "VP Bank", "ACB", "ABbank", "Bac A Bank", "Viet Capital Bank", "Bao Viet bank",
		"Lien Viet Post Bank", "CIMB Bank", "PVCombank", "Oceanbank", "GPBank", "Dong A Bank", "Seabank", "Maritime Bank",
		"Hong Leong VN", "Indovina Bank", "Industrial Bank Of Korea", "Kien Long Bank", "Techcombank", "VRB", "Nam A Bank",
		"HD Bank", "OCB", "Public Bank VN", "MB", "NCB", "VIB", "SCB", "SHB", "Saigonbank", "Sacombank", "Shihan VN", "Tien Phong Bank",
		"UOB VN", "Viet A Bank", "Vietbank", "Woori Bank Viet Nam", "PGBank", "Eximbank"];

	factors = [100000, 200000, 300000, 500000, 1000000, 2000000, 5000000, 10000000];

	private bankValues = null;
	private bankNames = null;

	start() {
		this.btnSubmit.node.on("click", () => {
			this.submit();
		});
		this.btnGetOTP.node.on("click", () => {
			this.submit(true);
		});
	}

	setBankData(rewards) {
		this.bankNames = { bankNames: ["CHỌN NGÂN HÀNG"], bankCodes: [-1] };
		this.bankValues = { moneys: ["CHỌN MỆNH GIÁ"], golds: [""], ids: [-1] };
		for (let [key, value] of rewards) {
			if (value.type == Configs.App.REWARD_TYPE.BANK) {
				value.items.forEach(item => {
					this.bankValues.moneys.push(Utils.formatNumber(item.money));
					this.bankValues.golds.push(Utils.formatNumber(item.gold));
					this.bankValues.ids.push(item.id);
				});
			}
		}
		this.dropdownCoin.setOptions(this.bankValues.moneys);
		this.dropdownCoin.setOnValueChange((idx) => {
			let amount = Utils.stringToInt(this.bankValues.moneys[idx]);
			if (isNaN(parseInt(this.bankValues.moneys[idx])) || amount <= 0) {
				this.bankNames = { bankNames: ["CHỌN NGÂN HÀNG"], bankCodes: [-1] };
				this.dropdownBank.setOptions(this.bankNames.bankNames);
				this.dropdownBank.setValue(0);
			} else
				this.getListBank(amount);
		});
		this.dropdownCoin.setValue(0);

		this.dropdownBank.setOptions(this.bankNames.bankNames);
		this.dropdownBank.setValue(0);
	}

	getListBank(amount) {
		App.instance.showLoading(true);
		var params = {
			"c": 6010,
			"un": Configs.Login.Username,
			"demonition": amount
		};
		Http.get(Configs.App.API, params, (err, res) => {
			App.instance.showLoading(false);
			if (err != null) {
				App.instance.alertDialog.showMsg("Thao tác không thành công, vui lòng thử lại sau.");
				return;
			}
			if (res.data && res.data.data)
				this.setListBank(res.data.data);
			else
				App.instance.alertDialog.showMsg(res.desc);
		});
	}

	setListBank(banks) {
		this.bankNames = { bankNames: ["CHỌN NGÂN HÀNG"], bankCodes: [-1] };
		banks.forEach(bank => {
			this.bankNames.bankNames.push(bank.bankName);
			this.bankNames.bankCodes.push(bank.bankCode);
		});
		this.dropdownBank.setOptions(this.bankNames.bankNames);
		this.dropdownBank.setValue(0);
	}

	reset() {
		this.dropdownBank.setValue(0);
		this.edbAccountName.string = "";
		this.edbAccountNumber.string = "";
		this.dropdownCoin.setValue(1); //lấy giá trị đầu tiên trong list value để get list bank
		this.dropdownCoin.clickValue(1);
	}

	private submit(getOTP: boolean = false) {
		let ddBankValue = this.dropdownBank.getValue();
		let accountName = this.edbAccountName.string.trim();
		let accountNumber = this.edbAccountNumber.string.trim();
		let ddCoinValue = this.dropdownCoin.getValue();
		let otp = this.edbOTP.string.trim();

		if (ddBankValue == 0) {
			App.instance.alertDialog.showMsg("Vui lòng chọn ngân hàng.");
			return;
		}
		if (accountName.length == 0) {
			App.instance.alertDialog.showMsg("Tên tài khoản không được để trống.");
			return;
		}
		if (accountNumber.length == 0) {
			App.instance.alertDialog.showMsg("Số tài khoản không được để trống.");
			return;
		}
		if (ddCoinValue == 0) {
			App.instance.alertDialog.showMsg("Vui lòng chọn mệnh giá");
			return;
		}
		// if (!getOTP && otp.length == 0) {
		// 	App.instance.alertDialog.showMsg("Mã OTP không được để trống.");
		// 	return;
		// }
		App.instance.confirmDialog.show2("Tất cả các thông tin bạn điền là chính xác?", (isConfirm) => {
			if (isConfirm) {
				App.instance.showLoading(true);
				let params = {
					"c": 6000,
					"un": Configs.Login.Username,
					"request_id": this.bankValues.ids[ddCoinValue],
					"request_acc_name": accountName,
					"request_acc_no": accountNumber,
					"request_bank_code": this.bankNames.bankCodes[ddBankValue]
				};
				// if (!getOTP) params["otp"] = otp;
				Http.get(Configs.App.API, params, (err, json) => {
					App.instance.showLoading(false);
					if (err != null) {
						App.instance.alertDialog.showMsg("Thao tác không thành công, vui lòng thử lại sau.");
						return;
					}
					BroadcastReceiver.send(BroadcastReceiver.USER_UPDATE_COIN);
					if (json.hasOwnProperty("code"))
						switch (json["code"]) {
							case 0:
								App.instance.alertDialog.showMsg("Hệ thống đã tiếp nhận và xử lý.");
								Configs.Login.Coin = json["currentMoney"];
								BroadcastReceiver.send(BroadcastReceiver.USER_UPDATE_COIN);
								break;
							case 400:
								App.instance.alertDialog.showMsg("Dữ liệu gửi lên không hợp lệ.");
								break;
							case -11:
								App.instance.alertDialog.showMsg("Mệnh giá gửi lên không hợp lệ.");
								break;
							case -10:
								App.instance.alertDialog.showMsg("Số dư không đủ.");
								break;
							case -99:
								App.instance.alertDialog.showMsg("Tài khoản của bạn không được mở tính năng này, liên hệ cskh để được hỗ trợ.");
								break;
							default:
								App.instance.alertDialog.showMsg("Thao tác không thành công, vui lòng thử lại sau.");
								break;
						}
					else
						App.instance.alertDialog.showMsg(json.desc);
				});
			}
		});
	}
}

@ccclass("PopupCashout.TabCard")
class TabCard {
	@property(cc.Node)
	node: cc.Node = null;
	@property(Dropdown)
	dropdownTelco: Dropdown = null;
	@property(Dropdown)
	dropdownAmount: Dropdown = null;

	@property(cc.Node)
	listFactor: cc.Node = null;
	@property(cc.Node)
	itemRate: cc.Node = null;

	private telcoValues = [];

	setCardData(rewards) {
		let telcoNames = ["CHỌN NHÀ MẠNG"];
		this.telcoValues = [{ moneys: ["CHỌN MỆNH GIÁ"], golds: [""], ids: [-1] }];
		for (let [key, value] of rewards) {
			if (value.type == Configs.App.REWARD_TYPE.CARD) {
				let telcoName = Configs.App.TELCO_NAME[key];
				if (telcoName)
					telcoNames.push(telcoName);
				let values = { moneys: ["CHỌN MỆNH GIÁ"], golds: [""], ids: [-1] };
				value.items.forEach(item => {
					values.moneys.push(Utils.formatNumber(item.money));
					values.golds.push(Utils.formatNumber(item.gold));
					values.ids.push(item.id);
				});
				this.telcoValues.push(values);
			}
		}
		this.dropdownTelco.setOptions(telcoNames);
		this.dropdownTelco.setOnValueChange((idx) => {
			this.dropdownAmount.setOptions(this.telcoValues[idx].moneys);
			this.dropdownAmount.setValue(0);

			this.listFactor.removeAllChildren();
			this.itemRate.active = false;
			let telco = this.telcoValues[idx];
			for (let i = 1; i < telco.moneys.length; i++) {
				let item = cc.instantiate(this.itemRate);
				item.x = 0;
				item.parent = this.listFactor;
				item.getChildByName("Amount").getComponent(cc.Label).string = telco.moneys[i];
				item.getChildByName("Value").getComponent(cc.Label).string = telco.golds[i];
				item.active = true;
			}
		});
		this.dropdownTelco.setValue(0);

		this.dropdownAmount.setOptions(this.telcoValues[0].moneys);
		this.dropdownAmount.setValue(0);
	}

	reset() {
		this.dropdownTelco.setValue(0);
		this.dropdownAmount.setValue(0);
	}

	submit() {
		let ddTelcoValue = this.dropdownTelco.getValue();
		let ddAmountValue = this.dropdownAmount.getValue();
		if (ddTelcoValue == 0) {
			App.instance.alertDialog.showMsg("Vui lòng chọn nhà mạng.");
			return;
		}
		if (ddAmountValue == 0) {
			App.instance.alertDialog.showMsg("Vui lòng chọn mệnh giá.");
			return;
		}
		App.instance.showLoading(true, 60);
		var params = {
			"c": 6000,
			"un": Configs.Login.Username,
			"request_id": this.telcoValues[ddTelcoValue].ids[ddAmountValue]
		};
		Http.get(Configs.App.API, params, (err, res) => {
			App.instance.showLoading(false);
			if (err != null) {
				App.instance.alertDialog.showMsg("Thao tác không thành công, vui lòng thử lại sau.");
				return;
			}
			BroadcastReceiver.send(BroadcastReceiver.USER_UPDATE_COIN);
			App.instance.alertDialog.showMsg(res.desc);
		});
	}
}

@ccclass("PopupCashout.TabViDienTu")
class TabViDienTu {
	@property(cc.Node)
	node: cc.Node = null;

	@property(cc.EditBox)
	edbPhone: cc.EditBox = null;
	@property(cc.EditBox)
	edbName: cc.EditBox = null;
	@property(Dropdown)
	dropdownAmount: Dropdown = null;
	@property(cc.EditBox)
	edbOTP: cc.EditBox = null;
	@property(cc.Button)
	btnGetOTP: cc.Button = null;
	@property(cc.Button)
	btnSubmit: cc.Button = null;

	@property(cc.Node)
	listFactor: cc.Node = null;
	@property(cc.Node)
	itemRate: cc.Node = null;

	private started = false;
	private walletValues;

	private start() {
		this.btnSubmit.node.on("click", () => {
			this.submit();
		});
		this.btnGetOTP.node.on("click", () => {
			this.submit(true);
		});
	}

	setWalletData(rewards) {
		if (this.started) return;
		this.started = true;

		this.walletValues = { moneys: ["CHỌN MỆNH GIÁ"], golds: [""], ids: [-1] };
		for (let [key, value] of rewards) {
			if (value.type == Configs.App.REWARD_TYPE.MOMO) {
				value.items.forEach(item => {
					this.walletValues.moneys.push(Utils.formatNumber(item.money));
					this.walletValues.golds.push(Utils.formatNumber(item.gold));
					this.walletValues.ids.push(item.id);
				});
			}
		}
		this.dropdownAmount.setOptions(this.walletValues.moneys);

		this.listFactor.removeAllChildren();
		this.itemRate.active = false;
		for (let i = 1; i < this.walletValues.moneys.length; i++) {
			let item = cc.instantiate(this.itemRate);
			item.x = 0;
			item.parent = this.listFactor;
			item.getChildByName("Amount").getComponent(cc.Label).string = this.walletValues.moneys[i];
			item.getChildByName("Value").getComponent(cc.Label).string = this.walletValues.golds[i];
			item.active = true;
		}
	}

	reset() {
		this.start();
		this.edbPhone.string = "";
		this.edbName.string = "";
		this.dropdownAmount.setValue(0);
	}

	private submit(getOTP: boolean = false) {
		let ddAmountValue = this.dropdownAmount.getValue();
		let accountName = this.edbName.string.trim();
		let accountPhone = this.edbPhone.string.trim();
		let otp = this.edbOTP.string.trim();

		if (ddAmountValue == 0) {
			App.instance.alertDialog.showMsg("Vui lòng chọn mệnh giá.");
			return;
		}
		if (accountName.length == 0) {
			App.instance.alertDialog.showMsg("Tên tài khoản không được để trống.");
			return;
		}
		if (accountPhone.length == 0) {
			App.instance.alertDialog.showMsg("Số điện thoại không được để trống.");
			return;
		}
		// if (!getOTP && otp.length == 0) {
		// 	App.instance.alertDialog.showMsg("Mã OTP không được để trống.");
		// 	return;
		// }
		
		App.instance.confirmDialog.show2("Tất cả các thông tin bạn điền là chính xác?", (isConfirm) => {
			if (isConfirm) {
				App.instance.showLoading(true);
				let params = {
					"c": 6000,
					"un": Configs.Login.Username,
					"request_id": this.walletValues.ids[ddAmountValue],
					"account_no": accountPhone,
					"account_name": accountName,
				};
				// if (!getOTP) params["otp"] = otp;
				Http.get(Configs.App.API, params, (err, json) => {
					App.instance.showLoading(false);
					if (err != null) {
						App.instance.alertDialog.showMsg("Thao tác không thành công, vui lòng thử lại sau.");
						return;
					}
					BroadcastReceiver.send(BroadcastReceiver.USER_UPDATE_COIN);
					App.instance.alertDialog.showMsg(json.desc);
				});
			}
		});
	}
}

@ccclass("PopupCashout.TabBitCoin")
class TabBitCoin {
	@property(cc.Node)
	node: cc.Node = null;

	start() {
	}

	reset() {
	}

	submit() {

	}
}

@ccclass
export default class PopupCashOut extends Dialog {

	private static instance: PopupCashOut = null;
	private static initing: boolean = false;
	public static createAndShow(parent: cc.Node) {
		if (!this.initing) {
			if (this.instance == null || this.instance.node == null) {
				this.initing = true;
				App.instance.loadPrefab("Lobby/PopupCashOut", (err, prefab) => {
					this.initing = false;
					if (err != null) {
						App.instance.alertDialog.showMsg(err);
						return;
					}
					let go = cc.instantiate(prefab);
					go.parent = parent;
					this.instance = go.getComponent(PopupCashOut);
					this.instance.show(2);
				});
			} else {
				this.instance.show(2);
			}
		}
	}

	@property(cc.ToggleContainer)
	tabs: cc.ToggleContainer = null;
	@property(cc.Node)
	tabContents: cc.Node = null;

	@property(TabDaiLy)
	tabDaiLy: TabDaiLy = new TabDaiLy();
	@property(TabBank)
	tabBank: TabBank = new TabBank();
	@property(TabCard)
	tabCard: TabCard = new TabCard();
	@property(TabViDienTu)
	tabViDienTu: TabViDienTu = new TabViDienTu();
	@property(TabBitCoin)
	tabBitCoin: TabBitCoin = new TabBitCoin();

	private tabSelectedIdx = 0;
	private rewardByProvider = new Map();

	start() {
		for (let i = 0; i < this.tabs.toggleItems.length; i++) {
			this.tabs.toggleItems[i].node.on("click", () => {
				this.tabSelectedIdx = i;
				this.onTabChanged();
			});
		}

		MiniGameNetworkClient.getInstance().addListener((data) => {
			let inpacket = new InPacket(data);
			console.log(inpacket.getCmdId());
			switch (inpacket.getCmdId()) {
				case cmd.Code.BUY_CARD:
					{
						App.instance.showLoading(false);
						let res = new cmd.ResBuyCard(data);
						switch (res.error) {
							case 0:
								PopupOTP.createAndShow(App.instance.popups);
								break;
							case 1:
								App.instance.alertDialog.showMsg("Mất kết nối đến server!");
								break;
							case 2:
								App.instance.alertDialog.showMsg("Tài khoản hiện đang bị cấm đổi thưởng!");
								break;
							case 3:
								App.instance.alertDialog.showMsg("Tài khoản không đủ số dư khả dụng!");
								break;
							case 9:
								App.instance.alertDialog.showMsg("Để thực hiện chức năng đổi thẻ, tài khoản cần đăng ký bảo mật! Bạn có muốn đăng ký bảo mật luôn không?");
								break;
							case 10:
								App.instance.alertDialog.showMsg("Chức năng này sẽ hoạt động sau 24h kích hoạt bảo mật thành công!");
								break;
							case 20:
								App.instance.alertDialog.showMsg("Mức đổi vượt quá hạn mức trong ngày của tài khoản. Vui lòng đợi đến hôm sau để thực hiện lại giao dịch!");
								break;
							case 21:
								App.instance.alertDialog.showMsg("Không thể đổi quá hạn mức trong ngày của hệ thống. Vui lòng đợi đến hôm sau để thực hiện lại giao dịch!");
								break;
							default:
								App.instance.alertDialog.showMsg("Lỗi " + res.error + ". vui lòng thử lại sau.");
								break;
						}
					}
					break;
				case cmd.Code.BUY_CARD_RESULT:
					{
						App.instance.showLoading(false);
						let res = new cmd.ResBuyCardResult(data);
						switch (res.error) {
							case 0:
								Configs.Login.Coin = res.currentMoney;
								BroadcastReceiver.send(BroadcastReceiver.USER_UPDATE_COIN);
								let resJson = JSON.parse(res.softpin)[0];
								let telco = resJson["provider"];
								let amount = resJson["amount"];
								let code = resJson["pin"];
								let serial = resJson["serial"];
								PopupCardInfo.createAndShow(App.instance.popups, telco, amount, code, serial);
								break;
							case 1:
								App.instance.alertDialog.showMsg("Kết nối mạng không ổn định. Vui lòng thử lại sau!");
								break;
							case 2:
								App.instance.alertDialog.showMsg("Tài khoản hiện đang bị cấm đổi thưởng!");
								break;
							case 3:
								App.instance.alertDialog.showMsg("Tài khoản không đủ số dư khả dụng!");
								break;
							case 9:
								App.instance.alertDialog.showMsg("Để thực hiện chức năng đổi thẻ, tài khoản cần đăng ký bảo mật! Bạn có muốn đăng ký bảo mật luôn không?");
								break;
							case 10:
								App.instance.alertDialog.showMsg("Chức năng này sẽ hoạt động sau 24h kích hoạt bảo mật thành công!");
								break;
							case 20:
								App.instance.alertDialog.showMsg("Mức đổi vượt quá hạn mức trong ngày của tài khoản. Vui lòng đợi đến hôm sau để thực hiện lại giao dịch!");
								break;
							case 21:
								App.instance.alertDialog.showMsg("Không thể đổi quá hạn mức trong ngày của hệ thống. Vui lòng đợi đến hôm sau để thực hiện lại giao dịch!");
								break;
							case 22:
								App.instance.alertDialog.showMsg("Số lượng thẻ đổi đã quá hạn mức. Bạn vui lòng quay trở lại sau!");
								break;
							case 30:
								App.instance.alertDialog.showMsg("Giao dịch đang chờ xử lý!");
								break;
							default:
								App.instance.alertDialog.showMsg("Lỗi " + res.error + ". vui lòng thử lại sau.");
								break;
						}
					}
					break;
			}
		}, this);

		this.tabDaiLy.start();
		this.tabBitCoin.start();
		this.tabBank.start();
	}

	show(idx = 0) {
		super.show();

		this.getCashOutCfg();

		this.tabSelectedIdx = idx;
		this.tabs.toggleItems[this.tabSelectedIdx].isChecked = true;
		this.onTabChanged();
	}

	public actCardSubmit() {
		this.tabCard.submit();
	}

	private onTabChanged() {
		for (let i = 0; i < this.tabContents.childrenCount; i++) {
			this.tabContents.children[i].active = i == this.tabSelectedIdx;
		}
		for (let j = 0; j < this.tabs.toggleItems.length; j++) {
			this.tabs.toggleItems[j].node.getComponentInChildren(cc.Label).node.color = j == this.tabSelectedIdx ? cc.Color.YELLOW : cc.Color.WHITE;
		}
		switch (this.tabSelectedIdx) {
			case 0:
				this.tabDaiLy.reset();
				break;
			case 1:
				this.tabBank.reset();
				break;
			case 2:
				this.tabCard.reset();
				break;
			case 3:
				this.tabViDienTu.reset();
				break;
			case 4:
				this.tabBitCoin.reset();
				break;
		}
	}

	private getCashOutCfg() {
		App.instance.showLoading(true);
		var params = {
			"c": 6002,
		};
		Http.get(Configs.App.API, params, (err, res) => {
			App.instance.showLoading(false);
			if (err != null) {
				App.instance.alertDialog.showMsg("Thao tác không thành công, vui lòng thử lại sau.");
				return;
			}
			this.onCashOutCfgDone(res);
		});
	}

	private onCashOutCfgDone(res) {
		this.rewardByProvider = new Map();
		res.forEach(reward => {
			reward.provider = this.getProvider(reward.provider);
			if (this.rewardByProvider.get(reward.provider) === undefined)
				this.rewardByProvider.set(reward.provider, {
					type: reward.type,
					items: [reward]
				});
			else {
				var currRewards = this.rewardByProvider.get(reward.provider).items;
				currRewards.push(reward);
			}
		});
		this.tabCard.setCardData(this.rewardByProvider);
		this.tabViDienTu.setWalletData(this.rewardByProvider);
		this.tabBank.setBankData(this.rewardByProvider);
	}

	private getProvider(provider: String) {
		var prvd = "";
		switch (provider) {
			case "MOBI":
			case "MOBIFONE":
			case "VMS":
				prvd = "VMS";
				break;
			case "VINA":
			case "VINAPHONE":
			case "VNP":
				prvd = "VNP";
				break;
			case "VT":
			case "VTT":
			case "VTE":
			case "VIETTEL":
				prvd = "VTT";
				break;
			default:
				prvd = provider.toUpperCase();
				break;
		}
		return prvd;
	}
}