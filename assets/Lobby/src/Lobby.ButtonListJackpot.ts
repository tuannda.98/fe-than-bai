import cmd from "./Lobby.Cmd";
import Tween from "../../scripts/common/Tween";

const { ccclass, property } = cc._decorator;

@ccclass
export default class ButtonListJackpot extends cc.Component {

    @property(cc.Node)
    button: cc.Node = null;
    @property(cc.Node)
    container: cc.Node = null;

    @property(cc.ToggleContainer)
    togglesBlind: cc.ToggleContainer = null;

    @property(cc.Label)
    labelsSlot1: cc.Label = null;
    @property(cc.Label)
    labelsSlot2: cc.Label = null;
    @property(cc.Label)
    labelsSlot3: cc.Label = null;
    @property(cc.Label)
    labelsSlot4: cc.Label = null;
    @property(cc.Label)
    labelsSlot5: cc.Label = null;
    @property(cc.Label)
    labelsSlot6: cc.Label = null;
    @property(cc.Label)
    labelsMiniPoker: cc.Label = null;
    @property(cc.Label)
    labelsCaoThap: cc.Label = null;
    @property(cc.Label)
    labelsSlot3x3: cc.Label = null;

    private buttonClicked = true;
    private buttonMoved = cc.Vec2.ZERO;
    private animate = false;

    private static lastRes: cmd.ResUpdateJackpotSlots = null;
    private selectedIdx = 0;

    onLoad() {
        this.container.active = false;

        this.button.on(cc.Node.EventType.TOUCH_START, (event: cc.Event.EventTouch) => {
            this.buttonClicked = true;
            this.buttonMoved = cc.Vec2.ZERO;
        }, this);

        this.button.on(cc.Node.EventType.TOUCH_MOVE, (event: cc.Event.EventTouch) => {
            this.buttonMoved = this.buttonMoved.add(event.getDelta());
            if (this.buttonClicked) {
                if (Math.abs(this.buttonMoved.x) > 30 || Math.abs(this.buttonMoved.y) > 30) {
                    let pos = this.node.position;
                    pos.x += this.buttonMoved.x;
                    pos.y += this.buttonMoved.y;
                    this.node.position = pos;
                    this.buttonClicked = false;
                }
            } else {
                let pos = this.node.position;
                pos.x += event.getDeltaX();
                pos.y += event.getDeltaY();
                this.node.position = pos;
            }
        }, this);

        this.button.on(cc.Node.EventType.TOUCH_END, (event: cc.Event.EventTouch) => {
            if (this.buttonClicked) {
                this.toggleShowPanel();
            }
        }, this);

        for (let i = 0; i < this.togglesBlind.toggleItems.length; i++) {
            this.togglesBlind.toggleItems[i].node.on("toggle", () => {
                this.selectedIdx = i;
                this.updateJackpot(0.3);
            });
        }
    }

    private toggleShowPanel() {
        if (this.animate) return;
        this.animate = true;
        if (!this.container.active) {
            this.container.stopAllActions();
            this.container.active = true;
            this.container.scaleY = 0;
            this.container.runAction(cc.sequence(
                cc.scaleTo(0.2, 1).easing(cc.easeBackOut()),
                cc.callFunc(() => {
                    this.animate = false;
                })
            ));
        } else {
            this.container.stopAllActions();
            this.container.runAction(cc.sequence(
                cc.scaleTo(0.2, 1, 0).easing(cc.easeBackIn()),
                cc.callFunc(() => {
                    this.container.active = false;
                    this.animate = false;
                })
            ));
        }
    }

    setData(res: cmd.ResUpdateJackpotSlots) {
        ButtonListJackpot.lastRes = res;
        this.updateJackpot();
    }

    updateJackpot(duration: number = 4) {
        // cc.log(ButtonListJackpot.lastRes)
        if (ButtonListJackpot.lastRes == null) return;
        var pots = JSON.parse(ButtonListJackpot.lastRes.pots);
        switch (this.selectedIdx) {
            case 0:
                // Tween.numberTo(this.labelsSlot1, 0, duration);
                Tween.numberTo(this.labelsSlot2, pots.ndv['100'].p, duration);
                // Tween.numberTo(this.labelsSlot3, 0, duration);
                Tween.numberTo(this.labelsSlot4, pots.vqv['100'].p, duration);
                Tween.numberTo(this.labelsSlot5, pots.kb['100'].p, duration);
                Tween.numberTo(this.labelsSlot6, pots.sah['100'].p, duration);
                // Tween.numberTo(this.labelsMiniPoker, ButtonListJackpot.lastRes.miniPoker100, duration);
                Tween.numberTo(this.labelsCaoThap, pots.caothap['1000'].p, duration);
                Tween.numberTo(this.labelsSlot3x3, pots.football['100'].p, duration);
                break;
            case 1:
                // Tween.numberTo(this.labelsSlot1, 0, duration);
                Tween.numberTo(this.labelsSlot2, pots.ndv['1000'].p, duration);
                // Tween.numberTo(this.labelsSlot3, 0, duration);
                Tween.numberTo(this.labelsSlot4, pots.vqv['1000'].p, duration);
                Tween.numberTo(this.labelsSlot5, pots.kb['1000'].p, duration);
                Tween.numberTo(this.labelsSlot6, pots.sah['1000'].p, duration);
                // Tween.numberTo(this.labelsMiniPoker, ButtonListJackpot.lastRes.miniPoker1000, duration);
                Tween.numberTo(this.labelsCaoThap, pots.caothap['10000'].p, duration);
                Tween.numberTo(this.labelsSlot3x3, pots.football['1000'].p, duration);
                break;
            case 2:
                // Tween.numberTo(this.labelsSlot1, 0, duration);
                Tween.numberTo(this.labelsSlot2, pots.ndv['10000'].p, duration);
                // Tween.numberTo(this.labelsSlot3, 0, duration);
                Tween.numberTo(this.labelsSlot4, pots.vqv['10000'].p, duration);
                Tween.numberTo(this.labelsSlot5, pots.kb['10000'].p, duration);
                Tween.numberTo(this.labelsSlot6, pots.sah['10000'].p, duration);
                // Tween.numberTo(this.labelsMiniPoker, ButtonListJackpot.lastRes.miniPoker10000, duration);
                Tween.numberTo(this.labelsCaoThap, pots.caothap['50000'].p, duration);
                Tween.numberTo(this.labelsSlot3x3, pots.football['10000'].p, duration);
                break;
        }
    }
}