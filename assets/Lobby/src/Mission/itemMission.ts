import App from "../../../scripts/common/App";
import BroadcastReceiver from "../../../scripts/common/BroadcastReceiver";
import Configs from "../../../scripts/common/Configs";
import Http from "../../../scripts/common/Http";
import Utils from "../../../scripts/common/Utils";
import PopupMission from "../Lobby.PopupMission";

const { ccclass, property } = cc._decorator;

@ccclass
export default class itemMission extends cc.Component {


    @property(cc.Label)
    prizeAmount: cc.Label = null;

    @property(cc.Label)
    descLb: cc.Label = null;


    @property(cc.ProgressBar)
    progressBar: cc.ProgressBar = null;

    @property(cc.Button)
    getBonusBtn: cc.Button = null;

    @property(cc.SpriteFrame)
    iconLst: cc.SpriteFrame[] = [];

    @property(cc.Sprite)
    iconSp: cc.Sprite = null;


    id: string = "";
    init(data, userData) {
        //         bonus: 5000
        // depend: ""
        // desc: "Kích hoạt OTP nhận thưởng"
        // id: "otp"
        // number_target: 1
        // repeat_type: "none"

        this.prizeAmount.string = Utils.formatNumber(data.bonus);
        this.descLb.string = data.desc;
        this.id = data.id;

        userData.forEach(item => {
            if (item.missionId == this.id) {
                this.progressBar.progress = item.numberDoingTarget / data.number_target;
                this.progressBar.node.getChildByName("percent").getComponent(cc.Label).string = item.numberDoingTarget + "/" + data.number_target;
                if (item.receivedBonus) {
                    this.getBonusBtn.node.getChildByName("txt").getComponent(cc.Label).string = "Đã nhận";
                }
                else if (item.numberDoingTarget >= data.number_target) {
                    this.getBonusBtn.node.color = cc.color(255, 255, 255, 255);
                    this.getBonusBtn.node.opacity = 255;
                    this.getBonusBtn.node.on("click", () => {
                        Http.get(Configs.App.API, { "c": 6016, "nn": Configs.Login.Nickname, "mi": this.id }, (err, res) => {
                            if (err != null) return;
                            if (res["is_success"]) {
                                this.getBonusBtn.node.off("click");
                                this.getBonusBtn.interactable = false;
                                this.getBonusBtn.node.getChildByName("txt").getComponent(cc.Label).string = "Đã nhận";
                            } else {
                                App.instance.alertDialog.showMsg(res.desc);
                            }
                        });
                    });
                }
            }
        })

        switch (data.mission_type) {
            case "recharge_card":
                this.iconSp.spriteFrame = this.iconLst[3];
                break;
            case "recharge_bank":
                this.iconSp.spriteFrame = this.iconLst[5];
                break;
            case "recharge_momo":
                this.iconSp.spriteFrame = this.iconLst[4];
                break;
            case "game":
                this.iconSp.spriteFrame = this.iconLst[0];
                break;
            default:
                break;
        }




        // var n = "Nạp thẻ";
        // this.napNgayBtn.node.on("click", () => {
        //     App.instance.popupShop.show();
        //     PopupMission.instance.dismiss();
        // });
        // if (data.channel == "bank") {
        //     n = "Bank";
        //     this.napNgayBtn.node.off("click")
        //     this.napNgayBtn.node.on("click", () => {
        //         App.instance.popupShop.show();
        //         App.instance.popupShop.tabSelectedIdx = 3;
        //         App.instance.popupShop.onTabChanged();
        //         PopupMission.instance.dismiss();
        //     });
        // } else if (data.channel == "momo") {
        //     n = "Momo";
        //     this.napNgayBtn.node.off("click")
        //     this.napNgayBtn.node.on("click", () => {
        //         App.instance.popupShop.show();
        //         App.instance.popupShop.tabSelectedIdx = 2;
        //         App.instance.popupShop.onTabChanged();
        //         PopupMission.instance.dismiss();
        //     });
        // }

        // this.nameDefault.string = n;
        // this.nameCharged.string = n;

        // if (data.userId) {

        //     this.chargedNode.active = true;
        //     this.defaultNode.active = false;

        //     this.chargedAmount.string = "<color=#ffffff>Đã nạp : </c><color=#33ff00>" + Utils.formatNumber(data.chargeAmount) + "</color>";
        //     this.prizeAmount.string = "<color=#ffffff>Thưởng : </c><color=#ffff18>" + Utils.formatNumber(data.bonusAmount) + "</color>";


        //     this.progressBar.progress = data.percent / 100;
        //     this.progressBar.node.getChildByName("percent").getComponent(cc.Label).string = Number.parseFloat(data.percent).toFixed(0) + "%";
        //     var date = new Date(data.endTime);
        //     this.progressBar.node.getChildByName("endDate").getComponent(cc.Label).string = "Hạn nhận : " + date.getDate() + "/" + (date.getMonth() + 1) + "/" + date.getFullYear() + " " + (date.getHours() < 10 ? "0" + date.getHours() : date.getHours()) + ":" + (date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes());

        //     if (data.percent >= 100) {
        //         this.getBonusBtn.node.active = true;
        //         this.napNgayBtn.node.active = false;
        //         this.getBonusBtn.node.on("click", () => {
        //             Http.get(Configs.App.API, { "c": 6014, "nn": Configs.Login.Nickname, "channel": data.channel }, (err, res) => {
        //                 if (err != null) return;
        //                 if (res["is_success"]) {
        //                     this.getBonusBtn.node.off("click");
        //                     this.getBonusBtn.interactable = false;
        //                     this.getBonusBtn.node.getChildByName("txt").getComponent(cc.Label).string = "Đã nhận";
        //                     // Configs.Login.Coin = res.totalMoney;
        //                     // BroadcastReceiver.send(BroadcastReceiver.USER_UPDATE_COIN);
        //                 } else {
        //                     App.instance.alertDialog.showMsg(res.desc);
        //                 }
        //             });
        //         });
        //     } else {
        //         this.getBonusBtn.node.active = false;
        //         this.napNgayBtn.node.active = true;
        //     }

        //     if (data.receivedBonus) {
        //         this.getBonusBtn.node.off("click");
        //         this.getBonusBtn.interactable = false;
        //         this.getBonusBtn.node.getChildByName("txt").getComponent(cc.Label).string = "Đã nhận";
        //     }

        // } else {
        //     this.chargedNode.active = false;
        //     this.defaultNode.active = true;
        //     if (data.channel == "card") {
        //         n = "Thẻ";
        //     }
        //     this.defaultText.string = this.defaultText.string.replace("@@@", n);
        // }


    }
}
