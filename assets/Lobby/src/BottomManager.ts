import App from "../../scripts/common/App";
import Configs from "../../scripts/common/Configs";
import PopupMailBox from "./Lobby.PopupMailBox";
import PopupCashOut from "./Lobby.PopupCashOut";
import PopupMission from "./Lobby.PopupMission";

const { ccclass, property } = cc._decorator;

@ccclass
export default class BottomManager extends cc.Component {

    @property(cc.Prefab)
    prefabGiftCode: cc.Prefab = null;
    @property(cc.Prefab)
    prefabVQMM: cc.Prefab = null;

    actGiftCode() {
        let item = cc.instantiate(this.prefabGiftCode);
        item.getComponent("Lobby.PopupGiftCode").show();
        this.node.addChild(item);
    }

    actVQMM() {
        let item = cc.instantiate(this.prefabVQMM);
        item.getComponent("Lobby.PopupLuckyWheel").show();
        this.node.addChild(item);
    }
    actTelegram() {
        App.instance.openTelegram(Configs.App.getLinkTelegramGroup());
    }

    actAppOTP() {
        App.instance.openTelegram();
    }

    actSupportOnline() {
        cc.sys.openURL(Configs.App.LINK_SUPPORT);
    }

    actFanpage() {
        cc.sys.openURL(Configs.App.getLinkFanpage());
    }

    actEventMission() {
        if (!Configs.Login.IsLogin) {
            App.instance.alertDialog.showMsg("Bạn chưa đăng nhập.");
            return;
        }
        PopupMailBox.createAndShow(App.instance.popups, true);
    }

    accExchange() {
        if (!Configs.Login.IsLogin) {
            App.instance.alertDialog.showMsg("Bạn chưa đăng nhập.");
            return;
        }
        PopupCashOut.createAndShow(App.instance.popups);
    }

    actMission(event, selectedTab?) {
        if (!Configs.Login.IsLogin) {
            App.instance.alertDialog.showMsg("Bạn chưa đăng nhập.");
            return;
        }
        PopupMission.createAndShow(App.instance.popups, selectedTab);
    }
}
