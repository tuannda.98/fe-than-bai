import App from "../../scripts/common/App";
import Configs from "../../scripts/common/Configs";
import Dialog from "../../scripts/common/Dialog";
import Http from "../../scripts/common/Http";
import Utils from "../../scripts/common/Utils";
import MiniGameNetworkClient from "../../scripts/networks/MiniGameNetworkClient";
import InPacket from "../../scripts/networks/Network.InPacket";
import cmd from "./Lobby.Cmd";
import itemMission from "./Mission/itemMission";
import itemX3Mission from "./Mission/itemX3Mission";

const { ccclass, property } = cc._decorator;

@ccclass
export default class PopupMission extends Dialog {

    public static instance: PopupMission = null;
    private static initing: boolean = false;
    public static createAndShow(parent: cc.Node, selectedTab) {
        if (!this.initing) {
            if (this.instance == null || this.instance.node == null) {
                this.initing = true;
                App.instance.loadPrefab("Lobby/PopupMission", (err, prefab) => {
                    this.initing = false;
                    if (err != null) {
                        App.instance.alertDialog.showMsg(err);
                        return;
                    }
                    let go = cc.instantiate(prefab);
                    go.parent = parent;
                    this.instance = go.getComponent(PopupMission);
                    this.instance.show();
                    if (selectedTab) {
                        if (selectedTab == "x3") {
                            PopupMission.instance.leftX3Toggle.isChecked = true;
                        } else if (selectedTab == "tanthu") {
                            PopupMission.instance.leftTanThuToggle.isChecked = true;
                        }
                        // PopupMission.instance.toggleLeft(null, selectedTab);
                    }
                });
            } else {
                this.instance.show();
                if (selectedTab) {
                    if (selectedTab == "x3") {
                        PopupMission.instance.leftX3Toggle.isChecked = true;
                    } else if (selectedTab == "tanthu") {
                        PopupMission.instance.leftTanThuToggle.isChecked = true;
                    }
                    // PopupMission.instance.toggleLeft(null, selectedTab);
                }
            }
        }
    }

    configData: any = null;

    @property(cc.ScrollView)
    scroll: cc.ScrollView = null;

    @property(cc.Node)
    instantiateNode: cc.Node = null;

    @property(cc.Node)
    lightContainer: cc.Node = null;

    @property(cc.Node)
    x3ProcessContent: cc.Node = null;

    @property(cc.Node)
    missionProcessContent: cc.Node = null;

    @property(cc.Node)
    missionTheLeContent: cc.Node = null;

    @property(cc.Node)
    x3TheLeContent: cc.Node = null;

    @property(cc.Node)
    tanThuContent: cc.Node = null;

    @property(cc.Toggle)
    topProcessToggle: cc.Toggle = null;

    @property(cc.Toggle)
    leftX3Toggle: cc.Toggle = null;

    @property(cc.Toggle)
    leftTanThuToggle: cc.Toggle = null;

    start() {
        // MiniGameNetworkClient.getInstance().send(new cmd.ReqMission());
        // MiniGameNetworkClient.getInstance().addListener((data) => {
        //     let inpacket = new InPacket(data);
        //     console.log(inpacket.getCmdId());
        //     switch (inpacket.getCmdId()) {
        //         case cmd.Code.LIST_MISSION:
        //             var res = new cmd.ResMission(data);
        //             this.configData = JSON.parse(res.data).lMisVin;
        //             this.configData.reverse();
        //             // debugger
        //             this.initData();
        //             break;
        //     }
        // }, this);
    }

    show() {
        super.show();
        this.lightContainer.children[0].stopAllActions();
        this.lightContainer.children[0].runAction(cc.repeatForever(cc.sequence(cc.fadeOut(0.5), cc.fadeIn(0.5))));
        this.lightContainer.children[1].stopAllActions();
        this.lightContainer.children[1].runAction(cc.repeatForever(cc.sequence(cc.fadeIn(0.5), cc.fadeOut(0.5))));
        this.initX3Deposit();
    }

    toggleTop(event, eventData) {
        if (this.currentTab == "x3") {
            this.scroll.content.parent.children.forEach(item => {
                item.active = false;
            })
            switch (Number(eventData)) {
                case 1:
                    this.scroll.content = this.x3ProcessContent;
                    this.x3ProcessContent.active = true;
                    break;

                default:
                    this.scroll.content = this.x3TheLeContent;
                    this.x3TheLeContent.active = true;
                    break;
            }
        }

        if (this.currentTab == "mission") {
            this.scroll.content.parent.children.forEach(item => {
                item.active = false;
            })
            switch (Number(eventData)) {
                case 1:
                    this.scroll.content = this.missionProcessContent;
                    this.missionProcessContent.active = true;
                    break;

                default:
                    this.scroll.content = this.missionTheLeContent;
                    this.missionTheLeContent.active = true;
                    break;
            }
        }

    }

    currentTab: string = "tanthu";
    toggleLeft(event, eventData) {
        this.topProcessToggle.isChecked = true;
        this.scroll.content.parent.children.forEach(item => {
            item.active = false;
        })
        switch (event.target.name) {
            case "x3":
                this.scroll.content = this.x3ProcessContent;
                this.x3ProcessContent.active = true;
                break;
            case "tanthu":
                this.scroll.content = this.tanThuContent;
                this.tanThuContent.active = true;
                break;
            case "mission":
                this.initMissionData();
                this.scroll.content = this.missionProcessContent;
                this.missionProcessContent.active = true;
                break;
        }
        this.currentTab = event.target.name;
    }

    initX3Deposit() {
        this.x3ProcessContent.removeAllChildren();
        Http.get(Configs.App.API, { "c": 6013, "nn": Configs.Login.Nickname }, (err, res) => {
            if (err != null) return;
            if (res["is_success"]) {
                var arr = [];
                res.data.forEach(item => {
                    var temp = cc.instantiate(this.instantiateNode.getChildByName("itemX3"));
                    arr.push(item.channel);
                    temp.getComponent(itemX3Mission).init(item);
                    this.x3ProcessContent.addChild(temp);
                    temp.x = 0;
                })

                if (arr.length != 3) {
                    if (arr.indexOf("bank") < 0) {
                        var temp = cc.instantiate(this.instantiateNode.getChildByName("itemX3"));
                        temp.getComponent(itemX3Mission).init({ channel: "bank" });
                        this.x3ProcessContent.addChild(temp);
                        temp.x = 0;
                    }
                    if (arr.indexOf("momo") < 0) {
                        var temp = cc.instantiate(this.instantiateNode.getChildByName("itemX3"));
                        temp.getComponent(itemX3Mission).init({ channel: "momo" });
                        this.x3ProcessContent.addChild(temp);
                        temp.x = 0;
                    }
                    if (arr.indexOf("card") < 0) {
                        var temp = cc.instantiate(this.instantiateNode.getChildByName("itemX3"));
                        temp.getComponent(itemX3Mission).init({ channel: "card" });
                        this.x3ProcessContent.addChild(temp);
                        temp.x = 0;
                    }
                }
            }
        });


    }

    initMissionData() {
        this.missionProcessContent.removeAllChildren();
        Http.get(Configs.App.API, { "c": 6015, "nn": Configs.Login.Nickname }, (err, res) => {
            if (res.is_success) {
                res.missions = res.missions.sort((a, b) => { return a.mission_type - b.mission_type });
                res.missions.forEach(item => {
                    var temp = cc.instantiate(this.instantiateNode.getChildByName("itemMission"));
                    temp.getComponent(itemMission).init(item, res.user_missions);
                    this.missionProcessContent.addChild(temp);
                    temp.x = 0;
                })
                this.scroll.scrollToTop(0.1);
            }
        })
    }

    openOTP() {
        // App.instance.openTelegram();
        App.instance.lobby.popupSecurity.show();
        this.dismiss();
    }

    showGame(name: string) {
        switch (name.toLowerCase()) {
            case "sam":
                App.instance.lobby.actGoToSam(null);
                break;
            case "bacay":
                App.instance.lobby.actGoToBaCay(null);
                break;
            case "binh":
                App.instance.lobby.actGoToMauBinh(null);
                break;
            case "tlmn":
                App.instance.lobby.actGoToTLMN(null);
                break;
            case "poker":
                App.instance.lobby.actGoToPoker(null);
                break;
            case "baicao":
                App.instance.lobby.actGoToBaiCao(null);
                break;
            // case "cotuong":
            //     App.instance.lobby.actGoToCo(null);
            //     break;
            // case "coup":
            //     App.instance.lobby.actGoToSam(null);
            //     break;
            case "taixiu":
                App.instance.lobby.actGameTaiXiu(null);
                break;
            // default:
            //     return name;
        }
        this.dismiss();
    }
}
