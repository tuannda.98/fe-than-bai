
AiCap.png
size: 2048,1024
format: RGBA8888
filter: Linear,Linear
repeat: none
100 (1)
  rotate: false
  xy: 1913, 699
  size: 54, 64
  orig: 54, 64
  offset: 0, 0
  index: -1
100 (2)
  rotate: true
  xy: 1385, 642
  size: 125, 155
  orig: 125, 155
  offset: 0, 0
  index: -1
100 (3)
  rotate: false
  xy: 1681, 764
  size: 142, 171
  orig: 142, 171
  offset: 0, 0
  index: -1
100 (4)
  rotate: false
  xy: 260, 311
  size: 143, 208
  orig: 143, 208
  offset: 0, 0
  index: -1
100 (5)
  rotate: false
  xy: 260, 127
  size: 169, 182
  orig: 169, 182
  offset: 0, 0
  index: -1
100 (6)
  rotate: true
  xy: 510, 319
  size: 159, 190
  orig: 159, 190
  offset: 0, 0
  index: -1
100 (7)
  rotate: false
  xy: 1825, 765
  size: 128, 182
  orig: 128, 182
  offset: 0, 0
  index: -1
100 (8)
  rotate: true
  xy: 516, 480
  size: 129, 191
  orig: 129, 191
  offset: 0, 0
  index: -1
100 (9)
  rotate: false
  xy: 431, 120
  size: 119, 191
  orig: 119, 191
  offset: 0, 0
  index: -1
Group 21
  rotate: false
  xy: 546, 611
  size: 247, 88
  orig: 247, 88
  offset: 0, 0
  index: -1
Hair1nv1
  rotate: false
  xy: 1831, 607
  size: 80, 156
  orig: 80, 156
  offset: 0, 0
  index: -1
Hand4nv1
  rotate: true
  xy: 1386, 937
  size: 85, 374
  orig: 85, 374
  offset: 0, 0
  index: -1
Hipnv1
  rotate: false
  xy: 846, 811
  size: 281, 211
  orig: 281, 211
  offset: 0, 0
  index: -1
Layer 13
  rotate: false
  xy: 2, 143
  size: 256, 420
  orig: 256, 420
  offset: 0, 0
  index: -1
Layer 14
  rotate: false
  xy: 709, 506
  size: 96, 103
  orig: 96, 103
  offset: 0, 0
  index: -1
Spark-bubble_0003_flash
  rotate: true
  xy: 1762, 949
  size: 73, 271
  orig: 73, 271
  offset: 0, 0
  index: -1
bodynv1
  rotate: false
  xy: 297, 611
  size: 247, 411
  orig: 247, 411
  offset: 0, 0
  index: -1
facenv1
  rotate: true
  xy: 2, 2
  size: 139, 171
  orig: 139, 171
  offset: 0, 0
  index: -1
frame12
  rotate: false
  xy: 2, 565
  size: 293, 457
  orig: 293, 457
  offset: 0, 0
  index: -1
hair2nv1
  rotate: false
  xy: 1955, 785
  size: 79, 162
  orig: 79, 162
  offset: 0, 0
  index: -1
hair3nv1
  rotate: true
  xy: 297, 521
  size: 88, 217
  orig: 88, 217
  offset: 0, 0
  index: -1
hair4nv1
  rotate: false
  xy: 175, 35
  size: 44, 106
  orig: 44, 106
  offset: 0, 0
  index: -1
hair5nv1
  rotate: false
  xy: 1129, 810
  size: 255, 212
  orig: 255, 212
  offset: 0, 0
  index: -1
hair6nv1
  rotate: false
  xy: 1307, 653
  size: 76, 155
  orig: 76, 155
  offset: 0, 0
  index: -1
hair9nv1
  rotate: true
  xy: 1386, 769
  size: 166, 293
  orig: 166, 293
  offset: 0, 0
  index: -1
hand1nv1
  rotate: false
  xy: 1087, 667
  size: 218, 141
  orig: 218, 141
  offset: 0, 0
  index: -1
hand2nv1
  rotate: false
  xy: 1681, 612
  size: 148, 150
  orig: 148, 150
  offset: 0, 0
  index: -1
hand3nv1
  rotate: false
  xy: 1913, 634
  size: 53, 63
  orig: 53, 63
  offset: 0, 0
  index: -1
necknv1
  rotate: true
  xy: 552, 130
  size: 187, 149
  orig: 187, 149
  offset: 0, 0
  index: -1
vai2nv1
  rotate: false
  xy: 546, 701
  size: 298, 321
  orig: 298, 321
  offset: 0, 0
  index: -1
vai3mv1
  rotate: true
  xy: 846, 684
  size: 125, 239
  orig: 125, 239
  offset: 0, 0
  index: -1
vaynv1
  rotate: false
  xy: 405, 313
  size: 103, 206
  orig: 103, 206
  offset: 0, 0
  index: -1
webp - 2020-07-31T093837.184
  rotate: true
  xy: 1542, 618
  size: 149, 137
  orig: 149, 137
  offset: 0, 0
  index: -1
