var fs = require('fs');
var path = require('path');
var crypto = require('crypto');
var exec = require("child_process").exec;

var manifest = {
    packageUrl: 'http://localhost/tutorial-hot-update/remote-assets/',
    remoteManifestUrl: 'http://localhost/tutorial-hot-update/remote-assets/project.manifest',
    remoteVersionUrl: 'http://localhost/tutorial-hot-update/remote-assets/version.manifest',
    version: '1.0.0',
    assets: {},
    searchPaths: [],
    providerCode: ''
};

var dest = './remote-assets/';
var src = './jsb/';

// Parse arguments
var i = 2;
var cdnUrl;
while ( i < process.argv.length) {
    var arg = process.argv[i];

    switch (arg) {
    case '--cdn' :
    case '-c' :
        cdnUrl = process.argv[i+1];
        i += 2;
        break;
    case '--url' :
    case '-u' :
        var url = process.argv[i+1];
        manifest.packageUrl = url;
        manifest.remoteManifestUrl = url + 'project.manifest';
        manifest.remoteVersionUrl = url + 'version.manifest';
        i += 2;
        break;
    case '--version' :
    case '-v' :
        manifest.version = process.argv[i+1];
        i += 2;
        break;
    case '--src' :
    case '-s' :
        src = process.argv[i+1];
        i += 2;
        break;
    case '--dest' :
    case '-d' :
        dest = process.argv[i+1];
        i += 2;
        break;
    case '-p' :
        manifest.providerCode = process.argv[i+1];
        i += 2;
        break;
    default :
        i++;
        break;
    }
}
if (cdnUrl)
{
	manifest.packageUrl = cdnUrl;
	console.log(manifest);
}


function readDir (dir, obj) {
    var stat = fs.statSync(dir);
    if (!stat.isDirectory()) {
        return;
    }
    var subpaths = fs.readdirSync(dir), subpath, size, md5, compressed, relative;
    for (var i = 0; i < subpaths.length; ++i) {
        if (subpaths[i][0] === '.') {
            continue;
        }
        subpath = path.join(dir, subpaths[i]);
        stat = fs.statSync(subpath);
        if (stat.isDirectory()) {
            readDir(subpath, obj);
        }
        else if (stat.isFile()) {
            // Size in Bytes
            size = stat['size'];
            md5 = crypto.createHash('md5').update(fs.readFileSync(subpath, 'binary')).digest('hex');
            compressed = path.extname(subpath).toLowerCase() === '.zip';

            relative = path.relative(src, subpath);
            relative = relative.replace(/\\/g, '/');
            relative = encodeURI(relative);
            obj[relative] = {
                'size' : size,
                'md5' : md5
            };
            if (compressed) {
                obj[relative].compressed = true;
            }
        }
    }
}

function readFileZip (file, obj) {
    // Size in Bytes
    size = file['size'];
    md5 = crypto.createHash('md5').update(fs.readFileSync(file, 'binary')).digest('hex');
    compressed = true;

    relative = path.relative(src, file);
    relative = relative.replace(/\\/g, '/');
    relative = encodeURI(relative);
    obj[relative] = {
        'size' : size,
        'md5' : md5
    };
    if (compressed) {
        obj[relative].compressed = true;
    }
}

var mkdirSync = function (path) {
    try {
        fs.mkdirSync(path);
    } catch(e) {
        if ( e.code != 'EEXIST' ) throw e;
    }
}

// Iterate res and src folder
readDir(path.join(src, 'src'), manifest.assets);
// readDir(path.join(src, 'res'), manifest.assets);
readFileZip(path.join(src, 'res.zip'), manifest.assets);

var destManifest = path.join(dest, 'project.manifest');
var destVersion = path.join(dest, 'version.manifest');

mkdirSync(dest);

fs.writeFile(destManifest, JSON.stringify(manifest), (err) => {
  if (err) throw err;
  console.log('Manifest successfully generated');
});

delete manifest.assets;
delete manifest.searchPaths;
fs.writeFile(destVersion, JSON.stringify(manifest), (err) => {
  if (err) throw err;
  console.log('Version successfully generated');
});
