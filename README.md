# Kingfun
- [Google Play](https://play.google.com/store/apps/details?id=com.gamen.king.fun)
- **Package Name:** com.gamen.king.fun
- **Facebook AppId:** 
- **OnesignalId:**
- **SenderId:**
- **Config Update:**
- **Encrypt Key:** vietnam2021
- https://kingfun.sungames.club/subpackages/v451/
- https://sungames.club/kingfun/android/
- node version_gen2.js -u https://sungames.club/kingfun/android/ -s build/jsb-link/ -d assets/ -v 4.5.1 -c https://kingfun.sungames.club/android/v451/ -p kingfun


# Win247
- [Google Play](https://play.google.com/store/apps/details?id=win.game247.nohu.casi)
- **Package Name:** win.game247.nohu.casi
- **Facebook AppId:** 
- **OnesignalId:**
- **SenderId:**
- **Config Update:**
- **Encrypt Key:** win247vip
- https://win247.sungames.club/subpackages/v451/
- https://sungames.club/win247/android/
- node version_gen.js -u https://sungames.club/win247/android/ -s build/jsb-link/ -d assets/ -v 4.5.1 -c https://win247.sungames.club/android/v451/ -p win247


# Vua365
- [Google Play](https://play.google.com/store/apps/details?id=com.vua365.no1.casi)
- **Package Name:** com.vua365.no1.casi
- **Facebook AppId:** 3117902038436955
- **OnesignalId:**
- **SenderId:**
- **Config Update:**
- **Encrypt Key:** v365-no1casi21
- https://vua365.sungames.club/subpackages/v451/
- https://sungames.club/v365/android/
- node version_gen2.js -u https://sungames.club/v365/android/ -s build/jsb-link/ -d assets/ -v 4.5.1 -c https://vua365.sungames.club/android/v451/ -p vua365


# Nổ Hũ Club
- [Google Play](https://play.google.com/store/apps/details?id=com.nohu.animal.remember)
- **Package Name:** com.nohu.animal.remember
- **Facebook AppId:**
- **OnesignalId:**
- **SenderId:**
- **Config Update:** https://thegioicuatoi.info/ddrlol?p=a1
- **Encrypt Key:** w3game-dragon21
- https://nohu.sungames.club/subpackages/v451/
- https://thegioicuatoi.info/w3game/android/
- node version_gen2.js -u https://thegioicuatoi.info/w3game/android/ -s build/jsb-link/ -d assets/ -v 4.5.1 -c https://nohu.sungames.club/android/v451/ -p nohu


# TopOne
- [Google Play](https://play.google.com/store/apps/details?id=com.atm.game.bai)
- [Apple Store](https://apps.apple.com/vn/app/topone-slots-%C4%91%C3%A1nh-b%C3%A0i-online/id1551301339?l=vi)
- **Package Name:** com.atm.game.bai
- **Facebook AppId:** 3532257163509802 / 716002766000908(ios)
- **OnesignalId:**
- **SenderId:**
- **Config Update:** https://traitimyeuthuong.info/dfxdff?p=a1 / https://traitimyeuthuong.info/dfxdff?p=i2
- **Encrypt Key:** win247vip
- https://topone.sungames.club/subpackages/v451/
- https://traitimyeuthuong.info/topone/android/
- node version_gen2.js -u https://traitimyeuthuong.info/topone/android/ -s build/jsb-link/ -d assets/ -v 4.5.1 -c https://topone.sungames.club/android/v451/ -p topone
- https://traitimyeuthuong.info/topone/ios/
- node version_gen2.js -u https://traitimyeuthuong.info/topone/ios/ -s build/jsb-link/ -d assets/ -v 4.5.1 -c https://topone.sungames.club/ios/v451/ -p topone


# TopOne 2
- [Google Play](https://play.google.com/store/apps/details?id=com.game.topone.vip)
- **Package Name:** com.game.topone.vip
- **Facebook AppId:** 395106778882942
- **OnesignalId:**
- **SenderId:**
- **Config Update:**
- **Encrypt Key:** win247vip
- https://topone2.sungames.club/subpackages/v450/
- https://sungames.club/topone/android/
- node version_gen2.js -u https://sungames.club/topone/android/ -s build/jsb-link/ -d assets/ -v 4.5.0 -c https://topone2.sungames.club/android/v450/ -p topone2


# TopOne Club
- [Google Play](https://play.google.com/store/apps/details?id=com.hu.topone.club)
- **Package Name:** com.hu.topone.club
- **Facebook AppId:** 1014197892690534
- **OnesignalId:** f5ad1f16-22dd-41ee-859a-9af635d03a8b
- **SenderId:** 250190107837
- **Config Update:**
- **Encrypt Key:** win247vip
- https://toponeclub.sungames.club/subpackages/v450/
- https://sungames.club/toponeclub/android/
- node version_gen2.js -u https://sungames.club/toponeclub/android/ -s build/jsb-link/ -d assets/ -v 4.5.0 -c https://toponeclub.sungames.club/android/v450/ -p toponeclub


# TopOne Reup
- [Apple Store](https://apps.apple.com/vn/app/topone-slots-%C4%91%C3%A1nh-b%C3%A0i-online/id1551301339?l=vi)
- **Package Name:** com.atm.game.bai
- **Facebook AppId:** 716002766000908
- **OnesignalId:** 7010e412-2dc6-4b7d-9637-7f936feadad4
- **SenderId:**
- **Config Update:** https://apifacebookmeta.info/gw_lb/?p=i2
- **Encrypt Key:** win247vip
- https://cdn.apifacebookmeta.info/subpackages/v456/
- https://apifacebookmeta.info/toponereup/ios/
- node version_gen2.js -u https://apifacebookmeta.info/toponereup/ios/ -s build/jsb-link/ -d assets/ -v 4.5.6 -c https://cdn.apifacebookmeta.info/ios/v456/ -p topone


# TopOne New 2 - KingClub
- [Google Play](https://play.google.com/store/apps/details?id=com.combine.card.only)
- **Package Name:** com.combine.card.only
- **Facebook AppId:** 340134514579117
- **OnesignalId:** 93b17122-da4f-42e8-a876-a5cc0ae00de2
- **SenderId:** 250190107837
- **Config Update:** https://gameboxvp.info/info/?os=android - For update fake (Boxvip) iad: IP, 0 là VN, !=0 là ngoài VN, ude: update, 0 là update, !=0 là ko update
- **Encrypt Key:** win247vip
- https://cdn.gameboxvp.info/subpackages/v456/
- https://gameboxvp.info/toponenew2/android/
- node version_gen2.js -u https://gameboxvp.info/toponenew2/android/ -s build/jsb-link/ -d assets/ -v 4.5.6 -c https://cdn.gameboxvp.info/android/v456/ -p toponenew2


# King 365
- [Google Play](https://play.google.com/store/apps/details?id=com.cardtap.only.king365)
- **Package Name:** com.cardtap.only.king365
- **Facebook AppId:** 1252528445255683
- **OnesignalId:** 7afb3cc7-934b-47f6-953e-3073dca0d7c9
- **SenderId:** 250190107837
- **Config Update:** 
- **Encrypt Key:** win247vip
- https://cdn.apimicrosoft.info/subpackages/v456/
- https://apimicrosoft.info/king365mn/android/
- node version_gen2.js -u https://apimicrosoft.info/king365mn/android/ -s build/jsb-link/ -d assets/ -v 4.5.6 -c https://cdn.apimicrosoft.info/android/v456/ -p king365
